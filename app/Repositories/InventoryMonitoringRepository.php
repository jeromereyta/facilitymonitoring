<?php

namespace App\Repositories;

use App\Models\Inventory;
use App\Models\monitorings;

use Request, Input,Image;
use Auth;
class InventoryMonitoringRepository
{

	public function create($data)
	{
		$result = null;
		$name 	= \DB::table('monitorings')
		->where('name',$data['name'])
		->whereNull('deleted_at')
		->count();

		if($name)
		{
			$result = 'This monitoring name already exist';
		} else
		  {
			$create = \DB::table('monitorings')
			->insert([
				'name' 			  => $data['name'],
				'description' 	  => $data['description'],
				'monitoring_type' => 'Inventory Monitoring',
				'created_by'  	  => Auth::user()->id,
				'created_at'  	  => date('Y-m-d H:i:s')
			]);

				$id  = \DB::table('monitorings')
				->where('name',$data['name'])
				->whereNull('deleted_at')
				->select('id')
				->get()[0]->id;

			$data['id'] 		 = $id;
			$monitoringInventory = $this->CreateMonitoringInventory($data);		
			$result 			 = 'Monitoring Inventory Created!';
		  }
		  return $result;
	}


	public function delete($id)
	{
		$query = \DB::table('monitorings')
		->where('id',$id)
		->update([
			'deleted_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);
	}


	public function get($id)
	{
		$monitoring = \DB::table('monitorings')
		->leftjoin('users','users.id','=','monitorings.created_by')
		->whereNull('monitorings.deleted_at')
		->where('monitoring_type','Inventory Monitoring')
		->where('monitorings.id',$id)
		->select('monitorings.id','monitorings.name','monitorings.description','users.name as created_by','monitorings.created_at')
		->get();

		$monitoringDetails 	   = null;
		$inventory_monitorings = null;
		$facilities 	       = $this->getFacilityList();


		foreach ($monitoring as $key => $value)
		 {
     		$monitoringDetails[$key]['name'] = $value->name;
     		$monitoringDetails[$key]['id'] = $value->id;

     		$monitoringDetails[$key]['description'] = $value->description;
     		$monitoringDetails[$key]['created_by'] = $value->created_by;
     		$monitoringDetails[$key]['created_at'] = $value->created_at;
     					
			$facility_details = $this->getFacilityDetailsByMonitoringId($value->id);
			
			foreach ($facility_details as $index => $facility)
			{
	     		$inventory_monitorings[$facility->name]['id']= $facility->id;
				$inventory_monitorings[$facility->name]['room_type']= $facility->room_type;
				$inventory_monitorings[$facility->name]['description']= $facility->description;
				$inventory_monitorings[$facility->name]['inventories']=$this->getInventoriesInMonitoring($facility->id,$value->id);
				
			}

			$monitoringDetails[$key]['facilities'] = $inventory_monitorings;
		}

		return $monitoringDetails;
	}


	public function  isMonitoringNameExist($name)
	{
		$result = 'Name already exist!';
	  	$name   = \DB::table('monitorings')
		->where('name',$data['name'])
		->whereNull('deleted_at')
		->count();

        return ($result)?true:false;
	}

	public function update($data)
	{
	
		$name = \DB::table('monitorings')
		->where('name',$data['name'])
		->whereNull('deleted_at')
		->update([
			'name' 		=> $data['name'],
			'description' => $data['description'],
			'updated_at'  => date('Y-m-d H:i:s')
		]);

		$result = 'Monitoring Inventory Created!';
		return $result;
	}



	public function getMonitoringIdByName($name)
	{
		return \DB::table('monitorings')
		->where('name',$name)
		->select('id')
		->get()[0]->id;
	}


	public function getFacilityIdByName($name)
	{
		return \DB::table('facilities')
		->where('name',$name)
		->select('id')
		->get()[0]->id;
	}


	public function getInventoryIdByName($name)
	{
		return \DB::table('inventory_items')
		->where('name',$name)
		->select('id')
		->get()[0]->id;

	}


	public function getStatusByInventoryId($id)
	{
		$query = \DB::table('monitoring_inventory')
		->join('inventory_status','id','=','monitoring_inventory.inventory_status_id')
		->select('inventory_status.name')
		->where('monitoring_inventory.inventory_id',$id)
		->get();

		return $query;

	}



	// Author :: mightyJ
	public function getMonitoringInventoryByFacilityId($facility_id)
	{
		return monitorings::join('monitoring_inventory','monitoring_inventory.monitoring_id','=','monitorings.id')
        ->join('users','users.id','=','monitorings.created_by')
        ->join('facilities','facilities.id','=','monitoring_inventory.facility_id')
        ->join('categories','categories.id','=','facilities.category_id')
        ->select('monitorings.id','monitorings.name','monitorings.description','users.name as created_by','monitorings.created_at')
        ->where('monitoring_inventory.facility_id',$facility_id)
        ->distinct('monitoring_inventory.facility_id')
        ->paginate(10);
	}


	// Author: mightyJ
	public function CreateMonitoringInventory($data)
	{
		// dd($data);
 
		$facility_row  = count($data['inventory_id']);

		for ($i=0; $i < $facility_row ; $i++)
		{ 
			$insertMonitoringInventory = \DB::table('monitoring_inventory')
			->insert([
			'monitoring_id' 	  => $data['id'],
			'facility_id'         => $data['facility_id'],
			'inventory_id'		  => $data['inventory_id'][$i],
			'inventory_status_id' => $data['status'][$i],
			'remarks'			  => $data['remarks'][$i],
			'created_at'		  => date('Y-m-d H:i:s')
			]);	
		}

	}

	//Author:: mightyJ
	public function UpdateMonitoringFacilityInventory($data)
	{
		foreach($data['inventory_id'] as $key =>$value)
		{

			$inventories[$key] = \DB::table('monitoring_inventory')
			->where('monitoring_id',$data['monitoring_id'])
			->where('inventory_id',$value)
			->update([
				'inventory_status_id' => $data['status'][$value],
				'remarks'			  => $data['remarks'][$value],
				'updated_at'		  => date('Y-m-d H:i:s')
			 ]);
		}
	}



	public function getInventoriesByFacilityId($facility_id)
	{
        return \DB::table('inventory_items')
        -> leftjoin('categories','categories.id','=','inventory_items.category')
        -> select('inventory_items.id','inventory_items.name','inventory_items.description','inventory_items.serial_no','categories.name as category')
        -> where('facility_id',$facility_id)        
        ->get();

	}








	public function getStatusIdByName($name)
	{
		$query= \DB::table('inventory_status')
		->where('name',$name)
		->select('id')
		->get();

		if($query){
			$query = $query[0]->id;
		}

		return $query;

	}

	public function UpdateMonitoringInventory($data)
	{
		$monitoringInventory = $data['data'] ;

		foreach ($monitoringInventory as $facility_name => $facility) 
		{
			foreach ($facility as $inventory_name => $inventory) 
			{
				$query = \DB::table('monitoring_inventory')
				->where('monitoring_id',$this->getMonitoringIdByName($data['name']))
				->where('facility_id',$this->getFacilityIdByName($facility_name))
				->where('inventory_id',$this->getInventoryIdByName($inventory_name))
				->update([
					'inventory_status_id' =>$this->getStatusIdByName($inventory['status']),
					'remarks'			  =>$inventory['remarks'],
					'updated_at' 		  =>date('Y-m-d H:i:s')
					]);		
			}
		}

	}

	public function updateInventoryMonitoringStatus($inventory_monitoring_id,$facility_id,$inventory_id,$status)
	{
			$query = \DB::table('monitoring_inventory')
				->where('monitoring_id',$inventory_monitoring_id)
				->where('facility_id',$facility_id)
				->where('inventory_id',$inventory_id)
				->update([
					'inventory_status_id' =>$this->getStatusIdByName($status),
					'updated_at' 		  =>date('Y-m-d H:i:s')
					]);		
	}

	public function updateInventoryMonitoringRemarks($inventory_monitoring_id,$facility_id,$inventory_id,$remarks)
	{
			$query = \DB::table('monitoring_inventory')
				->where('monitoring_id',$inventory_monitoring_id)
				->where('facility_id',$facility_id)
				->where('inventory_id',$inventory_id)
				->update([
					'remarks' => $remarks,
					'updated_at' =>date('Y-m-d H:i:s')
					]);		
	}


	public function getFacilityDetailsByMonitoringId($id)
	{
		$facilities = \DB::table('monitoring_inventory')
		-> join('facilities','facilities.id','=','monitoring_inventory.facility_id')
		-> select('facilities.id','facilities.name','room_type','facilities.description','facilities.created_at')
		-> where('monitoring_inventory.monitoring_id',$id)
		-> get();

		return $facilities;		

	}

	public function getInventoriesInAFacility($facility_id)
	{

		$inventories = \DB::table('inventory_items')
		->select('inventory_items.id','inventory_items.name as inventory_name','serial_no','description','category','inventory_items.created_at')
		->whereNull('inventory_items.deleted_at')
		->where('inventory_items.facility_id',$facility_id)
		->where('inventory_items.inventory_type','inventory')
		->get();

	
		$getInventoriesInAFacility = null;
		foreach($inventories as $key => $inventory)
		{
			$getInventoriesInAFacility[$key]['id']= $inventory->id;
			$getInventoriesInAFacility[$key]['inventory_name']= $inventory->inventory_name;
			$getInventoriesInAFacility[$key]['serial_no']= $inventory->serial_no;
			$getInventoriesInAFacility[$key]['description']= $inventory->description;
			$getInventoriesInAFacility[$key]['category']= $inventory->category;
			$getInventoriesInAFacility[$key]['created_at']= $inventory->created_at;
		}
		return $getInventoriesInAFacility;	
	}


	public function getInventoriesInMonitoring($facility_id,$monitoring_id)
	{
		$inventories = \DB::table('monitoring_inventory')
		->leftjoin('inventory_items','inventory_items.id','=','monitoring_inventory.inventory_id')
		->join('inventory_status','inventory_status.id','=','monitoring_inventory.inventory_status_id')
		->select('inventory_items.id','inventory_items.name as inventory_name','serial_no','description','category','inventory_items.created_at','inventory_status.name as status','monitoring_inventory.remarks')
		->whereNull('inventory_items.deleted_at')
		->where('monitoring_inventory.monitoring_id',$monitoring_id)
		->where('inventory_items.facility_id',$facility_id)
		->where('monitoring_inventory.facility_id',$facility_id)
		->distinct('inventory_items.name')
		->get();

	
		$getInventoriesInAFacility = null;
		foreach($inventories as $key => $inventory)
		{
			$getInventoriesInAFacility[$key]['id']= $inventory->id;
			$getInventoriesInAFacility[$key]['inventory_name']= $inventory->inventory_name;
			$getInventoriesInAFacility[$key]['serial_no']= $inventory->serial_no;
			$getInventoriesInAFacility[$key]['description']= $inventory->description;
			$getInventoriesInAFacility[$key]['category']= $inventory->category;
			$getInventoriesInAFacility[$key]['remarks']= $inventory->remarks;
			$getInventoriesInAFacility[$key]['status']= $inventory->status;

			$getInventoriesInAFacility[$key]['created_at']= $inventory->created_at;
		}
		return $getInventoriesInAFacility;
	}




	public function getFacilityList()
	{
		$facilities = \DB::table('facilities')
		->join('inventory_items','facility_id','=','facilities.id')
		-> select('facilities.id','facilities.name','room_type','facilities.description','facilities.created_at')
		-> whereNull ('facilities.deleted_at')
		-> get();
		return $facilities;
	}

	public function getInventoryStatus()
	{
		$status = \DB::table('inventory_status')
		->select('id','name as status_name')
		->whereNull('deleted_at')
		->get();
		return $status;
	}

	public function getInventoryStatusOfItem()
	{
		$status = \DB::table('monitoring_inventory')
		->join('inventory_status','inventory_status.id','=','monitoring_inventory.inventory_status_id')
		->select('monitoring_inventory.monitoring_id','inventory_status.name as status_name',
			'monitoring_inventory.inventory_status_id','monitoring_inventory.facility_id')
		->whereNull('monitoring_inventory.deleted_at')
		->get();
		return $status;
	}

	public function getFacilityDetails()
	{
		$inventory_monitorings = null;
		$facilities 	       = $this->getFacilityList();
		
		foreach ($facilities as $key => $facility) 
		{
			$inventory_monitorings[$facility->name]['id']= $facility->id;
			$inventory_monitorings[$facility->name]['room_type']= $facility->room_type;
			$inventory_monitorings[$facility->name]['description']= $facility->description;
			$inventory_monitorings[$facility->name]['inventories']=$this->getInventoriesInAFacility($facility->id);	
		}
		
		return $inventory_monitorings;
	}

	public function getListOfInventoryMonitoring()
	{
		return \DB::table('monitorings')
		->leftjoin('users','users.id','=','monitorings.created_by')
		->whereNull('monitorings.deleted_at')
		->where('monitoring_type','Inventory Monitoring')
		->select('monitorings.id','monitorings.name','monitorings.description','users.name as created_by','monitorings.created_at')
		->get();

	}




	public function getInventoriesWithMonitoringId($monitoring_id,$facility_id)
	{
		// dd($monitoring_id,$facility_id);
		return \DB::table('monitorings')
		->join('monitoring_inventory','monitoring_inventory.monitoring_id','=','monitorings.id')
		->join('inventory_items','inventory_items.id','=','monitoring_inventory.inventory_id')
		->join('inventory_status','inventory_status.id','=','monitoring_inventory.inventory_status_id')
		->leftjoin('categories','categories.id','=','inventory_items.category')
		->select(
			'inventory_items.id',
			'inventory_items.name',
			'inventory_items.serial_no',
			'inventory_items.description',
			'categories.name as category',
			'inventory_status.name as status',
			'inventory_status.id as status_id',
			'monitoring_inventory.remarks',
			'monitoring_inventory.monitoring_id',
			'monitoring_inventory.facility_id'
			)
		->where('monitorings.id',$monitoring_id)
		->where('monitoring_inventory.facility_id',$facility_id)
		->distinct('monitoring_inventory.inventory_id')
	
		->get();
	}
}