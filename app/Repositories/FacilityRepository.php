<?php

namespace App\Repositories;

use App\Models\Inventory;
use Request, Input,Image;

class FacilityRepository
{

	public function getFacilities()
	{

		$facilities = \DB::table('facilities')
		->leftjoin('categories','categories.id','=','facilities.category_id')
		->select('facilities.id','facilities.name','categories.name as room_type','facilities.description','facilities.created_at',
		\DB::raw('(CASE WHEN facilities.deleted_at is NULL THEN "Active" ELSE "Deleted" END) AS availability'))

		->orderBy('availability')
		->paginate(10);

			return $facilities;	
	}


	public function getFacilityCount()
	{

		$facilities = \DB::table('inventory_items')
		->select('inventory_items.facility_id as id',\DB::raw('count(inventory_items.facility_id) as facility_count'))
		->join('facilities','facilities.id','=','inventory_items.facility_id')
		->orderBy('facility_count')
		->groupBy('facility_id')
		->get();
		
		return $facilities;	
	}
	

/*	public function get($id){
		
		$inventory = Inventory::findOrFail($id);

        return $inventory;
	}*/
	
	public function create($data)
	{
		$name = \DB::table('facilities')
		->where('name',$data['name'])
		->whereNull('deleted_at')
		->count();
		
		$result ='Name already exist';

		if(!$name)
		{
    		$result = \DB::table('facilities')
	        ->insert([
            'name'        => $data['name'],
            'category_id'   => $data['category'],            
            'room_type'   => $data['category'],
            'description' => $data['description'],
            'created_at'  => date('Y-m-d H:i:s')]);

			$result = 'Added Successfully';		
		}

		return $result;		
	}



	public function update($id,$data)
	{
		$inventory = \DB::table('facilities')
		->whereNull ('deleted_at')
		->where('id',$id)
		->update([
			'name'=> $data['name'],
			'category_id'=> $data['category'],
			'room_type'=> $data['category'],
			'description'=> $data['description'],
			'updated_at'=> date('Y-m-d H:i:s')
		]);

		// $this->updateFacilityInventory($id,$data);

	}


	public function updateFacilityInventory($facility_id,$data)
	{
		if(isset($data['inventories']))
		{
			//remove existing data of the room
			$result = \DB::table('facility_inventory')
	        ->where('facility_id',$facility_id)
	        ->whereNull('deleted_at')
	        ->delete();

	        foreach ($data['inventories'] as $key => $value) 
	        {
	        	$insert= \DB::table('facility_inventory')
	        	->insert([
	        		'facility_id'  => $facility_id,
	        		'inventory_id' => $value->id		
	        	]);
	        }

				
		}
	}



	public function get($id)
	{
		$facility = \DB::table('facilities')
		->leftjoin('categories','categories.id','=','facilities.category_id')
		->select('facilities.id','facilities.name','categories.name as room_type','facilities.description','facilities.created_at')
		->where('facilities.id',$id)
		->get();	

		return $facility;
	}


	public function undelete($id)
	{
		$query = \DB::table('facilities')
		->where('id',$id)
		->update([
			'deleted_at' => NULL,
			'updated_at' => date('Y-m-d H:i:s')
			]);
	}



	public function delete($id)
	{

		$query = \DB::table('facilities')
		->where('id',$id)
		->update([
			'deleted_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);

	}




    public function isFacilityNameExist($name)
    {
        $query = \DB::table('facilities')
        -> where('name',trim($name))
        -> whereNull('deleted_at')
        -> get();

        return ($query)?true:false;
    }


    	public function isFacilityExistInPivotTable($id)
	{
		$query = \DB::table('facility_inventory')
		->where('facility_id',$id)
		->whereNull('deleted_at')
		->count();

		return ($query)?true:false;
	}

	public function getFacilityByName($name)
	{
		$facility = \DB::table('facilities')
		->join('categories','categories.id','=','facilities.category_id')
		->select('facilities.id','facilities.name','categories.name as room_type','facilities.description','facilities.created_at')
		->whereNull ('facilities.deleted_at')
		->where('facilities.name',$name)
		->get();	
		return $facility;
	}

	public function searchFacilities($params){

        if(isset($params['sort'])&&isset($params['order'])) {
            $sort = $params['sort'];
            $order = $params['order'];
        } else {
            $sort = 'facilities.name';
            $order = 'asc';
        }

        $facility = \DB::table('facilities')
            ->where(function ($query) use ($params) {
                if(isset($params['searchQuery'])){
                    $query->where('facilities.name', 'like', '%'.$params['searchQuery'].'%');
                }
            })
            ->join('monitoring_inventory','monitoring_inventory.facility_id','=','facilities.id')
            ->where('monitoring_inventory.monitoring_id',$params['monitoring_id'])
            ->select('facilities.name','facilities.id')
            ->orderBy($sort,$order)
            ->distinct('facilities.name');

            $facilities	  = $facility->paginate(1);


        return $facilities;
    }

    public function searchFacilitiesByUpkeep($params){

        if(isset($params['sort'])&&isset($params['order'])) {
            $sort = $params['sort'];
            $order = $params['order'];
        } else {
            $sort = 'facilities.name';
            $order = 'asc';
        }

        $facility = \DB::table('facilities')
            ->where(function ($query) use ($params) {
                if(isset($params['searchQuery'])){
                    $query->where('facilities.name', 'like', '%'.$params['searchQuery'].'%');
                }
            })
            ->join('monitoring_upkeep','monitoring_upkeep.facility_id','=','facilities.id')
            ->where('monitoring_upkeep.monitoring_id',$params['monitoring_id'])
            ->select('facilities.name','facilities.id')
            ->orderBy($sort,$order)
            ->distinct('facilities.name');

            $facilities	  = $facility->paginate(5);


        return $facilities;
    }

	public function searchFacilityItems($params){

		if(isset($params['sort'])&&isset($params['order'])) {
            $sort = $params['sort'];
            $order = $params['order'];
        } else {
            $sort = 'facilities.name';
            $order = 'asc';
        }

            $facility_items = \DB::table('facilities')
            ->where(function ($query) use ($params) {
                if(isset($params['searchQuery'])){
                    $query->where('facilities.name', 'like', '%'.$params['searchQuery'].'%');
                }
            })
            ->join('inventory_items','inventory_items.facility_id','=','facilities.id')
          	->where('inventory_type','inventory')
            ->select('inventory_items.name','facilities.id')
            ->orderBy($sort,$order);

            $facility_items	 ;

            return $facility_items;
    }

}