<?php

namespace App\Repositories;

use App\Models\Inventory;
use Request, Input,Image;

class InventoryRepository
{

	public function getInventories()
	{
		$inventories = \DB::table('inventory_items')
		->select    ('id','name','serial_no','description','category','created_at',         
		   	\DB::raw('(CASE WHEN inventory_items.deleted_at is NULL THEN "Active" ELSE "Deleted" END) AS status'))
		// ->whereNull ('deleted_at')
		->where('inventory_type','inventory')
		->orderBy('status')
		->paginate(10);

		return $inventories;	
	}

	public function create($data)
	{
		$name = \DB::table('inventory_items')
		->where('name',$data['name'])
		->where('inventory_type','inventory')
		->whereNull('deleted_at')
		->count();
		
		$serial_no = \DB::table('inventory_items')
		->where('serial_no',$data['serial_no'])
		->whereNull('deleted_at')
		->where('inventory_type','inventory')
		->count();

		if($name || $serial_no)
		{
			if($name && $serial_no==false)
				{return 'Name already exist';}
			if($serial_no && $name==false)
				{return 'Serial Number already exist';}
				return 'Name and Serial Number already exist';
		}

		if($name ==false && $serial_no ==false)
	   	{   
	   	    $result = \DB::table('inventory_items')
	        ->insert([
	            'name'           => $data['name'],
	            'serial_no'      => $data['serial_no'],
	            'category' 	     => $data['category'],
	            'inventory_type' => 'inventory',
	            'description'    => $data['description'],
	            'created_at'     => date('Y-m-d H:i:s')]);

			return 'Added Successfully';
		}

	}

	public function getUpkeepItems()
	{

		$items = \DB::table('inventory_items')
		->select    ('id','name','description','created_at')
		->whereNull ('deleted_at')
		->where('inventory_type','upkeep')
		->paginate(10);
	
		return $items;	

	}


	public function createUpkeepItem($data)
	{
		$result = 'Name already exist';
		$name   = \DB::table('inventory_items')
		->where('name',$data['name'])
		->where('inventory_type','upkeep')
		->whereNull('deleted_at')
		->count();

		if(!$data)
			{$result='No data to create';}

		if(!$name)
		{
			if(!isset($data['remarks_required'])){
				$data['remarks_required'] = 0;
			}
			$result = \DB::table('inventory_items')
	        ->insert([
	            'name'           		=> $data['name'],
	            'serial_no'      		=> 'null',
	            'category' 	     		=> 'null',
	            'inventory_type' 		=> 'upkeep',
	            'is_remarks_required'	=> $data['remarks_required'],
	            'description'    		=> $data['description'],
	            'created_at'     		=> date('Y-m-d H:i:s')]);

			$result = 'Added Successfully';	
		}

		return $result;
	}


	public function getUpkeepItem($id)
	{
		 $inventory = \DB::table('inventory_items')
		->select    ('id','name','serial_no','description','category','is_remarks_required','created_at')
		->whereNull ('deleted_at')
		->where('id',$id)
		->where('inventory_type','upkeep')
		->get();

		return $inventory;
	}

	public function get($id)
	{
		 $inventory = \DB::table('inventory_items')
		->select    ('id','name','serial_no','description','category','created_at')
		->whereNull ('deleted_at')
		->where('id',$id)
		->where('inventory_type','inventory')
		->get();

		return $inventory;
	}

	public function update($id,$data)
	{
		 $inventory = \DB::table('inventory_items')
		->whereNull ('deleted_at')
		->where('id',$id)
		->where('inventory_type','inventory')
		->update([
			'name'=> $data['name'],
			'serial_no'=> $data['serial_no'],
			'category'=> $data['category'],
			'description'=> $data['description'],
			'updated_at'=> date('Y-m-d H:i:s')
		]);
	}



	public function updateUpkeepItem($id,$data)
	{
		if(!isset($data['remarks_required'])){
				$data['remarks_required'] = 0;
			}

		 $inventory = \DB::table('inventory_items')
		->whereNull ('deleted_at')
		->where('id',$id)
		->where('inventory_type','upkeep')
		->update([
			'name'					=> $data['name'],
			'serial_no'				=> null,
			'category'				=> null,
			'description'			=> $data['description'],
			'is_remarks_required' 	=> $data['remarks_required'],
			'updated_at'			=> date('Y-m-d H:i:s')
		]);
	}


	public function undelete($id)
	{
		$inventory = \DB::table('inventory_items')
		->where('id',$id)
		->update([
			'deleted_at' => NULL,
			'updated_at' => date('Y-m-d H:i:s')
			]);
	}

	public function delete($id)
	{
		$inventory = \DB::table('inventory_items')
		->where('id',$id)
		->whereNull('deleted_at')
		->update([
			'deleted_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
	}

	public function isNameExist($name)
	{
		$inventory = \DB::table('inventory_items')
		->where('name',trim($name))
		->whereNull('deleted_at')
		->count();

        return ($inventory)?true:false;
	}

	public function isSerialNumberExist($serial_no)
	{
		$inventory = \DB::table('inventory_items')
		->where('serial_no',trim($serial_no))
		->whereNull('deleted_at')
		->count();

        return ($inventory)?true:false;
	}

	public function search($params){

        if(isset($params['sort'])&&isset($params['order'])) {
            $sort = $params['sort'];
            $order = $params['order'];
        } else {
            $sort = 'inventory_items.name';
            $order = 'asc';
        }

        $inventories = \DB::table('facilities')
            ->rightjoin('inventory_items','inventory_items.facility_id','=','facilities.id')
             ->where('inventory_type','inventory')
            ->where(function ($query) use ($params) {
                if(isset($params['searchQuery'])){
                    $query->where('inventory_items.name', 'like', '%'.$params['searchQuery'].'%');
                }
                if($params['page_type']==='template_list'){
                	$query->where('inventory_items.facility_id','=','0');
                }
                if($params['page_type']==='template_selected'){
                	$query->where('inventory_items.facility_id','=',$params['facility_id']);
                }
            })
    
            ->select('inventory_items.name','inventory_items.id','inventory_items.serial_no','inventory_items.description','inventory_items.created_at','category',
            	\DB::raw('(CASE WHEN inventory_items.deleted_at is NULL THEN "Active" ELSE "Deleted" END) AS status'))
            ->orderBy($sort,$order);
           /* if(isset($params['commodity_id'])){
                $subcommodities= $subcommodities->where('commodity_id', '=',$params['commodity_id']);
            }*/
            $inventories = $inventories->paginate(10);

        return $inventories;
    }
}