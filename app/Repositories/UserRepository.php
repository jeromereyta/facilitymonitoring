<?php

namespace App\Repositories;

use App\User;
use Request, Input, Image;


class UserRepository
{
    
    /**
     * Get all the users
     * 
     * @return App\Models\User Collection;
     */
    public function getAll()
    {
        return User::all();
    }

    public function getUsers()
    {
        $users = \DB::table('users')
        ->select    ('id','name','created_at','updated_at')
        ->get();
        
        return $users;
    }

    /**
     * Get single instance
     * @param  $id
     * @return App\Models\User;
     */
    public function get($id) 
    {
        $user = User::withTrashed()->findOrFail($id);
        return $user; 
    }
    protected function setTemporaryPassword($data)
    {
        $data['temp_password'] = 'dtiopms';
        $data['password'] = 'dtiopms';
        return $data;
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function update($user_id, $data)
    {   
        $data['role'] = explode("-", $data['role'])[0];
        $user = User::findOrFail($user_id);

        if(Input::hasFile('thumbnail')) {
            $file = Input::file('thumbnail');
            if(Input::file('thumbnail')->isValid()) {
                $destinationPath = '/uploads/';
                $extension = Input::file('thumbnail')->getClientOriginalExtension();
                $originalFilename = $destinationPath.rand(11111, 99999).'.'.$extension;

                $img = Image::make($file);
                $img->resize(null, 200, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $img->save(public_path($originalFilename));

                $data['thumbnail'] = $originalFilename;
            }
        }
        $user->update($data);
        $role = Role::findOrFail($data['role']);
        if( ! $user->hasRole($role->name) )
        {
            // Remove existing role
            $user->removeCurrentRole($user->roles()->first()->id);
            // Asign a new role
            $user->assignRole($role->id);
        }
        return $user;
    }


    public function destroy($id)
    {
        $user = $this->get($id);

        $user->delete();
        
        return $user;
    }
     public function restore($id)
    {
        $user = $this->get($id); 

        $user->restore();
        
        return $user;
    }


    /**
     * Search
     * Kelvin Roger Ang Yap 2015
     * @param  $params Search params
     * @return
     */
    /**/
    protected function buildInitialQuery($params){
        $query = User::select('users.*')
            ->where(function ($query) use ($params) {
                if(isset($params['searchQuery'])){
                    $query->where('users.last_name', 'like', '%'.$params['searchQuery'].'%')
                        ->orWhere('users.first_name', 'like', '%'.$params['searchQuery'].'%')
                        ->orWhere('users.email', 'like', '%'.$params['searchQuery'].'%');
                }
            });
        return $query;
    }
    protected function buildQuery($params){
        $query =$this->buildInitialQuery($params);
        /*Special Cases Here*/
        if($params['page_type']=="template_list"){
            $query =$query->leftJoin('template_user', function($join)use($params)
            {
                $join->on('template_user.user_id', '=', 'users.id')
                ->where('template_user.template_id','=',$params['template_id']);
            })->whereNull('template_user.user_id');

            if(Auth::user()->role('System Admin')){
                //do nothing you will be able to see everything
            }else if(Auth::user()->role('Provincial Officer')){
                $query =$query->where('users.director_id','=',Auth::user()->id);
                //only able to see parts
            }else if(Auth::user()->role('Provincial Monitor')){
                //monitor
            }
        }else if($params['page_type']=="template_selected"){
            $query = $query->leftJoin('template_user','template_user.user_id','=','users.id')
                ->where('template_user.template_id','=',$params['template_id']);
        } else if($params['page_type']=="market_list"){

            if(Auth::user()->role('System Admin')){
                //do nothing you will be able to see everything
            }else if(Auth::user()->role('Provincial Officer')){
                $query =$query->where('users.director_id','=',Auth::user()->id);
                //only able to see parts
            }else if(Auth::user()->role('Provincial Monitor')){
                //monitor
            }
            $query =$query->leftJoin('market_user', function($join)use($params)
            {
                $join->on('market_user.user_id', '=', 'users.id')
                    ->where('market_user.market_id','=',$params['market_id']);
            })->whereNull('market_user.user_id')
                ->where('users.director_id','=',Auth::user()->id);
        }else if($params['page_type']=="market_selected") {
            $query = $query->leftJoin('market_user', 'market_user.user_id', '=', 'users.id')
                ->where('market_user.market_id', '=', $params['market_id']);
        }
        else if($params['page_type']=='user_director_search'){
            $query = $query->where('province_id',"=",$params['province_id']);
            $query =$query->join('role_user', function($join)
            {
                $join->on('role_user.user_id', '=', 'users.id')
                    ->where('role_user.role_id','=',Config::get('globals.USER_ROLE_DIRECTOR'));
            })->whereNotNull('role_user.user_id');
        }
        if($params['view_thrashed_rows'] ==true){
            $query = $query->withTrashed();
        }
        return $query->orderBy($params['sort'],$params['order'])
            ->paginate(10);
    }
    public function search($params){

        if(!isset($params['sort'])||!isset($params['order'])) {
            $params['sort'] = 'users.email';
            $params['order'] = 'asc';
        }
        $return = $this->buildQuery($params);
        return $return;
    }


}