<?php

namespace App\Repositories;

use App\Models\rooms;
use InfyOm\Generator\Common\BaseRepository;

class roomsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'room_name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return rooms::class;
    }


    public function isRoomNameExist($roomName){
        $query = \DB::table('rooms')
        ->where('room_name',trim($roomName))
        ->whereNull('deleted_at')
        ->get();
        
        return ($query)?true:false;
    }
}
