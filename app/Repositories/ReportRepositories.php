<?php
namespace App\Repositories;


use App\Http\Requests;
use Illuminate\Http\Request;
use Flash;
use Response;
use \DB;
use App\Repositories\monitoringsRepository;

class ReportRepositories 
{

	protected $monitorings;
    public function __construct(monitoringsRepository $monitorings)
    {
    	$this->monitorings=$monitorings;
    }


    public function getListOfInventoriesFromMonitoring($data)
    {
    	$from   	 = $data['from'];
    	$to     	 = $data['to'];
    	$status 	 = $data['status'];
    	$facility_id = $data['facility_id'];

    	$recent_date = \DB::table('monitoring_inventory')->select('created_at')
    	->orderBy('created_at','desc')
    	->first();


    	$query = \DB::table('monitoring_inventory')
    	-> join('inventory_items','inventory_items.id','=','monitoring_inventory.inventory_id')
		-> join('inventory_status','inventory_status.id','=','monitoring_inventory.inventory_status_id') 
		-> join('facilities','facilities.id','=','monitoring_inventory.facility_id') 
		-> whereNull('monitoring_inventory.deleted_at')
		-> where(function($query) use ($facility_id,$from,$to,$status,$recent_date)
		{            
			if($facility_id)
			{
			  $query->where('monitoring_inventory.facility_id',$facility_id);
			}				
			if($from && $to==false)
			{
			  $query->where('monitoring_inventory.created_at',$from);				
			}
			if($to && $from==false)
			{
			  $query->where('monitoring_inventory.created_at',$to);				
			}
			if($to && $from)
			{
			  $query->whereBetween('monitoring_inventory.created_at',[$from,$to]);				
			}
			if($from ==false && $to==false)
			{
			  $query->where('monitoring_inventory.created_at',$recent_date->created_at);								
			}
			if($status)
			{
			  $query->where('inventory_status.name',$status);				
			}
		})
    	-> select(
    		'inventory_items.id',
    		'inventory_items.name',
    		'inventory_items.serial_no',
    		'inventory_items.description',
    		'inventory_items.category',
    		'inventory_status.name as status',
    		'monitoring_inventory.remarks',
    		'facilities.name as facility_name',
    		'inventory_items.created_at')
    	-> get();
    	
    	return $query;
    }

    public function getListOfUpkeepStatusMovement($data)
    {
    	$from   	 = $data['from'];
    	$to     	 = $data['to'];
    	$status 	 = $data['status'];
    	$facility_id = $data['facility_id'];

    	$recent_date = \DB::table('monitoring_upkeep')->select('created_at')
    	->orderBy('created_at','desc')
    	->first();


    	$query = \DB::table('monitoring_upkeep')
    	-> join('inventory_items','inventory_items.id','=','monitoring_upkeep.upkeep_id')
		-> join('upkeep_status','upkeep_status.id','=','monitoring_upkeep.upkeep_status_id') 
		-> join('facilities','facilities.id','=','monitoring_upkeep.facility_id') 
		-> whereNull('monitoring_upkeep.deleted_at')
		-> where(function($query) use ($facility_id,$from,$to,$status,$recent_date)
		{            
			if($facility_id)
			{
			  $query->where('monitoring_upkeep.facility_id',$facility_id);
			}				
			if($from && $to==false)
			{
			  $query->where('monitoring_upkeep.created_at',$from);				
			}
			if($to && $from==false)
			{
			  $query->where('monitoring_upkeep.created_at',$to);				
			}
			if($to && $from)
			{
			  $query->whereBetween('monitoring_upkeep.created_at',[$from,$to]);				
			}
			if($from ==false && $to==false)
			{
			  $query->where('monitoring_upkeep.created_at',$recent_date->created_at);								
			}
			if($status)
			{
			  $query->where('upkeep_status.name',$status);				
			}
		})
    	-> select(
    		'inventory_items.id',
    		'inventory_items.name',
    		'inventory_items.serial_no',
    		'inventory_items.description',
    		'inventory_items.category',
    		'upkeep_status.name as status',
    		'monitoring_upkeep.remarks',
    		'facilities.name as facility_name',
    		'inventory_items.created_at')
    	-> get();
    	
    	return $query;
    }

    public function getUpkeepItemsCount()
    {
    	/*$from   	 = $data['from'];
    	$to     	 = $data['to'];
    	$status 	 = $data['status'];
    	$facility_id = $data['facility_id'];*/


    	$query = \DB::table('inventory_items')
    	-> join('upkeep_facility','upkeep_facility.upkeep_id','=','inventory_items.id')
		-> whereNull('inventory_items.deleted_at')
		/*-> where(function($query) use ($facility_id,$from,$to,$status)
		{            
			if($facility_id)
			{
			  $query->where('monitoring_upkeep.facility_id',$facility_id);
			}				
			if($from && $to==false)
			{
			  $query->where('monitoring_upkeep.created_at',$from);				
			}
			if($to && $from==false)
			{
			  $query->where('monitoring_upkeep.created_at',$to);				
			}
			if($to && $from)
			{
			  $query->whereBetween('monitoring_upkeep.created_at',[$from,$to]);				
			}
		})*/
    	-> select('inventory_items.name',DB::raw('count(upkeep_id) as count'))
    	-> groupBy('upkeep_id')
    	-> get();
    	
    	return $query;
    }


   public function getRecentStatus($inventory_id)
	{

		$query = \DB::table('monitoring_inventory')
		->join('inventory_status','inventory_status.id','=','monitoring_inventory.inventory_status_id')
		->select('inventory_status.name')
		->where('inventory_id',$inventory_id)
		->distinct('facility_id')
		->orderBy('monitoring_inventory.created_at','desc')
		->first();
		$result = 'N/A';
		if($query)
		{
			$result = $query->name;
		}

		return $result;
	}

    public function countStatus($query)
    {
    	$list = array();
    	$total = null;
    	$count = 1;
    	if($query){
	    	foreach($query as $key => $data)
	    	{

	    		if(!in_array($data->status,$list))
	    		{
	    			$list [$key] = $data->status;
					$total[$data->status]= $count;
	    		}
	    		else
	    		{	
	    			$count++;
	    			$total[$data->status] = $count;
	    		}    		
	    	}
	    	array_values($total);
	    }
    	return $total;
    }
	    


    

    public function getListOfFacility($category_id=false)
    {
    	$query = \DB::table('facilities')
    	-> join('categories','categories.id','=','facilities.category_id')
    	-> select('facilities.name as facility_name','facilities.id','facilities.description','categories.name as category','facilities.created_at')
    	-> whereNull('facilities.deleted_at')
		-> where(function($query) use ($category_id)
			 {            
				if($category_id)
				{
				  $query->where('facilities.category_id',$category_id);
				}				
			 })
    	-> get();
    	return $query;
    }

    public function getInventoryDetailsByFacilityId($facility_id)
    {
		$query = \DB::table('inventory_items')
		->select    ('id','name','serial_no','description','category','created_at')
		->whereNull ('deleted_at')
		->where('facility_id',$facility_id)
		->where('inventory_type','inventory')
		->get();
		$inventories;
    	foreach ($query as $key => $value)
    	{
    		$inventories [$key]['id'] 			 = $value->id;
    		$inventories [$key]['name'] 		 = $value->name;
    		$inventories [$key]['serial_no'] 	 = $value->serial_no;
    		$inventories [$key]['description']   = $value->description;
    		$inventories [$key]['category'] 	 = $value->category;
    		$inventories [$key]['recent_status'] = $this->getRecentStatus($value->id);
    	}

	    	
    	return $inventories;
    }



  public function getInventoryList($inventory_id)
    {
    	$inventories = \DB::table('inventories_item')
    	->select('inventories_item.id','inventories_item.name','inventories_item.serial_no','category')
    	->get();

    }

	public function getListOfRoomsByFacility($facility_id)
	{
		$query = \DB::table('facilities')
		->join('room_facilities','facility_id','=','facilities.id')
		->join('rooms','rooms.id','=','room_facilities.room_id')
		->whereNull('facilities.deleted_at')
		->whereNull('rooms.deleted_at')
		->whereNull('room_facilities.deleted_at')
		->select('rooms.id','rooms.room_name')
		->distinct('rooms.id')
		->get();
		$listOfRooms=null;
		if($query)
		{
			foreach ($query as $key => $value)
		    {
				$listOfRooms['listOfRooms'][$key]['id']   = $value->id ;
				$listOfRooms['listOfRooms'][$key]['name'] = $value->room_name; 

			}
		}
		return $listOfRooms;
	}

	public function getListOfFacilityBasedOnStatus($facility_id,$status=false)
	{
		$query = \DB::table('facilities')
		->join('monitoring_room_facilities','facility_id','=','facilities.id')
		->join('rooms','rooms.id','=','room_id')
		->join('monitorings','monitorings.id','=','monitoring_id')
		->whereNull('monitorings.deleted_at')
		// ->where('monitoring_room_facilities.status',$status)
		->where('facilities.id',$facility_id)
		->whereNull('facilities.deleted_at')
		->distinct('monitoring_id')
		->orderBy('monitoring_room_facilities.created_at','desc')
		->orderBy('monitoring_id','desc')

		->select('facility_name','monitoring_room_facilities.status','rooms.room_name','monitorings.name','monitorings.created_at')
		->get();
		
		$listOfFacility = null;
		if($query)
		{
			foreach ($query as $key => $value)
			{
				$listOfFacility[$key]['monitoring_name'] = $value->name;
				$listOfFacility[$key]['status'] = $value->status;
				$listOfFacility[$key]['room_name'] = $value->room_name;
				$listOfFacility[$key]['date'] = $value->created_at;
			}

		}
		return $listOfFacility;
	}

	public function getAllFacility()
	{
		$query = \DB::table('facilities')
		-> join('room_facilities','room_facilities.facility_id','=','facilities.id')
		-> whereNull('facilities.deleted_at')
		-> distinct('facilities.id')
		-> select('facilities.id','facility_name')
		-> get();
		return $query;
	
	}

	public function getFacilityListReport($data)
	{	
		$from 	= $data['from'];
		$to		= $data['to'];

		$query = \DB::table('facilities')
		-> whereNull('facilities.deleted_at')
		-> select('id','facility_name')
		-> get();
		return $query;
	
	}

	public function FacilityListReport($data=false)
	{
		$status 	= $data['status'];
		
		if($data){
			$facilities = $this->getFacilityListReport($data);
		}else{
			$facilities = $this->getAllFacility();
		}

		$reports;
		$facility_list;
		foreach ($facilities as $key => $value)
		{
			$facility_list[$key]['id']   = $value->id;
			$facility_list[$key]['name'] = $value->facility_name; 
			$facility	=$this->getListOfFacilityBasedOnStatus($value->id,false);
			$facility_list[$key]['info'] = $facility; 
			
		}

		return $facility_list;
	}


}