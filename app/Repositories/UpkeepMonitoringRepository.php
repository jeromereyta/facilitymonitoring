<?php

namespace App\Repositories;

use App\Models\Inventory;
use Request, Input,Image;
use Auth;

class UpkeepMonitoringRepository
{

	public function create($data)
	{
		$result = null;
		$name 	= \DB::table('monitorings')
		->where('name',$data['name'])
		->whereNull('deleted_at')
		->count();

		if($name)
		{
			$result = 'This monitoring name already exist';
		} else
		  {
			$create = \DB::table('monitorings')
			->insert([
				'name' 			  => $data['name'],
				'description' 	  => $data['description'],
				'monitoring_type' => 'Upkeep Monitoring',
				'created_by'  	  => Auth::user()->id,
				'created_at'  	  => date('Y-m-d H:i:s')
			]);

			$upkeepMonitoring = $this->CreateUpkeepMonitoring($data);		
			$result 			 = 'Upkeep Monitoring Sheet Created!';
		  }
		  return $result;
	}

	public function CreateUpkeepMonitoring($data)
	{
		$upkeepMonitoring = $data['data'] ;

		foreach ($upkeepMonitoring as $facility_name => $facility) 
		{
			foreach ($facility as $upkeep_name => $upkeep) 
			{
				$query = \DB::table('monitoring_upkeep')
				->insert([
					'monitoring_id' 	  	=>$this->getMonitoringIdByName($data['name']), 
					'facility_id'		  	=>$this->getFacilityIdByName($facility_name),
					'upkeep_id'  	  		=>$this->getUpkeepItemIdByName($upkeep_name),
					'upkeep_status_id' 		=>'1',
					'remarks'			  	=>$upkeep['remarks'],
					'created_at' 		  	=>date('Y-m-d H:i:s')
					]);		
			}
		}

	}

	public function getMonitoringIdByName($name)
	{
		return \DB::table('monitorings')
		->where('name',$name)
		->select('id')
		->get()[0]->id;
	}

	public function getMonitoringIdByDate($start,$end)
	{
		return \DB::table('monitorings')
		->whereBetween('monitorings.created_at',[$start,$end])
		->select('created_at','name','id')
		->where('monitoring_type','Upkeep Monitoring')
		->groupBy('created_at')
		->get();
	}

	public function getFacilityIdByName($name)
	{
		return \DB::table('facilities')
		->where('name',$name)
		->select('id')
		->get()[0]->id;
	}


	public function getUpkeepItemIdByName($name)
	{
		return \DB::table('inventory_items')
		->where('name',$name)
		->select('id')
		->get()[0]->id;

	}
	public function getListOfUpkeepMonitoringByDate($start,$end)
	{
		return \DB::table('monitorings')
		->leftjoin('users','users.id','=','monitorings.created_by')
		->whereNull('monitorings.deleted_at')
		->whereBetween('monitorings.created_at',[$start,$end])
		->where('monitoring_type','Upkeep Monitoring')
		->select('monitorings.id','monitorings.name','monitorings.description','users.name as created_by','monitorings.created_at')
		->orderBy('monitorings.created_at','desc')
		->first();
	}

	public function getFacilityDetails()
	{
		$upkeep_monitorings = null;
		$facilities 	       = $this->getFacilityList();
		
		foreach ($facilities as $key => $facility) 
		{
			$upkeep_monitorings[$facility->name]['id']= $facility->id;
			$upkeep_monitorings[$facility->name]['room_type']= $facility->room_type;
			$upkeep_monitorings[$facility->name]['description']= $facility->description;
			$upkeep_monitorings[$facility->name]['inventories']=$this->getUpkeepItemsInAFacility($facility->id);	
		}
		
		return $upkeep_monitorings;
	}

	public function getFacilityList()
	{
		$facilities = \DB::table('facilities')
		->join('inventory_items','facility_id','=','facilities.id')
		-> select('facilities.id','facilities.name','room_type','facilities.description','facilities.created_at')
		-> whereNull ('facilities.deleted_at')
		-> get();
		return $facilities;
	}

	public function get($id)
	{
		$monitoring = \DB::table('monitorings')
		->leftjoin('users','users.id','=','monitorings.created_by')
		->whereNull('monitorings.deleted_at')
		->where('monitoring_type','Upkeep Monitoring')
		->where('monitorings.id',$id)
		->select('monitorings.id','monitorings.name','monitorings.description','users.name as created_by','monitorings.created_at')
		->get();

		$monitoringDetails 	   = null;
		$inventory_monitorings = null;
		$facilities 	       = $this->getFacilityList();


		foreach ($monitoring as $key => $value)
		 {
     		$monitoringDetails[$key]['name'] = $value->name;
     		$monitoringDetails[$key]['id'] = $value->id;

     		$monitoringDetails[$key]['description'] = $value->description;
     		$monitoringDetails[$key]['created_by'] = $value->created_by;
     		$monitoringDetails[$key]['created_at'] = $value->created_at;
     					
			$facility_details = $this->getFacilityDetailsByMonitoringId($value->id);
			
			foreach ($facility_details as $index => $facility)
			{
	     		$upkeep_monitorings[$facility->name]['id']= $facility->id;
				$upkeep_monitorings[$facility->name]['room_type']= $facility->room_type;
				$upkeep_monitorings[$facility->name]['description']= $facility->description;
				$upkeep_monitorings[$facility->name]['inventories']=$this->getUpkeepItemsInMonitoring($facility->id,$value->id);
				
			}
			$monitoringDetails[$key]['facilities'] = $upkeep_monitorings;
		}

		return $monitoringDetails;
	}

	public function getFacilityDetailsByMonitoringId($id)
	{
		$facilities = \DB::table('monitoring_upkeep')
		-> join('facilities','facilities.id','=','monitoring_upkeep.facility_id')
		-> select('facilities.id','facilities.name','room_type','facilities.description','facilities.created_at')
		-> where('monitoring_upkeep.monitoring_id',$id)
		-> get();

		return $facilities;		

	}

	public function getUpkeepItemsInAFacility($facility_id)
	{

		$inventories = \DB::table('inventory_items')
		->select('inventory_items.id','inventory_items.name as inventory_name','serial_no','description','category','inventory_items.created_at')
		->join('upkeep_facility','upkeep_facility.upkeep_id','=','inventory_items.id')
		->whereNull('inventory_items.deleted_at')
		->where('upkeep_facility.facility_id',$facility_id)
		->where('inventory_items.inventory_type','upkeep')
		->get();

	
		$getInventoriesInAFacility = null;
		foreach($inventories as $key => $inventory)
		{
			$getInventoriesInAFacility[$key]['id']= $inventory->id;
			$getInventoriesInAFacility[$key]['inventory_name']= $inventory->inventory_name;
			$getInventoriesInAFacility[$key]['serial_no']= $inventory->serial_no;
			$getInventoriesInAFacility[$key]['description']= $inventory->description;
			$getInventoriesInAFacility[$key]['category']= $inventory->category;
			$getInventoriesInAFacility[$key]['created_at']= $inventory->created_at;
		}
		return $getInventoriesInAFacility;	
	}

	public function getUpkeepItemsInMonitoring($facility_id,$monitoring_id)
	{
		$upkeeps = \DB::table('monitoring_upkeep')
		->leftjoin('inventory_items','inventory_items.id','=','monitoring_upkeep.upkeep_id')
		->join('upkeep_status','upkeep_status.id','=','monitoring_upkeep.upkeep_status_id')
		->select('inventory_items.id',
				 'inventory_items.name as inventory_name',
				 'serial_no',
				 'description',
				 'category',
				 'inventory_items.created_at',
				 'upkeep_status.name as status',
				 'inventory_items.is_remarks_required',
				 'monitoring_upkeep.remarks')
		->whereNull('inventory_items.deleted_at')
		->where('monitoring_upkeep.monitoring_id',$monitoring_id)
		->where('monitoring_upkeep.facility_id',$facility_id)
		->distinct('inventory_items.name')
		->get();
		
		$getUpkeepItemsInAFacility = null;
		foreach($upkeeps as $key => $upkeep)
		{

			$getUpkeepItemsInAFacility[$key]['id']= $upkeep->id;
			$getUpkeepItemsInAFacility[$key]['inventory_name']= $upkeep->inventory_name;
			$getUpkeepItemsInAFacility[$key]['serial_no']= $upkeep->serial_no;
			$getUpkeepItemsInAFacility[$key]['description']= $upkeep->description;
			$getUpkeepItemsInAFacility[$key]['category']= $upkeep->category;
			$getUpkeepItemsInAFacility[$key]['remarks']= $upkeep->remarks;
			$getUpkeepItemsInAFacility[$key]['status']= $upkeep->status;
			$getUpkeepItemsInAFacility[$key]['is_remarks_required']= $upkeep->is_remarks_required;
			$getUpkeepItemsInAFacility[$key]['created_at']= $upkeep->created_at;
		}

		return $getUpkeepItemsInAFacility;
	}

	public function getUpkeepStatus()
	{
		$status = \DB::table('upkeep_status')
		->select('id','name as status_name')
		->whereNull('deleted_at')
		->get();
		return $status;
	}

	public function delete($id)
	{
		$query = \DB::table('monitorings')
		->where('id',$id)
		->update([
			'deleted_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);
	}

	public function getStatusIdByName($name)
	{
		$query= \DB::table('upkeep_status')
		->where('name',$name)
		->select('id')
		->get();

		if($query){
			$query = $query[0]->id;
		}

		return $query;

	}

	public function updateUpkeepMonitoringStatus($upkeep_monitoring_id,$facility_id,$upkeep_id,$status)
	{

			$query = \DB::table('monitoring_upkeep')
				->where('monitoring_id',$upkeep_monitoring_id)
				->where('facility_id',$facility_id)
				->where('upkeep_id',$upkeep_id)
				->update([
					'upkeep_status_id' =>$this->getStatusIdByName($status),
					'updated_at' 		  =>date('Y-m-d H:i:s')
					]);		
	}

	public function updateUpkeepMonitoringRemarks($upkeep_monitoring_id,$facility_id,$upkeep_id,$remarks)
	{
			$query = \DB::table('monitoring_upkeep')
				->where('monitoring_id',$upkeep_monitoring_id)
				->where('facility_id',$facility_id)
				->where('upkeep_id',$upkeep_id)
				->update([
					'remarks' => $remarks,
					'updated_at' =>date('Y-m-d H:i:s')
					]);		
	}

}