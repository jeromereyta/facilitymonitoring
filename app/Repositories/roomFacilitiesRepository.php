<?php

namespace App\Repositories;

use App\Models\roomFacilities;
use InfyOm\Generator\Common\BaseRepository;

class roomFacilitiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'room_id',
        'facility_id',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return roomFacilities::class;
    }

    public function updateRoomFacility($room_id,$roomName,$facilities){

        $query = \DB::table('room_facilities')
        ->where('room_id',$room_id)
        ->delete();
        if($facilities){       
            foreach($facilities as $facility){
                $query = \DB::table('room_facilities')
                ->insert([
                    'room_id' => $room_id,
                    'facility_id'=> $facility,
                    'created_at'=>'now()',
                    'updated_at'=>'now()']);
        }
    }
        //create or
  
        

        $updateRoom = \DB::table('rooms')
        ->where('id',$room_id)
        ->update([
            'room_name'  => $roomName,
            'updated_at' => 'now()'
            ]);

    }




    public function getFacilityNotInRoomId($id){

        $roomFacility = $this->getRoomFacilitiesByRoomId($id);
        $facility =[];

            foreach ($roomFacility as $key => $value) {
                    $facility[$key] = $value['id'];
            }

        $query =\DB::table('facilities')
        ->whereNotIn('id',$facility)
        ->select('id','facility_name')
        ->whereNull('deleted_at')
        ->get();

        $query = json_decode(json_encode($query));

        $facilities = [];
            foreach ($query as $index => $value) {
                    $facilities[$index]['id']    = $value->id;
                    $facilities[$index]['name']  = $value->facility_name;                    
                    $facilities[$index]['value'] = null;
        }

        return $facilities;
    }

    public function getRoomNameById($id){
        $rooms = \DB::table('rooms')
        ->select('room_name')
        ->whereNull('deleted_at')
        ->where('id',$id)
        ->get();
        $rooms = $rooms[0]->room_name;

        return $rooms;

    }


    public function getFacilitiesInMonitoringSheetsUsingRoomId($id){
        //get facility ids
        $query = \DB::table('monitoring_sheets')
        ->whereNull('deleted_at')
        ->where('room_id',$id)
        ->select('facilities')
        ->get();
        //convert to array
        $facility_ids = [];
        foreach ($query as $key => $value) {
            $facility_ids=json_decode($value->facilities);
        }

        $facilities = [];
        foreach ($facility_ids as $index => $data) {
            $facility = \DB::table('facilities')
            ->where('id',$data)
            ->select('id','facility_name')
            ->get();

            $facilities [$index]['id']   = $facility[0]->id;
            $facilities [$index]['name'] = $facility[0]->facility_name;
        }
        return $facilities;

    }
    public function getRoomFacilitiesByRoomId($id){

        $query = \DB::table('room_facilities')
        ->leftjoin('rooms','rooms.id','=','room_facilities.room_id')
        ->join('facilities','facilities.id','=','room_facilities.facility_id')
        ->select('room_id','room_name','facility_id','facility_name')
        ->where('rooms.id',$id)
        ->get();

        $roomFacility = [];
        foreach($query as $value =>$index){
            $roomFacility[$value]['id']    =$index->facility_id;
            $roomFacility[$value]['name']  =$index->facility_name;            
            $roomFacility[$value]['value'] = 'checked';
        }

        return $roomFacility;
    }


}
