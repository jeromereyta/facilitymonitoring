<?php

namespace App\Repositories;


class StatusRepository
{

	public function getListOfInventoryStatus()
	{
		$query = \DB::table('inventory_status')
		->select('id','name')
		->whereNull('deleted_at')
		->get();

		return $query;
	}

	public function getListOfUpkeepStatus()
	{
		$query = \DB::table('upkeep_status')
		->select('id','name')
		->whereNull('deleted_at')
		->get();

		return $query;
	}

	public function get($id)
	{
		$query = \DB::table('inventory_status')
		->select('id','name')
		->whereNull('deleted_at')
		->where('id',$id)		
		->get();

		return $query;
	}

	public function create($data)
	{
		$name = \DB::table('inventory_status')
		->where('name',$data['name'])
		->count();
		$result = false;

		if(!$name)
		{
		$query =\DB::table('inventory_status')
		->insert([
			'name'=>$data['name'],
			'created_at'=>date('Y-m-d H:i:s')
			]);
		$result = true;
		}
	
		return $result;
	}

	public function create_upkeep($data)
	{
		$name = \DB::table('upkeep_status')
		->where('name',$data['name'])
		->count();
		$result = false;

		if(!$name)
		{
		$query =\DB::table('upkeep_status')
		->insert([
			'name'=>$data['name'],
			'created_at'=>date('Y-m-d H:i:s')
			]);
		$result = true;
		}
	
		return $result;
	}


}
