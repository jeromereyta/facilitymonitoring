<?php

namespace App\Repositories;

use App\Models\monitorings;
use InfyOm\Generator\Common\BaseRepository;

class monitoringsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
 

    

    public function model()
    {
        return monitorings::class;
    }




    public function getRoomsWithFacilities($id){

        $query =\DB::table('monitoring_sheets')
        ->select('monitoring_id','room_id','facilities')
        ->where('monitoring_id',$id)
        ->whereNull('deleted_at')
        ->get();
        
        $monitorings = [];

        foreach($query as $index =>$value){
            //convert string to array to disect facilities
            $temporaryFacilities             = json_decode($value->facilities);
            $storage =[];
            
            foreach($temporaryFacilities as $x =>$facility){
    
                $storage[$x]=$facility;
            }

            $monitorings [$index]['id']         = $value->monitoring_id;
            $monitorings [$index]['room_id']    = $value->room_id;
            $monitorings [$index]['facilities'] = $storage;

        }

            return $monitorings;

    }


    public function listOfRooms(){
        $query = \DB::table('rooms')->whereNull('deleted_at')->get();
        $rooms = [];
        foreach($query as $index => $value){
            
            $rooms[$index]['id']         = $value->id;
            $rooms[$index]['name']       = $value->room_name; 
            $rooms[$index]['created_at'] = $value->created_at;
        }

        return $rooms;
    }


    public function updateMonitorings($data)
    {
        foreach ($data as $monitorings_name => $value) 
        {
            // create new monitorings
            $insertMonitoring = \DB::table('monitorings')
            ->whereNull('deleted_at')
            ->insert([
                'name'       => $monitorings_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);


            foreach ($value as $room_name => $room_data) 
            {
             $monitoringId = \DB::table('monitorings')->max('id');
                
                foreach ($room_data['facilities'] as $facility)
                {
                    
                 $query = \DB::table('monitoring_room_facilities')
                 ->insert([
                  'monitoring_id'      => $monitoringId,  
                  'room_id'            => $room_data['id'],
                  'facility_id'        => $facility['id'],
                  'status'             => $facility['status'], 
                  'remarks'            => $facility['remarks'],
                  'created_at'         => date('Y-m-d H:i:s')
                  ]);        
                }
            }
        }
    }

    public function updateMonitoringRoomFacilities($monitorings,$facility,$remarks)
    {
        dd($monitorings);
        $monitoring_name = \DB::table('monitorings')->whereNull('deleted_at')->where('name',$monitorings['name'])->select('name')->get();
        dd($monitoring_name);

                $query = \DB::table('monitoring_room_facilities')
                ->insert([
                 'monitoring_id'      => $monitoringId,  
                 'room_id'            => $rooms_id,
                 'facility_id'        => $facility_id,
                 'status'             => $status, 
                 'remarks'            => $$remarks,
                 'created_at'         => date('Y-m-d H:i:s')
                 ]);        
    }


    public function getRoomFacilitiesByRoomId($id,$monitoring_id){

        $listOfFacilities = \DB::table('monitoring_sheets')
        ->whereNull('deleted_at')
        ->where('room_id',$id)
        ->where('monitoring_id',$monitoring_id)
        ->select('list_of_facilities')
        ->get();

        if(isset($listOfFacilities[0])){
            $listOfFacilities = json_decode($listOfFacilities[0]->list_of_facilities);    
        }
        

        $roomFacility = [];

        foreach ($listOfFacilities as $key => $facility_id) {
            $facility = \DB::table('facilities')
            ->where('id',$facility_id)
            ->select('id','facility_name')
            ->get();

            $roomFacility[$key]['id']    =$facility[0]->id;
            $roomFacility[$key]['name']  =$facility[0]->facility_name;            
            $roomFacility[$key]['value'] = 'checked';

        }

        // $query = \DB::table('monitoring')
        // ->leftjoin('rooms','rooms.id','=','room_facilities.room_id')
        // ->join('facilities','facilities.id','=','room_facilities.facility_id')
        // ->select('room_id','room_name','facility_id','facility_name')
        // ->where('rooms.id',$id)
        // ->get();

        // $roomFacility = [];
        // foreach($query as $value =>$index){
        //     $roomFacility[$value]['id']    =$index->facility_id;
        //     $roomFacility[$value]['name']  =$index->facility_name;            
        //     $roomFacility[$value]['value'] = 'checked';
        // }

        return $roomFacility;
    }

    public function getFacilityNotInRoomId($id,$monitoring_id){

        $roomFacility = $this->getRoomFacilitiesByRoomId($id,$monitoring_id);
        $facility =[];

            foreach ($roomFacility as $key => $value) {
                    $facility[$key] = $value['id'];
            }

        $query =\DB::table('facilities')
        ->whereNotIn('id',$facility)
        ->select('id','facility_name')
        ->get();

        $query = json_decode(json_encode($query));

        $facilities = [];
            foreach ($query as $index => $value) {
                    $facilities[$index]['id']    = $value->id;
                    $facilities[$index]['name']  = $value->facility_name;                    
                    $facilities[$index]['value'] = null;
        }

        return $facilities;
    }

    public function getMonitoringIdByName($name)
    {
        $query = \DB::table('monitorings')
        ->whereNull('deleted_at')
        ->where('name',$name)
        ->select('id')
        ->get();

        return $query[0]->id;
    }




    public function getMonitoringSheets($monitoring_id)
    {    
        // get all the list of rooms 
         $roomsInMonitoringSheets  =  \DB::table('monitoring_room_facilities')
         // ->select('monitoring_id','room_id','facility_id','status','remarks')
         ->select('room_id')
         ->where('monitoring_id',$monitoring_id)
         ->distinct()
         ->get();
         $rooms;
         $facilities;

         foreach ($roomsInMonitoringSheets as $key => $value) {             

           $room_name =\DB::table('rooms')->select('room_name')->where('id',$value->room_id)->get()[0]->room_name;
           $roomData  =\DB::table('monitoring_room_facilities')
           -> select('monitoring_id','room_id','facility_id','status','remarks')
           -> where('monitoring_id',$monitoring_id)
           -> where('room_id',$value->room_id)
           -> get();         
            $rooms [$room_name]['id']=$value->room_id;
            $rooms [$room_name]['name']=$room_name;

           foreach ($roomData as $index => $data)
           {

               $facility_name =\DB::table('facilities')->select('facility_name')->where('id',$data->facility_id)->get()[0]->facility_name;
                $facilities[$index]['id']=$data->facility_id;   
                $facilities[$index]['name']=$facility_name;    
                $facilities[$index]['status']=$data->status;    
                $facilities[$index]['remarks']=$data->remarks;
            }
            $rooms[$room_name]['facilities']=$facilities;

         }

        return $rooms;

    }


    public function updateMonitoringSheets($monitorings,$monitoring_id)
    {
        foreach ($monitorings as $room_name => $room_data) 
        {   
            foreach ($room_data['facilities'] as $key => $value) 
            {   
                $query = \DB::table('monitoring_room_facilities')
                ->where('monitoring_id',$monitoring_id)
                ->where('room_id',$room_data['id'])
                ->where('facility_id',$value['id'])
                ->update([
                  'status'             => $value['status'], 
                  'remarks'            => $value['remarks'],
                  'updated_at'         => date('Y-m-d H:i:s')
                ]);
            }
        }
        //end of function
    }


    public function updateMonitoring($id,$name){
        $query = \DB::table('monitorings')
        ->where('id',$id)
        ->update([
            'name' => $name,
            'updated_at' =>date('Y-m-d H:i:s')
            ]);

    }

    public function getRoomsPerMonitoringId($monitoring_id){
          $roomsInMonitoringSheets  =  \DB::table('monitoring_sheets')
         ->select('room_id')
         ->where('monitoring_id',$monitoring_id)
         ->get();

         return $roomsInMonitoringSheets;

    }


    public function isMonitoringNameExist($monitoringName){
        $query = \DB::table('monitorings')
        -> where('name',trim($monitoringName))
        -> whereNull('deleted_at')
        -> get();
        return ($query)?true:false;
    }




}
