<?php

namespace App\Repositories;

use App\Role;
use App\Permission;

use Request, Input,Image;

class RoleRepository
{

	public function update($data)
	{
		$role = Role::find($data['id']);

		$permissions_id;
		$count=0;
		if($data['permissions']){
		foreach ($data['permissions'] as $permission_name => $value) 
		{
			if($value)
			{
				//clean all the role to rewrite its permission
				$query = \DB::table('permission_role')->where('role_id',$data['id'])->delete();
				//get the permission id then add connect to role
				$permission_id 		    = Permission::where('name',$permission_name)->select('id','name')->get();
				if(isset($permission_id[0]))
				{
					$permissions_id[$count] = $permission_id[0]->id;
					$count=$count+1; 
				}
			}
		}
		$role->givePermissionTo($permissions_id);

		}
	}
}
	
