<?php

namespace App\Repositories;

use App\Models\InventoryItems;
use Request, Input,Image;

class UpkeepItemsRepository
{

	public function search($params){

        if(isset($params['sort'])&&isset($params['order'])) {
            $sort = $params['sort'];
            $order = $params['order'];
        } else {
            $sort = 'inventory_items.name';
            $order = 'asc';
        }

        $upkeeps = \DB::table('facilities')
            ->rightjoin('inventory_items','inventory_items.facility_id','=','facilities.id')
            ->where('inventory_type','upkeep')
            ->where(function ($query) use ($params) {
                if(isset($params['searchQuery'])){
                    $query->where('inventory_items.name', 'like', '%'.$params['searchQuery'].'%');
                }
                if($params['page_type']==='template_list'){
                	$non_tagged = \DB::table('upkeep_facility')
                	->select('upkeep_id')
                	->whereNull('deleted_at')
                    ->where('facility_id',$params['facility_id']);
                	$query->whereNotIn('inventory_items.id',$non_tagged);
                }
                if($params['page_type']==='template_selected'){
                	
                	$query->whereIn('inventory_items.id', function($tagged) use ($params)
						    {
						        $tagged->select(\DB::raw('upkeep_id'))
						              ->from('upkeep_facility')
						              ->whereNull('deleted_at')
                                      ->where('facility_id',$params['facility_id']);
						    });
                }
            })
    
            ->select('inventory_items.name','inventory_items.id',
            	\DB::raw('(CASE WHEN inventory_items.deleted_at is NULL THEN "Active" ELSE "Deleted" END) AS status'))
            ->orderBy($sort,$order);
           /* if(isset($params['commodity_id'])){
                $subcommodities= $subcommodities->where('commodity_id', '=',$params['commodity_id']);
            }*/
            $upkeeps = $upkeeps->paginate(10);

        return $upkeeps;
    }
}