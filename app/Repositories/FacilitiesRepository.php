<?php

namespace App\Repositories;

use App\Models\facilities;
use InfyOm\Generator\Common\BaseRepository;

class facilitiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'facility_name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return facilities::class;
    }

    public function countFacilityInAllRooms($facility_id){

        $query = \DB::table('room_facilities')
        ->where('facility_id',$facility_id)
        ->whereNull('deleted_at')
        ->count();

        return $query;
    }

    public function getAllFacilities(){

        $query = \DB::table('facilities')->select('id','facility_name')
        ->whereNull('deleted_at')
        ->get();
        return $query;
    }

    public function isFacilityNameExist($facilityName){
        $query = \DB::table('facilities')
        -> where('facility_name',trim($facilityName))
        -> whereNull('deleted_at')
        -> get();

        return ($query)?true:false;
    }

    public function deleteFacilityinRoom($id){
        $query = \DB::table('room_facilities')
        -> where('facility_id',$id)
        -> delete();

    }

}
