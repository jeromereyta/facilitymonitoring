<?php

namespace App\Repositories;

use App\Models\Inventory;
use Request, Input,Image;

class CategoryRepository
{

	public function getCategories($type=false)
	{
		$query = \DB::table('categories')
		->select('id','name','description')
		->where('type',$type)		
		->whereNull('deleted_at')
		->paginate(10);

		return $query;
	}



}
	
