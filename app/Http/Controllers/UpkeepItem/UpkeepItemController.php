<?php

namespace App\Http\Controllers\UpkeepItem;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUpkeepItemRequest;
use App\Repositories\InventoryRepository;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\Controller;

class UpkeepItemController extends Controller
{
    
    protected $inventory;
    public function __construct(InventoryRepository $inventory)
    {

        $this->inventory=$inventory;
    	$this->middleware('auth');
    }

    /**
     * Display a listing of the rooms.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request,InventoryRepository $inventory)
    {
        $inventories = $inventory->getUpkeepItems();
        return view('upkeep_item.index')->with('inventories',$inventories);
    }

    /**
     * Show the form for creating a new rooms.
     *
     * @return Response
     */
    public function create()
    {
        $inventory =null;
        return view('upkeep_item.create');
    }

    /**
     * Store a newly created rooms in storage.
     *
     * @param CreateroomsRequest $request
     *
     * @return Response
     */
    public function store(CreateUpkeepItemRequest $request)
    {
        
        $input = $request->all();

        $query = $this->inventory->createUpkeepItem($input);
        $inventory =null;
        
        if(!strcmp('Added Successfully',$query))
        {
            Flash::success($query);
            return view('upkeep_item.index')->with('inventories',$this->inventory->getUpkeepItems());
        }
        else{
            Flash::error($query);
            return view('upkeep_item.create')->with('inventory',$this->inventory->getUpkeepItems());   
        }

    }



    public function show($id,InventoryRepository $inventory)
    {

        $item = $inventory->getUpkeepItem($id);

        if(empty($item))
        {
            Flash::error('Item not found!');
            return view('upkeep_item.index');
        }
        

        return view('upkeep_item.show')->with('inventory', $item);
    }



    public function edit($id,InventoryRepository $inventory)
    {
        $item = $inventory->getUpkeepItem($id);

        if (empty($item)) 
        {
            Flash::error('Room not found');
            return view('inventory.index');
        }

        return view('upkeep_item.edit')->with('inventory', $item);
    }



    public function update($id, Request $request,InventoryRepository $inventory)
    {
        $input = $request->all();
        $item  = $inventory->getUpkeepItem($id);
        
        if( $inventory->isNameExist($request->name) && strcmp($item[0]->name,$request->name))
        {
               Flash::error('Name already exist!');
               return view('upkeep_item.edit')->with('inventory',$inventory->getUpkeepItem($id));

        }
        else{
            $inventory->updateUpkeepItem($id,$input);
            return view('upkeep_item.index')->with('inventories',$inventory->getUpkeepItems());
        }
    }



    public function destroy($id,InventoryRepository $inventory)
    {
        $inventory->delete($id);
        $inventories = $inventory->getUpkeepItems();
        Flash::success('Item deleted successfully.');

        return view('upkeep_item.index')->with('inventories',$inventories);
    }

}
