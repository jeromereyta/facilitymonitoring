<?php

namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Flash;
use App\User;
use Response;
use App\Role;
use Gate;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Display a listing of the rooms.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request,UserRepository $user)
    {
                                        if(Gate::denies('user-read')) {
            return redirect('/');
        }

        $users = $user->getAll();
        return view('user.index')->with('users',$users);
    }

    /**
     * Show the form for creating a new rooms.
     *
     * @return Response
     */
    public function create(UserRepository $user)
    {
                                                if(Gate::denies('user-create')) {
            return redirect('/');
        }

        $user = null;

        return view('user.create')->with('users',$user)->with('roles',Role::all());
    }

    /**
     * Store a newly created rooms in storage.
     *
     * @param CreateroomsRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
                                                if(Gate::denies('user-create')) {
            return redirect('/');
        }

        
        $data = $request->all();



        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);     

        $user_id = \DB::table('users')->where('name',trim($data['name']))->select('id')->get();
        if(!isset($user_id[0]))
        {
            return view('users.create')->with('users',null)->with('roles',Role::all());
        }
               $user_id = $user_id[0]->id;
         

        $query = \DB::table('role_user')->insert(['role_id'=>$data['role_id'],
        'user_id'=>$user_id
        ]);    



        flash()->success('Successfully added new user.');
        
        return view('user.index')->with('users',User::all());

    }

    /**
     * Display the specified rooms.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(Gate::denies('user-read')) 
        {
            return redirect('/');
        }

        $roles = Role::all();
        $role = \DB::table('role_user')
        -> join('roles','roles.id','=','role_user.role_id')
        -> where('user_id',$id)
        ->select('roles.name')
        ->get();

        $user = User::find($id);
        return view('user.show')->with('users',$user)->with('role',$role)->with('roles',$roles);
    
    }


    /**
     * Show the form for editing the specified rooms.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(Gate::denies('user-update')) {
            return redirect('/');
        }

        $roles = Role::all();
        $role = \DB::table('role_user')
        -> join('roles','roles.id','=','role_user.role_id')
        -> where('user_id',$id)
        ->select('roles.name')
        ->get();
        
        $user = User::find($id);
        return view('user.edit')->with('users',$user)->with('role',$role)->with('roles',$roles);
    }

    /**
     * Update the specified rooms in storage.
     *
     * @param  int              $id
     * @param UpdateroomsRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        if(Gate::denies('user-update')) {
            return redirect('/');
        }
            $input= $request->all();
            $old_role = \DB::table('role_user')->where('user_id',$id)->select('role_id')->get()[0]->role_id;


            if(!$old_role ==$input['role_id'])
            {
                $query = \DB::table('role_user')->where('user_id',$id)
                -> update(['role_id'=>$input['role_id']]);   
            }

              $user = \DB::table('users')->where('id',$id)
                -> update(['name'=>$input['name']]);

                return view('user.index')->with('users',$users = User::all());
    }


    /**
     * Remove the specified rooms from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,UserRepository $user)
    {
        if(Gate::denies('user-delete')) {
            return redirect('/');
        }
        $deleteUser = \DB::table('users')->where('id',$id)->delete();        
        
        $users =$user->getAll();
                    Flash::success('User deleted!');

        return view('user.index')->with('users',$users);

    }
}
