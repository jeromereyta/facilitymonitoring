<?php

namespace App\Http\Controllers\Rooms;

use App\Http\Requests;
use App\Http\Requests\CreateroomsRequest;
use App\Http\Requests\UpdateroomsRequest;
use App\Repositories\roomsRepository;
use App\Repositories\facilitiesRepository;
use App\Repositories\roomFacilitiesRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\Controller;
class RoomController extends Controller
{
    /** @var  roomsRepository */
    private $roomsRepository;
    private $facilitiesRepository;
    private $roomFacilitiesRepository;

    public function __construct(roomsRepository $roomsRepo, facilitiesRepository $facilitiesRepo, roomFacilitiesRepository $roomFacilitiesRepo)
    {
        $this->roomsRepository = $roomsRepo;
        $this->roomFacilitiesRepository = $roomFacilitiesRepo;
        $this->facilitiesRepository = $facilitiesRepo;
    	$this->middleware('auth');


    }

    /**
     * Display a listing of the rooms.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomsRepository->pushCriteria(new RequestCriteria($request));
        $rooms = $this->roomsRepository->all();
        // dd($rooms);
        return view('rooms.index')
            ->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new rooms.
     *
     * @return Response
     */
    public function create()
    {
            $roomFacilities = [];
            $getAllFacility = $this->roomFacilitiesRepository->getFacilityNotInRoomId(0);
            foreach($getAllFacility as $index => $value){
            $roomFacilities[]=$value;

        }

        return view('rooms.create') ->with('roomFacilities',$roomFacilities);
    }

    /**
     * Store a newly created rooms in storage.
     *
     * @param CreateroomsRequest $request
     *
     * @return Response
     */
    public function store(CreateroomsRequest $request)
    {

        $input = $request->all();
        $isExist = $this->roomsRepository->isRoomNameExist($input['room_name']);
        
        if($isExist){
         $roomFacilities = [];
            $getAllFacility = $this->roomFacilitiesRepository->getFacilityNotInRoomId(0);
            foreach($getAllFacility as $index => $value){
            $roomFacilities[]=$value;

        }

            Flash::error('Room name already exist!');
            return view('rooms.create') ->with('roomFacilities',$roomFacilities);

        }


        $rooms = $this->roomsRepository->create($input);
        
        $roomFacilities = $this->roomFacilitiesRepository->updateRoomFacility($rooms->id,$input['room_name'],$request->facilities);
        Flash::success('Room saved successfully.');

        return redirect(route('rooms.index'));
    }

    /**
     * Display the specified rooms.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {


        $roomWithFacilities = [];
        $rooms = $this->roomsRepository->findWithoutFail($id);
        // $roomFacilities = $this->roomFacilitiesRepository->findWithoutFail($id);
        // $roomFacilities =json_decode(json_encode($roomFacilities));
        $facilities = $this->facilitiesRepository->all();
        $facilities = json_decode(json_encode($facilities));

        $roomFacilities       = [];
        $getRoomFacilities    = $this->roomFacilitiesRepository->getRoomFacilitiesByRoomId($id);

        foreach($getRoomFacilities as $index => $value){
            $roomFacilities[]=$value;
        }

        $getFacilityNotInRoom = $this->roomFacilitiesRepository->getFacilityNotInRoomId($id);
            foreach($getFacilityNotInRoom as $index => $value){
            $roomFacilities[]=$value;
        }
        // $rooms = $this->roomsRepository->findWithoutFail($id);
        if (empty($rooms)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        return view('rooms.show')->with('rooms', $rooms)->with('roomFacilities',$roomFacilities);
    }

    /**
     * Show the form for editing the specified rooms.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $roomWithFacilities = [];
        $rooms = $this->roomsRepository->findWithoutFail($id);
        // $roomFacilities = $this->roomFacilitiesRepository->findWithoutFail($id);
        // $roomFacilities =json_decode(json_encode($roomFacilities));
        $facilities = $this->facilitiesRepository->all();
        $facilities = json_decode(json_encode($facilities));

        $roomFacilities       = [];
        $getRoomFacilities    = $this->roomFacilitiesRepository->getRoomFacilitiesByRoomId($id);

        foreach($getRoomFacilities as $index => $value){
            $roomFacilities[]=$value;
        }

        $getFacilityNotInRoom = $this->roomFacilitiesRepository->getFacilityNotInRoomId($id);
            foreach($getFacilityNotInRoom as $index => $value){
            $roomFacilities[]=$value;
        }

                if (empty($rooms)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        return view('rooms.edit')->with('rooms', $rooms)
                                 ->with('facilities',$facilities)
                                 ->with('roomFacilities', $roomFacilities);
    }

    /**
     * Update the specified rooms in storage.
     *
     * @param  int              $id
     * @param UpdateroomsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateroomsRequest $request)
    {


        $rooms = $this->roomsRepository->findWithoutFail($id);
        $roomName = $request->room_name;
        if (empty($rooms)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }
        $currentRoomName = $this->roomsRepository->findWithoutFail($id)->room_name;

        if(strcasecmp($roomName,$currentRoomName)!=0){
            if($this->roomsRepository->isRoomNameExist($roomName)){
                
                    $roomWithFacilities = [];
                    $facilities         = $this->facilitiesRepository->all();
                    $facilities         = json_decode(json_encode($facilities));
                    $roomFacilities     = [];
                    $getRoomFacilities  = $this->roomFacilitiesRepository->getRoomFacilitiesByRoomId($id);

                    foreach($getRoomFacilities as $index => $value){
                        $roomFacilities[]=$value;
                    }

                    $getFacilityNotInRoom = $this->roomFacilitiesRepository->getFacilityNotInRoomId($id);
                        foreach($getFacilityNotInRoom as $index => $value){
                        $roomFacilities[]=$value;
                    }

                Flash::error('Room already exist.');
                return view('rooms.edit')->with('rooms', $rooms)
                                 ->with('facilities',$facilities)
                                 ->with('roomFacilities', $roomFacilities);

            }

        }



        // $roomFacilities = $this->roomFacilitiesRepository
        // ->update(['room_id'=>$request->room_id,'facility_id'=>json_encode
        //     ($request->facilities)], $id);
       
        $roomFacilities = $this->roomFacilitiesRepository
        ->updateRoomFacility($request->room_id,$roomName,$request->facilities);


        //$rooms = $this->roomsRepository->update($request->all(), $id);

        Flash::success('Room updated successfully.');

        return redirect(route('rooms.index'));
    }

    /**
     * Remove the specified rooms from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rooms = $this->roomsRepository->findWithoutFail($id);

        if (empty($rooms)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        $this->roomsRepository->delete($id);

        Flash::success('Room deleted successfully.');

        return redirect(route('rooms.index'));
    }
}
