<?php

namespace App\Http\Controllers\Search;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\SearchRequest;

use App\Repositories\InventoryRepository;
use App\Repositories\UpkeepItemsRepository;
use App\Repositories\UpkeepMonitoringRepository;
use App\Repositories\InventoryMonitoringRepository;
use App\Repositories\FacilityRepository;
use Gate,Input,Response,SortableColumn;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $inventories;
    protected $facilities;
    protected $upkeeps;
    protected $inventoryMonitorings;
  

    function __construct(
        InventoryRepository $inventories,
        FacilityRepository $facilities,
        UpkeepItemsRepository $upkeeps,
        InventoryMonitoringRepository $inventoryMonitorings,
        UpkeepMonitoringRepository $upkeepMonitorings
        )
    {
        $this->inventories = $inventories;
        $this->facilities = $facilities;
        $this->upkeeps = $upkeeps;
        $this->upkeepMonitorings = $upkeepMonitorings;
        $this->inventoryMonitorings = $inventoryMonitorings;
    }

    public function search($searchDomain,SearchRequest $request){
       
        if(strcmp($searchDomain,"inventories")==0){
            $list =$this->inventories->search($request->all());
            $html = view('search.modules.inventories.search', ['list'=>$list,'params'=>$request->all()])->render();
            return Response::json(['success' => true, 'html'=>$html]);
        }
        if(strcmp($searchDomain,"upkeep")==0){
            $list =$this->upkeeps->search($request->all());
            $html = view('search.modules.upkeeps.search', ['list'=>$list,'params'=>$request->all()])->render();
            return Response::json(['success' => true, 'html'=>$html]);
        }
        if(strcmp($searchDomain,"test")==0){
            $list =$this->upkeeps->search($request->all());
            $html = view('search.modules.test', ['list'=>$list,'params'=>$request->all()])->render();
            return Response::json(['success' => true, 'html'=>$html]);
        }
        if(strcmp($searchDomain,"inventory_facilities")==0){
            $list_facility          = $this->facilities->searchFacilities($request->all()); 
            $list_inventory_items   = $this->inventoryMonitorings->get($request->monitoring_id);
            $html = view('search.modules.monitorings.inventory.search', 
                ['list_inventory_items'=>$list_inventory_items,
                    'list_facility'=>$list_facility,
                    'params'=>$request->all()])->render();
            return Response::json(['success' => true, 'html'=>$html]);
        }
        if(strcmp($searchDomain,"upkeep_facilities")==0){
            $list_facility          = $this->facilities->searchFacilitiesByUpkeep($request->all());
            $list_upkeep_items      = $this->upkeepMonitorings->get($request->monitoring_id);
            $html = view('search.modules.monitorings.upkeep.search', 
                ['list_upkeep_items'=>$list_upkeep_items,
                    'list_facility'=>$list_facility,
                    'params'=>$request->all()])->render();
            return Response::json(['success' => true, 'html'=>$html]);
        }
    }
}