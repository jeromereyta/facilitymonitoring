<?php

namespace App\Http\Controllers\Status;

use App\Http\Requests;
use App\Repositories\InventoryRepository;
use App\Repositories\FacilityRepository;

use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Http\Controllers\Controller;

class UpkeepStatusController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function index(Request $request,StatusRepository $status)
    {       
        return view('upkeep_status.index')->with('status',$status->getListOfUpkeepStatus());
    }



    public function create(StatusRepository $status)
    {
        $facilities = $status->getListOfUpkeepStatus();
        return view('upkeep_status.create')->with('status',$status->getListOfUpkeepStatus());
    }

    public function store(Request $request,StatusRepository $status )
    {
         $input = $request->all();

         if(!$input['name'])
         {
            Flash::error('Name cannot be blank');
            return view('upkeep_status.index')->with('status',$status->getListOfUpkeepStatus());        
         }


         if(!$status->create_upkeep($input))
         {
           Flash::error('Name already exist');
           return view('upkeep_status.index')->with('status',$status->getListOfUpkeepStatus());
         }   

         Flash::success('Added new status');

         return view('upkeep_status.index')->with('status',$status->getListOfUpkeepStatus());
    }



    public function show($id,FacilityRepository $facility)
    {
        return view('upkeep_status.show')->with('status',$facility->get($id));
    }



    public function edit($id,FacilityRepository $facility)
    {
        $room = $facility->get($id);
        return view('upkeep_status.edit')->with('facility',$room);
    }

    

    public function update($id,Request $request,FacilityRepository $facility)
    {
        $input = $request->all();
        $item  = $facility->get($id);

        if(strcmp($request->name,$item[0]->name) && $facility->isFacilityNameExist($input['name']))
        {
           Flash::error('Name already exist!');
           return view('upkeep_status.edit')->with('facility',$facility->get($id));   
        }
        else{
            $facility->update($id,$input);
            Flash::success('Update Successfully');
            return view('upkeep_status.index')->with('facilities',$facility->getFacilities());
        }   


    }


    public function destroy($id,FacilityRepository $facility)
    {
        $facility->delete($id);

        Flash::success('Deleted successfully!');
        return view('facility.index');
    }
}
