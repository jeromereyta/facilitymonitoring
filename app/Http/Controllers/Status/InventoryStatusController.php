<?php

namespace App\Http\Controllers\Status;

use App\Http\Requests;
use App\Repositories\InventoryRepository;
use App\Repositories\FacilityRepository;

use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Http\Controllers\Controller;

class InventoryStatusController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }




    public function index(Request $request,StatusRepository $status)
    {       
        return view('inventory_status.index')->with('status',$status->getListOfInventoryStatus());
    }



    public function create(StatusRepository $status)
    {
        $facilities = $status->getListOfInventoryStatus();
        return view('inventory_status.create')->with('status',$status->getListOfInventoryStatus());
    }

    public function store(Request $request,StatusRepository $status )
    {
         $input = $request->all();

         if(!$input['name'])
         {
            Flash::error('Name cannot be blank');
            return view('inventory_status.index')->with('status',$status->getListOfInventoryStatus());        
         }


         if(!$status->create($input))
         {
           Flash::error('Name already exist');
           return view('inventory_status.index')->with('status',$status->getListOfInventoryStatus());
         }   

         Flash::success('Added new status');

         return view('inventory_status.index')->with('status',$status->getListOfInventoryStatus());
    }



    public function show($id,FacilityRepository $facility)
    {
        return view('inventory_status.show')->with('status',$facility->get($id));
    }



    public function edit($id,FacilityRepository $facility)
    {
        $room = $facility->get($id);
        return view('inventory_status.edit')->with('facility',$room);
    }

    

    public function update($id,Request $request,FacilityRepository $facility)
    {
        $input = $request->all();
        $item  = $facility->get($id);

        if(strcmp($request->name,$item[0]->name) && $facility->isFacilityNameExist($input['name']))
        {
           Flash::error('Name already exist!');
           return view('facility.edit')->with('facility',$facility->get($id));   
        }
        else{
            $facility->update($id,$input);
            Flash::success('Update Successfully');
            return view('facility.index')->with('facilities',$facility->getFacilities());
        }   


    }


    public function destroy($id,FacilityRepository $facility)
    {
        $facility->delete($id);

        Flash::success('Deleted successfully!');
        return view('facility.index');
    }
}
