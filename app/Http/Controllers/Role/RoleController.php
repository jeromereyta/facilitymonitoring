<?php

namespace App\Http\Controllers\Role;

use App\Http\Requests;
use App\Http\Requests\CreateRoleRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use Validator;
use Illuminate\Http\Request;
use Flash;
use App\User;
use App\Role;
use Input;
use App\Permission;
use Response;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    protected $role;
    public function __construct(RoleRepository $role)
    {
    	$this->middleware('auth');
        $this->role =$role;
    }

    /**
     * Display a listing of the rooms.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request,UserRepository $user)
    {
        return view('roles.index')->with('roles',Role::all());
    }

    /**
     * Show the form for creating a new rooms.
     *
     * @return Response
     */
    public function create()
    {
        $role = null;
     
        $permissions = Permission::select('name')->get();
        $permission_list=[];
        $disable =false;
        
        return view('roles.create')->with('role',$role)->with('permissions',$permission_list)->with('disable',$disable);
    }

    /**
     * Store a newly created rooms in storage.
     *
     * @param CreateroomsRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data    = $request->all();

        $isExist = \DB::table('roles')->where('name',trim($data['name']))->count();
        
        if(!isset($data['permissions']))
        {
            $data['permissions'] = null;
        }
        if(!$data['name'])
        {
            Flash::error('Name should not be empty');
            return view('roles.create')->with('role',null)->with('permissions',[]);
        }

        if($isExist)
        {
            Flash::error('Role name already exist');
            return view('roles.create')->with('role',null)->with('permissions',[])->with('disable',false);
        }

        Role::create(['name' => $data['name']]);     

        $id         = \DB::table('roles')->select('id')->where('name',$data['name'])->get()[0]->id;
        $data['id'] = $id;

        $this->role->update($data);
           
        flash()->success('Successfully added new role.');
        
        return view('roles.index')->with('roles',Role::all());

    }


    /**
     * Display the specified rooms.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $permissions = array_pluck($role->permissions, 'name');

        return view('roles.show')->with('role',$role)->with('permissions',$permissions);

    }

    /**
     * Show the form for editing the specified rooms.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = array_pluck($role->permissions, 'name');
        
        return view('roles.edit')->with('role',$role)->with('permissions',$permissions);
    }

    /**
     * Update the specified rooms in storage.
     *
     * @param  int              $id
     * @param UpdateroomsRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();
        $input['id'] = $id;
        $old_role = Role::find($id);

                
        $count = $old_role->count();
        if(strcmp($request->name,$old_role->name) && $count)
        {
           $permissions = array_pluck($old_role->permissions, 'name');
           Flash::error('Name already exist!');
           return view('roles.edit')->with('role',Role::find($id))->with('permissions',$permissions);   
        }
        else{

            if(!isset($input['permissions'])){
                $input['permissions'] = null;
            }   
            $this->role->update($input);
            Flash::success('Updated Successfully');
            return view('roles.index')->with('roles',Role::all());
        }   
    }


    /**
     * Remove the specified rooms from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {         
        $role = \DB::table('roles')->where('id',$id)->delete();

        Flash::success('Deleted Successfully');
        return view('roles.index')->with('roles',Role::all());
    }
}
