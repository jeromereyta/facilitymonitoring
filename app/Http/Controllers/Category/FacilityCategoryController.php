<?php

namespace App\Http\Controllers\Category;

use App\Http\Requests;
use App\Http\Requests\FacilityRequest;
use App\Repositories\InventoryRepository;
use App\Repositories\FacilityRepository;
use App\Repositories\CategoryRepository;

use App\Models\Category;
use App\Models\Facilities;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Http\Controllers\Controller;
use Input;

class FacilityCategoryController extends Controller
{
    protected $category;
    public function __construct(FacilityRepository $facility,CategoryRepository $category)
    {
    	$this->middleware('auth');
        $this->facility = $facility;
        $this->category = $category;
    }


    public function index(Request $request,CategoryRepository $category)
    {       
        return view('category_facility.index')->with('categories',$this->category->getCategories('room_type'));
    }


    public function create(CategoryRepository $category)
    {
        return view('category_facility.create')->with('category',null);
    }


    public function store(Request $request,CategoryRepository $category)
    {

         $input = $request->all();
         $category=Category::where('name',$input['name'])->count();

         if(!$input['name'])
         {
            Flash::error('Name cannot be blank');
            return view('category_facility.create')->with('category',null);        
         }

         if($category)
         {
            Flash::error('Name already exists');
            return view('category_facility.create')->with('category',null);        
         }

         $insert              = new Category;
         $insert->name        = $input['name'];
         $insert->description = $input['description'];
         $insert->type        = 'room_type';
         $insert->save();
         
         Flash::success('Added new category');

         return view('category_facility.index')->with('categories',$this->category->getCategories('room_type'));
    }



    public function show($id,FacilityRepository $facility)
    {
        return view('facility.show')->with('facility',$facility->get($id));
    }



    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('category_facility.edit')->with('category',$category);
    }

    

    public function update($id,Request $request)
    {
        $input = $request->all();
        $name = trim($input['name']);
        $category  = Category::where('name',$name)->get();
        $old_category = Category::find($id);
        $count = $category->count();
        
        if(strcmp($old_category->name,$request->name) && $count)
        {
           Flash::error('Name already exist!');
           return view('category_facility.edit')->with('category',Category::find($id));   
        }
        else{
            $category = Category::where('id',$id)->update(['name'=>$input['name'],'description'=>$input['description']]);
            
            Flash::success('Updated Successfully');
            return view('category_facility.index')->with('categories',$this->category->getCategories('room_type'));
        }   


    }

    public function undelete($id,FacilityRepository $facility)
    {
        $facility->delete($id);
        Flash::success('Removed from archives successfully!');
        return view('facility.index')->with('facilities',$facility->getFacilities());
    }


    public function destroy($id)
    {

        $category = Category::findOrFail($id);

        $category->delete();

        Flash::success('Deleted successfully!');
        return view('category_facility.index')->with('categories',$this->category->getCategories('room_type'));
    }

    public function addInventoryToFacility(){

        $facility_id = Input::get('facility_id');
        $inv_id = Input::get('inv_id');
     
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('inventory_items')
            ->where('id', $inv_id)
            ->update(array('facility_id' => $facility_id));
        }
        return Response::json(['success' => true]);
    }


    public function deleteInventoryFromFacility(){
        $facility_id = Input::get('facility_id');
        $inv_id = Input::get('inv_id');
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('inventory_items')
            ->where('id', $inv_id)
            ->update(array('facility_id' => '0'));
        }
        return Response::json(['success' => true]);
    }

}
