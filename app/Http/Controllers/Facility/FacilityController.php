<?php

namespace App\Http\Controllers\Facility;

use App\Http\Requests;
use App\Http\Requests\FacilityRequest;
use App\Repositories\InventoryRepository;
use App\Repositories\FacilityRepository;
use App\Models\Inventory;
use App\Models\Category;
use App\Models\Facilities;
use Illuminate\Http\Request;
use Flash;
use Response;
use Gate;
use App\Http\Controllers\Controller;
use Input;

class FacilityController extends Controller
{
    public function __construct(FacilityRepository $facility)
    {
    	$this->middleware('auth');
        $this->facility = $facility;
    }


    /**
     * Display a listing of the rooms.
     *
     * @param Request $request
     * @return Response
     */
  /*  public function index(Request $request,InventoryRepository $inventory)
    {
        return view('facility.index');
    }
*/
    /**
     * Show the form for creating a new rooms.
     *
     * @return Response
     */
   /* public function create(InventoryRepository $inventory, FacilityRepository $facility)
    {
        $inventories = $inventory->getInventories();
        $facilities = $facility->getAll();

        return view('facility.create')->with('inventories',$inventories)
                                      ->with('facilities',$facility);
    }*/

    /**
     * Store a newly created rooms in storage.
     *
     * @param CreateroomsRequest $request
     *
     * @return Response
     */
    /*public function store(Request $request,InventoryRepository $inventory)
    {
        
        $input = $request->all();
        $query = $inventory->create($input);

        if(!$input['name'])
        {
            Flash::error('Name cannot be blank');
            return view('inventory.create');   
        }
        
        if(!$input['serial_no'])
        {
            Flash::error('Serial number cannot be blank');
            return view('inventory.create');   
        }
    }*/

    public function index(Request $request,FacilityRepository $facility)
    {       
        if(Gate::denies('facility-read')) {
            return redirect('/');
        }


        //$facility_count = $facility->getFacilityCount();
        $facility = $facility->getFacilities();
      
        return view('facility.index')
        ->with('facilities',$facility);
        //->with('facility_count',$facility_count);
    }



    public function create(FacilityRepository $facility)
    {
        if(Gate::denies('facility-create')) {
            return redirect('/');
        }

        $facilities = $facility->getFacilities();

        return view('facility.create')->with('facilities',$facilities)->with('categories',Category::all());
 
    }


    public function store(Request $request,FacilityRepository $facility,InventoryRepository $inventory )
    {
                if(Gate::denies('facility-create')) {
            return redirect('/');
        }
         $input = $request->all();

         if(!$input['name'])
         {
            Flash::error('Name cannot be blank');
            return view('facility.create');        
         }

         $facility->create($input);
         $facilities = $facility->getFacilities();
         Flash::success('Added new facility');

         return view('facility.edit')
         ->with('facility',$facility->getFacilityByName($input['name']))
         ->with('inventories',$inventory->getInventories())
         ->with('categories',Category::all());

    }



    public function show($id,FacilityRepository $facility,InventoryRepository $inventory)
    {

                if(Gate::denies('facility-read')) {
            return redirect('/');
        }
        $params['page_type']   = 'template_selected';
        $params['facility_id'] = $id; 
        $inventory_list        = $inventory->search($params);

        return view('facility.show')->with('facility',$facility->get($id))->with('categories',Category::all())->with('inventories',$inventory_list);
    }



    public function edit($id,FacilityRepository $facility,InventoryRepository $inventory)
    {
                if(Gate::denies('facility-update')) {
            return redirect('/');
        }
        $room = $facility->get($id);
        return view('facility.edit')->with('facility',$room)
        ->with('inventories',$inventory->getInventories())
        ->with('categories',Category::all());
    }

    

    public function update($id,Request $request,FacilityRepository $facility)
    {
                if(Gate::denies('facility-update')) {
            return redirect('/');
        }
        $input = $request->all();
        $item  = $facility->get($id);
        
        if(strcmp($request->name,$item[0]->name) && $facility->isFacilityNameExist($input['name']))
        {
           Flash::error('Name already exist!');
           return view('facility.edit')->with('facility',$facility->get($id));   
        }
        else{
            $facility->update($id,$input);
            Flash::success('Updated Successfully');
            return view('facility.index')->with('facilities',$facility->getFacilities());
        }   
    }

    public function undelete($id,FacilityRepository $facility)
    {
        $facility->undelete($id);
        Flash::success('Removed from archives successfully!');
        return view('facility.index')->with('facilities',$facility->getFacilities());
    }


    public function destroy($id,FacilityRepository $facility)
    {
                if(Gate::denies('facility-delete')) {
            return redirect('/');
        }
        $facility->delete($id);
        Flash::success('Deleted successfully!');
        return view('facility.index')->with('facilities',$facility->getFacilities());
    }

    public function addInventoryToFacility(){

        $facility_id = Input::get('facility_id');
        $inv_id = Input::get('inv_id');
     
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('inventory_items')
            ->where('id', $inv_id)
            ->update(array('facility_id' => $facility_id));
        }
        return Response::json(['success' => true]);
    }


    public function deleteInventoryFromFacility(){
        $facility_id = Input::get('facility_id');
        $inv_id = Input::get('inv_id');
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('inventory_items')
            ->where('id', $inv_id)
            ->update(array('facility_id' => '0'));
        }
        return Response::json(['success' => true]);
    }

    public function addUpkeepToFacility(){

        $facility_id = Input::get('facility_id');
        $upkeep_id = Input::get('upkeep_id');
     
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('upkeep_facility')
            ->insert(array('facility_id' => $facility_id,'upkeep_id'=>$upkeep_id,
                           'created_at' => date('Y-m-d H:i:s')));
        }
        return Response::json(['success' => true]);
    }


    public function deleteUpkeepFromFacility(){
        $facility_id = Input::get('facility_id');
        $upkeep_id = Input::get('upkeep_id');
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('upkeep_facility')
            ->where('upkeep_id', $upkeep_id)
            ->where('facility_id',$facility_id)
            ->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }
        return Response::json(['success' => true]);
    }

}
