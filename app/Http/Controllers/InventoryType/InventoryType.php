<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Requests;
use App\Http\Requests\InventoryRequest;
use App\Repositories\InventoryRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\Controller;

class InventoryController extends Controller
{
    
    protected $inventory;
    public function __construct(InventoryRepository $inventory)
    {

        $this->inventory=$inventory;
    	$this->middleware('auth');
    }

    /**
     * Display a listing of the rooms.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request,InventoryRepository $inventory)
    {
        $inventories = $inventory->getInventories();
        return view('inventory.index')->with('inventories',$inventories);
    }

    /**
     * Show the form for creating a new rooms.
     *
     * @return Response
     */
    public function create()
    {
        $inventory =null;
        return view('inventory.create')->with('inventory',$inventory);
    }

    /**
     * Store a newly created rooms in storage.
     *
     * @param CreateroomsRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
        $input = $request->all();
        $query = $this->inventory->create($input);
        $inventory =null;
        if(!$input['name'])
        {
            Flash::error('Name cannot be blank');
            return view('inventory.create')->with('inventory',$inventory);   
        }
        
        if(!$input['serial_no'])
        {
            Flash::error('Serial number cannot be blank');
            return view('inventory.create')->with('inventory',$inventory);   
        }
        

        if(!strcmp('Added Successfully',$query))
        {

            Flash::success($query);
            return view('inventory.index')->with('inventories',$this->inventory->getInventories());
        }
        else{
            Flash::error($query);
            return view('inventory.create')->with('inventory',$inventory);   
        }

    }



    public function show($id,InventoryRepository $inventory)
    {

        $item = $inventory->get($id);

        if(empty($inventory))
        {
            Flash::error('Inventory not found!');
            return view('inventory.index');
        }

        return view('inventory.show')->with('inventory', $item);
    }



    public function edit($id,InventoryRepository $inventory)
    {
        $item = $inventory->get($id);

        if (empty($item)) 
        {
            Flash::error('Room not found');
            return view('inventory.index');
        }

        return view('inventory.edit')->with('inventory', $item);
    }



    public function update($id, Request $request,InventoryRepository $inventory)
    {
        $input = $request->all();
        $item  = $inventory->get($id);
       
        if($item && strcmp($item[0]->name,$request->name))
        {
            if($inventory->isNameExist($request->name))               
            {
               Flash::error('Name already exist!');
               return view('inventory.edit')->with('inventory',$inventory->get($id));
            }

        }
        if($item && strcmp($item[0]->serial_no,$request->serial_no))
        {
            if($inventory->isSerialNumberExist($request->serial_no))
            {
               Flash::error('Serial Number already exist!');
               return view('inventory.edit')->with('inventory',$inventory->get($id));
            }

        }
        else{
            $inventory->update($id,$input);
            return view('inventory.index')->with('inventories',$inventory->getInventories());
        }
    
    }

    public function undelete($id,InventoryRepository $inventory)
    {
        $inventory->undelete($id);
        Flash::success('Removed from archives successfully!');
        return view('inventory.index')->with('inventories',$inventory->getInventories());
    }


    public function destroy($id,InventoryRepository $inventory)
    {   
        $inventory->delete($id);
        $inventories = $inventory->getInventories();
        Flash::success('Inventory Item deleted successfully.');

        return view('inventory.index')->with('inventories',$inventories);
    }

}
