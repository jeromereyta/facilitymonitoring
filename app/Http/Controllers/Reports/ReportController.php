<?php

namespace App\Http\Controllers\Reports;
use App\Http\Requests;
use App\Repositories\ReportRepositories;
use Illuminate\Http\Request;
use App\Models\Status;	
use App\Repositories\InventoryRepository;
use App\Http\Controllers\Controller;
use App\Models\facilities;

class ReportController extends Controller 
{
	protected $report;
	protected $inventory;
	public function __construct(InventoryRepository $inventory)
    {
        $this->middleware('auth');
        $this->inventory = $inventory;
    }

	public function index()
	{
		return view('reports.index');
	}

	public function test(ReportRepositories $report)
	{
		return $report->getUpkeepItemsCount();
	}

	public function UpkeepItemsCountReport(Request $request,ReportRepositories $report)
	{
        
		$UpkeepCountList = $report->getUpkeepItemsCount();
		
		return view('reports.UpkeepItemsCountReport',
			['count_list'=> $UpkeepCountList
			]);
	}

	public function ListOfFacilityReport(Request $request,ReportRepositories $report)
	{
        
		$listOfFacility= $report->getListOfFacility();
		
		$inventories;$inventory_list;$reportList;$facilityList;$inventory_count;

		foreach ($listOfFacility as $key => $value)
		 {
		 	$facilityList[$key]= $value->facility_name;
		 	$params['page_type']   = 'template_selected';
		 	$params['facility_id'] = $value->id;

		 	$reportList [$key]['facility_name'] = $value->facility_name;
		 	$reportList [$key]['id'] 			= $value->id;
			$reportList [$key]['description']  	= $value->description;
		 	$reportList [$key]['category'] 		= $value->category;
		 	$reportList [$key]['inventories']   = $report->getInventoryDetailsByFacilityId($value->id);
		 	$inventory_count[$key]				= count($report->getInventoryDetailsByFacilityId($value->id));
		}
	
		$inventory_count = json_encode($inventory_count);
		$facilityList 	 = json_encode($facilityList);

		return view('reports.ListOfFacilityReport',
			['facilities'	 => $reportList,
			'listOfFacility' => $facilityList,
			'facilityCount'  => $inventory_count
			]);
	}

	public function ListOfInventories(Request $request,ReportRepositories $report)
	{
		$data['facility_id'] = $request->get('facility_id',false);
		$data['from']		 = $request->get('from_date',false);
		$data['to']		 	 = $request->get('to_date',false);
		$data['status']		 = $request->get('status',false);
		
		$reportList = $report->getListOfInventoriesFromMonitoring($data);
		
		$status = Status::select('name')->get();
		$count  = json_encode($report->countStatus($reportList));

		foreach ($status as $key => $value) 
		{
			$listOfStatus[$key] = $value->name;
		}
		return view('reports.InventoryListReport')
		-> with('inventory_list',$reportList)
		-> with('status',json_encode($listOfStatus))
		-> with('count',$count);
	}

	public function ListOfUpkeepItems(Request $request,ReportRepositories $report)
	{
		$data['facility_id'] = $request->get('facility_id',false);
		$data['from']		 = $request->get('from_date',false);
		$data['to']		 	 = $request->get('to_date',false);
		$data['status']		 = $request->get('status',false);
		
		$reportList = $report->getListOfUpkeepStatusMovement($data);
		$status = Status::select('name')->get();
		$count  = json_encode($report->countStatus($reportList));

		$listOfStatus;
		foreach ($status as $key => $value) 
		{
			$listOfStatus[$key] = $value->name;
		}

		return view('reports.UpkeepListReport')
		-> with('upkeep_list',$reportList)
		-> with('status',json_encode($listOfStatus))
		-> with('count',$count);
	}



	public function FacilityCountChart(ReportRepositories $report)
	{
        $chartData = Facilities::select(\DB::raw('MONTHNAME(updated_at) as month'), 
        	\DB::raw("DATE_FORMAT(created_at,'%Y-%m') as monthNum"), \DB::raw('count(*) as facilityCount'))
            ->groupBy('monthNum')
            ->get();

        return $chartData;
    }




    public function FacilityStatusChart(ReportRepositories $report)
	{
        $chartData = \DB::table('monitoring_room_facilities')
		->select(\DB::raw("DATE_FORMAT(monitoring_room_facilities.created_at,'%Y-%m') as monthNum"),
		('monitoring_room_facilities.status'),
		\DB::raw('count(monitoring_room_facilities.status) as facilityCount'))
		->join('facilities','facilities.id','=','monitoring_room_facilities.facility_id')
		->whereNull('monitoring_room_facilities.deleted_at')
		->groupBy('status')
		->get();

        return $chartData;
    }

}