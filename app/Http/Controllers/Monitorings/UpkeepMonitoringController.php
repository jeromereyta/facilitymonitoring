<?php

namespace App\Http\Controllers\Monitorings;
use App\Repositories\StatusRepository;
use App\Repositories\UpkeepMonitoringRepository;
use App\Http\Requests;
use App\Http\Requests\FacilityRequest;
use App\Repositories\InventoryRepository;
use App\Repositories\FacilityRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use Input;
use App\Http\Controllers\Controller;
use Gate;
use Carbon\Carbon;

class UpkeepMonitoringController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function index(Request $request,UpkeepMonitoringRepository $monitoring, StatusRepository $status)
    {       
                                if(Gate::denies('monitoring_upkeep-read')) {
            return redirect('/');
        }
        return view('upkeep_monitoring.index');
    }



    public function create(UpkeepMonitoringRepository $monitoring)
    {
                                        if(Gate::denies('monitoring_upkeep-create')) {
            return redirect('/');
        }

        $facilities = $monitoring->getFacilityDetails();
        $status     = $monitoring->getUpkeepStatus();

        $facility_list = [];
        foreach($facilities as $local => $facility)
        { 
            $facility_list['name']        = $local;
            $facility_list['id']          = $facility['id'];
            $facility_list['description'] = $facility['description'];
        }

        return view('upkeep_monitoring.create')->with('facilities',$facilities)->with('status',$status)->with('facility_list',$facility_list);    
   
    }



    public function store(Request $request,UpkeepMonitoringRepository $monitoring)
    {
        if(Gate::denies('monitoring_upkeep-create')) {
            return redirect('/');
        }

         $input = $request->all();
         
         if(!$input['name'])
         {
            Flash::error('Name cannot be blank');
            return view('upkeep_monitoring.create');        
         }

         $monitorings = $monitoring->create($input);
         
         if(strcmp($monitorings, 'Upkeep Monitoring Sheet Created!'))
         {
     
            $facilities = $monitoring->getFacilityDetails();
            $status     = $monitoring->getUpkeepStatus();

             Flash::success($monitorings);   
             return view('upkeep_monitoring.create')->with('status',$status)->with('facilities',$facilities);    
     
         }
    }



    public function show($id,UpkeepMonitoringRepository $monitoring)
    {
        if(Gate::denies('monitoring_upkeep-read')) {
            return redirect('/');
        }

        $data = Input::all();
        $monitoring_date = $data['monitoring_date'];

        $start = Carbon::parse($monitoring_date)->startOfDay();
        $end = Carbon::parse($monitoring_date)->endOfDay();

        $monitorings = $monitoring->getListOfUpkeepMonitoringByDate($start,$end);

        return view('upkeep_monitoring.show')->with('monitorings',$monitorings)->with('request_date',$monitoring_date);
    }



    public function edit($id,UpkeepMonitoringRepository $monitoring)
    {

                                        if(Gate::denies('monitoring_upkeep-update')) {
            return redirect('/');
        }

        $inventory_monitoring = $monitoring->get($id);
        $status               = $monitoring->getUpkeepStatus();

        return view('upkeep_monitoring.edit')->with('monitorings',$inventory_monitoring)->with('status',$status);
    }

    

    public function editFacilityItems($monitoring_id,$facility_id,UpkeepMonitoringRepository $monitoring)
    {
        $params['monitoring_id']    = $monitoring_id;
        $params['facility_id']      = $facility_id;
        $data = $monitoring->getUpkeepItemsInMonitoring($facility_id,$monitoring_id);

        return view('upkeep_monitoring.facility_edit')->with('data',$data)->with('params',$params);
    }

    public function getEventsJson(UpkeepMonitoringRepository $monitoring) {

        $input = Input::all();

        $start = new Carbon($input['start']);
        $end = new Carbon($input['end']);
        $start = $start->toDateTimeString();
        $end = $end->toDateTimeString();
        $events = $monitoring->getMonitoringIdByDate($start,$end);
        
        $eventsJson = array();
        foreach ($events as $event) {
            $eventsJson[] = array(
                'id' => $event->id,
                'title' => $event->name,
                'start' => $event->created_at
            );
        }
        return Response::json($eventsJson);
    }

    

    public function update($id,Request $request,FacilityRepository $facility)
    {
                                        if(Gate::denies('monitoring_upkeep-update')) {
            return redirect('/');
        }

        $input = $request->all();
        $item  = $facility->get($id);

        if(strcmp($request->name,$item[0]->name) && $facility->isFacilityNameExist($input['name']))
        {
           Flash::error('Name already exist!');
           return view('facility.edit')->with('facility',$facility->get($id));   
        }
        else{
            $facility->update($id,$input);
            Flash::success('Update Successfully');
            return view('facility.index')->with('facilities',$facility->getFacilities());
        }   


    }

    public function updateUpkeepMonitoringStatus($id,UpkeepMonitoringRepository $monitoring){
       
        if(Gate::denies('monitoring_upkeep-update')) {
            return redirect('/');
        }

        $upkeep_monitoring_id = Input::get('upkeep_monitoring_id');
        $facility_id = Input::get('facility_id');
        $status = Input::get('status');
        $inventory_id = Input::get('inventory_id');

        if($monitoring->updateUpkeepMonitoringStatus($upkeep_monitoring_id,$facility_id,$inventory_id,$status)){
            return Response::json(['success' => true]);
        }
        return Response::json(['success' => false]);
    }

    public function updateUpkeepMonitoringRemarks($id,UpkeepMonitoringRepository $monitoring){
       
                                       if(Gate::denies('monitoring_upkeep-update')) {
            return redirect('/');
        }

        $upkeep_monitoring_id = Input::get('upkeep_monitoring_id');
        $facility_id = Input::get('facility_id');
        $remarks = Input::get('remarks');
        $inventory_id = Input::get('inventory_id');

        if($monitoring->updateUpkeepMonitoringRemarks($upkeep_monitoring_id,$facility_id,$inventory_id,$remarks)){
            return Response::json(['success' => true]);
        }
        return Response::json(['success' => false]);
    }


    public function destroy($id,UpkeepMonitoringRepository $monitoring)
    {
                                        if(Gate::denies('monitoring_upkeep-delete')) {
            return redirect('/');
        }

        $monitoring->delete($id);

        Flash::success('Deleted successfully!');
        return view('upkeep_monitoring.index')->with('monitorings',$monitoring->getListOfUpkeepMonitoring());
    }
}
