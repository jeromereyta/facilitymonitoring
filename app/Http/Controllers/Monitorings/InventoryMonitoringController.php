<?php

namespace App\Http\Controllers\Monitorings;

use App\Http\Requests;
use App\Http\Requests\FacilityRequest;
use App\Repositories\InventoryMonitoringRepository;
use App\Repositories\InventoryRepository;
use App\Models\Inventory;
use App\Models\Status;

use App\Models\monitorings;
use App\User;
use App\Repositories\StatusRepository;
use App\Models\Facilities;
use App\Repositories\FacilityRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use Gate;
use Input;
use Auth;
use App\Http\Controllers\Controller;

class InventoryMonitoringController extends Controller
{
    protected $inventory_monitoring;
    public function __construct(InventoryMonitoringRepository $inventory_monitoring)
    {
    	$this->middleware('auth');
        $this->inventory_monitoring=$inventory_monitoring;

    }




    public function viewMonitoring($monitoring_id,$facility_id)
    {
        $monitorings = monitorings::select('name','id','description')->where('id',$monitoring_id)->get()[0];
        $monitorings['inventories'] = $this->inventory_monitoring->getInventoriesWithMonitoringId($monitoring_id,$facility_id);

            // dd($monitorings['inventories']);

        return view('inventory_monitoring.show_monitoring_field')->with('monitorings',$monitorings)->with('facility_id',$facility_id)->with('status',Status::all());

    }


    public function showMonitoringList($facility_id)
    {
        $name =Facilities::find($facility_id)->name;
        $monitorings = $this->inventory_monitoring->getMonitoringInventoryByFacilityId($facility_id);

        return view('inventory_monitoring.monitoring_list')->with('monitorings',$monitorings)->with('facility_name',$name)->with('facility_id',$facility_id);
    }

    public function editMonitoring($monitoring_id,$facility_id)
    {

        $monitorings = monitorings::select('name','id','description')->where('id',$monitoring_id)->get()[0];
        $monitorings['inventories'] = $this->inventory_monitoring->getInventoriesWithMonitoringId($monitoring_id,$facility_id);

            // dd($monitorings['inventories']);

        return view('inventory_monitoring.edit_monitoring_field')->with('monitorings',$monitorings)->with('facility_id',$facility_id)->with('status',Status::all());
    }

    public function updateMonitoring(Request $request)
    {
        $input = $request->all();
        $monitoring_id = monitorings::where('name',trim($input['name']))->select('id')->get()[0]->id;
        $input['monitoring_id'] = $monitoring_id;
        $facility_id = $input['facility_id'];
        $description = $input['description'];
            


        if(!$input['name'])
        {
            $monitorings = monitorings::select('name','id','description')->where('id',$monitoring_id)->get()[0];
            $monitorings['inventories'] = $this->inventory_monitoring->getInventoriesWithMonitoringId($monitoring_id,$facility_id);

            Flash::error('Name cannot be blank');
            return view('inventory_monitoring.edit_monitoring_field')->with('monitorings',$monitorings)->with('facility_id',$facility_id)->with('status',Status::all());
        }

        $this->inventory_monitoring->update($input);        
        $this->inventory_monitoring->UpdateMonitoringFacilityInventory($input);


        $name =Facilities::find($facility_id)->name;
        $monitorings = $this->inventory_monitoring->getMonitoringInventoryByFacilityId($facility_id);
        
        Flash::success('Update successfully');
          
        return view('inventory_monitoring.monitoring_list')->with('monitorings',$monitorings)->with('facility_name',$name)->with('facility_id',$facility_id);

    }


    public function deleteMonitoring($monitoring_id,$facility_id)
    {
        if(Gate::denies('monitoring_inventory-delete')) {
        return redirect('/');
        }
        $softDelete = \DB::table('monitorings')
        ->where('id',$monitoring_id)
        ->update(['deleted_at'=>date('Y-m-d H:i:s')]);


        $name =Facilities::find($facility_id)->name;
        $monitorings = $this->inventory_monitoring->getMonitoringInventoryByFacilityId($facility_id);

        return view('inventory_monitoring.monitoring_list')->with('monitorings',$monitorings)->with('facility_name',$name)->with('facility_id',$facility_id);

    }



    public function index(Request $request,InventoryMonitoringRepository $monitoring, StatusRepository $status)
    {   
                        if(Gate::denies('monitoring_inventory-read')) {
            return redirect('/reports');
     }

        $inventory_count = Inventory::count();    
        $facility_count  = Facilities::count();           
        // $monitorings = $monitoring->getListOfInventoryMonitoring();
        $facilities = Facilities::paginate(10);

        return view('inventory_monitoring.index')
        ->with('facilities',$facilities)
        ->with('inventory_count',$inventory_count)
        ->with('facility_count',$facility_count);
    
    }

    public function createMonitoring($id)
    {
        $inventories = $this->inventory_monitoring->getInventoriesByFacilityId($id);

        $status = Status::all();

        return view('inventory_monitoring.fields')->with('inventories',$inventories)->with('status',$status)->with('facility_id',$id)->with('monitorings',null);

    }

    public function create(Request $request)
    {
        if(Gate::denies('monitoring_inventory-create')) {
        return redirect('/');
        }

        $input = $request->all();

        dd($input);
    }



    public function store(Request $request,InventoryMonitoringRepository $monitoring)
    {
        if(Gate::denies('monitoring_inventory-create')) {
        return redirect('/');
        }

         $input = $request->all();


         if(!$input['name'])
         {
            $inventories = $monitoring->getInventoriesByFacilityId($input['facility_id']);
            $status = Status::all();
            Flash::error('Name cannot be blank');
            return view('inventory_monitoring.fields')->with('inventories',$inventories)->with('status',$status);
         }

         $monitorings = $monitoring->create($input);
         
         if(!strcmp($monitorings, 'This monitoring name already exist'))
         {
            $inventories = $this->inventory_monitoring->getInventoriesByFacilityId($input['facility_id']);

            $status = Status::all();
        Flash::error('Name Already Exist');
        
        return view('inventory_monitoring.fields')->with('inventories',$inventories)->with('status',$status)->with('facility_id',$input['facility_id'])->with('monitorings',null);

         }
        $inventory_count = Inventory::count();    
        $facility_count  = Facilities::count();

        $name =Facilities::find($input['facility_id'])->name;
        $monitorings = $this->inventory_monitoring->getMonitoringInventoryByFacilityId($input['facility_id']);

        Flash::success('Added successfully');
        return view('inventory_monitoring.monitoring_list')->with('monitorings',$monitorings)->with('facility_name',$name)->with('facility_id',$input['facility_id']);
    }



    public function show($id,InventoryMonitoringRepository $monitoring)
    {

        if(Gate::denies('monitoring_inventory-read')) {
        return redirect('/');
        }

        $inventory_monitoring = $monitoring->get($id);
        $facilities = $monitoring->getFacilityDetails();
        $status     = $monitoring->getInventoryStatus();

        return view('inventory_monitoring.show')->with('monitorings',$inventory_monitoring)
                ->with('show_view','show_view');
    }




    public function edit($id,InventoryMonitoringRepository $monitoring)
    {
                                if(Gate::denies('monitoring_inventory-update')) {
            return redirect('/');
        }

        $inventory_monitoring = $monitoring->get($id);
        $status     = $monitoring->getInventoryStatus();

        return view('inventory_monitoring.edit')->with('monitorings',$inventory_monitoring)->with('status',$status);
    }

    

    public function update($id,Request $request,InventoryMonitoringRepository $monitoring)
    {
                                if(Gate::denies('monitoring_inventory-update')) {
            return redirect('/');
        }

        $input = $request->all();
        $item =$monitoring->get($id);

        /*if(strcmp($request->name,$item[0]['name']) && $monitoring->isMonitoringNameExist($input['name']))
        {
                    $inventory_monitoring = $monitoring->get($id);

           Flash::error('Name already exist!');
           return view('inventory_monitoring.edit')->with('inventory_monitoring',$inventory_monitoring);   
        }
        else{*/
        $inventory_count = Inventory::count();    
        $facility_count  = Facilities::count();

            $monitoring->update($input);
            $monitorings = $monitoring->getListOfInventoryMonitoring();
            Flash::success('Update Successfully');
            return view('inventory_monitoring.index')->with('monitorings',$monitorings)
            ->with('inventory_count',$inventory_count)
            ->with('facility_count',$facility_count);

    }

    public function updateInventoryMonitoringStatus($id,InventoryMonitoringRepository $monitoring){
       
                               if(Gate::denies('monitoring_inventory-update')) {
            return redirect('/');
        }

        $inventory_monitoring_id = Input::get('inventory_monitoring_id');
        $facility_id = Input::get('facility_id');
        $status = Input::get('status');
        $inventory_id = Input::get('inventory_id');

        if($monitoring->updateInventoryMonitoringStatus($inventory_monitoring_id,$facility_id,$inventory_id,$status)){
            return Response::json(['success' => true]);
        }
        return Response::json(['success' => false]);
    }

    public function updateInventoryMonitoringRemarks($id,InventoryMonitoringRepository $monitoring){
       
                               if(Gate::denies('monitoring_inventory-delete')) {
            return redirect('/');
        }

        $inventory_monitoring_id = Input::get('inventory_monitoring_id');
        $facility_id = Input::get('facility_id');
        $remarks = Input::get('remarks');
        $inventory_id = Input::get('inventory_id');

        if($monitoring->updateInventoryMonitoringRemarks($inventory_monitoring_id,$facility_id,$inventory_id,$remarks)){
            return Response::json(['success' => true]);
        }
        return Response::json(['success' => false]);
    }


    public function destroy($id,InventoryMonitoringRepository $monitoring)
    {
                                if(Gate::denies('monitoring_inventory-delete')) {
            return redirect('/');
        }

        $monitoring->delete($id);

        $inventory_count = Inventory::count();    
        $facility_count  = Facilities::count();

        Flash::success('Deleted successfully!');
        return view('inventory_monitoring.index')->with('monitorings',$monitoring->getListOfInventoryMonitoring())
            ->with('inventory_count',$inventory_count)
            ->with('facility_count',$facility_count);;
    }

    public function addFacilityToMonitoring(){

        $facility_id = Input::get('facility_id');
        $inv_id = Input::get('inv_id');
     
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('inventory_items')
            ->where('id', $inv_id)
            ->update(array('facility_id' => $facility_id));
        }
        return Response::json(['success' => true]);
    }


    public function deleteFacilityFromMonitoring(){
        $facility_id = Input::get('facility_id');
        $inv_id = Input::get('inv_id');
        $facility = Facilities::findOrFail($facility_id);
        if ($facility) {
            \DB::table('inventory_items')
            ->where('id', $inv_id)
            ->update(array('facility_id' => '0'));
        }
        return Response::json(['success' => true]);
    }

}
