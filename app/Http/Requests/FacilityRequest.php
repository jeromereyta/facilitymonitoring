<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Inventory;

class FacilityRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
        'name' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Please Select a Commodity Type'
        ];
    }
}
