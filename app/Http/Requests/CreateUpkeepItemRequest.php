<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateUpkeepItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required',
            'description' => 'required'
        ];

    }

    public function messages()
    {
        return ['name.required' => 'Er, you forgot to provide a name',
                'description.required' => 'What am i then?, you forgot to provide a description'];
    }
}
