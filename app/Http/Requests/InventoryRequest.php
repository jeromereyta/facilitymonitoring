<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Inventory;

class InventoryRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
        'name' => 'required'
        ];

        return $rules;
    }

    // public function messages()
    // {
    //     return [
    //         'name.required' => 'Please Select a Commodity Type',
    //         'from_monthly.required_without' => '',
    //         'from_weekly.required_without' => 'Please Provide a Date, Search Either Weekly or Monthly',
    //         'region_id.required_without_all' => 'Region Filter is Required When Municipality or Province is not Present',
    //         'province_id.required_without_all' => 'Province Filter is Required When Region or Municipality is not Present',
    //         'municipality_id.required_without_all' => 'Municipality Filter is Required When Region or Province is not Present'
    //     ];
    // }
}
