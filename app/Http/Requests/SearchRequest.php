<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;
class SearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        return [
        ];
    }
    public function sanitize(){
        $data = $this->all();
        $sort = Input::get('sort');
        $order =Input::get('order');
        $view_thrashed_rows = Input::get('view_thrashed_rows');
        if($sort!=null&&$order!=null){
            $data['sort'] = $sort;
            $data['order'] = $order;
        }
        if($view_thrashed_rows==null){
            $data['view_thrashed_rows'] = false;
        }else{
            $data['view_thrashed_rows'] = true;
        }
        $data['page_type'] = Input::get('page_type');
        $this->replace($data);
    }
}
