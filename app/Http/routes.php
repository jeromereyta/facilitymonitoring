<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::auth();

Route::get('/','Monitorings\InventoryMonitoringController@index');
Route::group(['namespace'=>''], function()
{
	foreach ( File::allFiles(__DIR__.'/Routes') as $partial )
	{
   		 require_once $partial->getPathName();
	}

});

Route::get('facility_chart_count', 'Reports\ReportController@facilityCountChart');

Route::get('facility_chart_status', 'Reports\ReportController@FacilityStatusChart');






// Route::auth();

// Route::get('/', 'monitoringsController@index');


// // Authentication routes...
// Route::get('auth/login', 'Auth\AuthController@getLogin');
// Route::post('auth/login', 'Auth\AuthController@postLogin');
// Route::get('auth/logout', 'Auth\AuthController@getLogout');

// // Registration routes...
// Route::get('auth/register', 'Auth\AuthController@getRegister');
// Route::post('auth/register', 'Auth\AuthController@postRegister');


// Route::controllers([
//    'password' => 'Auth\PasswordController',
// ]);