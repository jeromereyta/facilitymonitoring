<?php

Route::group(['namespace' => 'Search'], function() {
    Route::post('search/{searchDomain}',array('as' => 'search.search', 'uses' => 'SearchController@search'));
});