<?php

Route::group(['namespace' => 'UpkeepItem'], function() {

  	Route::resource('upkeep_item', 'UpkeepItemController');

	Route::get('upkeep_item/{id}/delete', ['as' => 'upkeep_item.delete', 'uses' => 'UpkeepItemController@destroy']);
});



