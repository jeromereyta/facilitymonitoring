<?php

Route::group(['namespace' => 'Facility'], function() {
  Route::resource('facility', 'FacilityController');
  Route::get('facility/{id}/delete', ['as' => 'facility.delete', 'uses' => 'FacilityController@destroy']);
  Route::get('facility/{id}/undelete', ['as' => 'facility.undelete', 'uses' => 'FacilityController@undelete']);


    Route::POST('facility/addInventory', [
        'as'   => 'facility.addInventory',
        'uses' =>  'FacilityController@addInventoryToFacility'
    ]);
    Route::POST('facility/deleteInventory', [
        'as'   => 'facility.deleteInventory',
        'uses' =>  'FacilityController@deleteInventoryFromFacility'
    ]);

    Route::POST('facility/addUpkeep', [
        'as'   => 'facility.addUpkeep',
        'uses' =>  'FacilityController@addUpkeepToFacility'
    ]);
    Route::POST('facility/deleteUpkeep', [
        'as'   => 'facility.deleteUpkeep',
        'uses' =>  'FacilityController@deleteUpkeepFromFacility'
    ]);
});



