<?php

Route::group(['namespace' => 'Inventory'], function() {

  	Route::resource('inventory', 'InventoryController');

	Route::get('inventory/{id}/delete', ['as' => 'inventory.delete', 'uses' => 'InventoryController@destroy']);

	Route::get('inventory/{id}/createMonitoring', ['as' => 'inventory.createMonitoring', 'uses' => 'InventoryController@createMonitoring']);


	Route::get('inventory/{id}/undelete', ['as' => 'inventory.undelete', 'uses' => 'InventoryController@undelete']);
	
});



