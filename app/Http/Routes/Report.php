<?php

Route::group(['namespace' => 'Reports'], function() {

	Route::get('/reports','ReportController@index');

    Route::get('/facilitylist','ReportController@ListOfFacilityReport');
    Route::post('/facilitylist','ReportController@ListOfFacilityReport');
    Route::get('/inventoryListReport','ReportController@ListOfInventories');
    Route::post('/inventoryListReport','ReportController@ListOfInventories');
    Route::get('/upkeep-list-report', 'ReportController@ListOfUpkeepItems');
    Route::post('/upkeep-list-report', 'ReportController@ListOfUpkeepItems');
    Route::get('/upkeep-count-report', 'ReportController@UpkeepItemsCountReport');
    Route::post('/upkeep-count-report', 'ReportController@UpkeepItemsCountReport');

     Route::get('/test-report', 'ReportController@test');


});



Route::get('/reports', ['middleware' => 'auth' ,function() {
return View('reports.index');
}]);