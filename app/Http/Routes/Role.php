<?php

Route::group(['namespace' => 'Role'], function() {
  Route::resource('roles', 'RoleController');

    Route::get('roles/{id}/destroy', ['as' => 'roles.destroy', 'uses' => 'RoleController@destroy']);
});



