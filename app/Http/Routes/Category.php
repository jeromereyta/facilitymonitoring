<?php

Route::group(['namespace' => 'Category'], function() {

  	Route::resource('category_facility', 'FacilityCategoryController');

	Route::get('category_facility/{id}/delete', ['as' => 'category_facility.delete', 'uses' => 'FacilityCategoryController@destroy']);

	Route::get('category/{id}/undelete', ['as' => 'category_facility.undelete', 'uses' => 'FacilityCategoryController@undelete']);
	
});



