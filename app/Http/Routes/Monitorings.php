<?php

Route::group(['namespace' => 'Monitorings'], function() {

Route::resource('inventory_monitoring', 'InventoryMonitoringController');

	Route::get('inventory_monitoring/{id}/createMonitoring', ['as' => 'inventory_monitoring.createMonitoring', 'uses' => 'InventoryMonitoringController@createMonitoring']);

Route::get('inventory_monitoring/{id}/delete', ['as' => 'inventory_monitoring.delete', 'uses' => 'InventoryMonitoringController@destroy']);

Route::get('inventory_monitoring/{id}/showMonitoringList', ['as' => 'inventory_monitoring.showMonitoringList', 'uses' => 'InventoryMonitoringController@showMonitoringList']);

Route::get('inventory_monitoring/{monitoring_id}/editMonitoring/{facility_id}', ['as' => 'inventory_monitoring.editMonitoring', 'uses' => 'InventoryMonitoringController@editMonitoring']);

Route::get('inventory_monitoring/{monitoring_id}/viewMonitoring/{facility_id}', ['as' => 'inventory_monitoring.viewMonitoring', 'uses' => 'InventoryMonitoringController@viewMonitoring']);

Route::get('inventory_monitoring/{monitoring_id}/deleteMonitoring/{facility_id}', ['as' => 'inventory_monitoring.deleteMonitoring', 'uses' => 'InventoryMonitoringController@deleteMonitoring']);



Route::post('inventory_monitoring/updateMonitoring', ['as' => 'inventory_monitoring.updateMonitoring', 'uses' => 'InventoryMonitoringController@updateMonitoring']);





Route::post('update_inventory_status/{inventory_monitoring_id}', 
	['as' => 'inventory_monitoring.updateInventoryStatus', 
	'uses' => 'InventoryMonitoringController@updateInventoryMonitoringStatus']);

Route::post('update_inventory_remarks/{inventory_monitoring_id}', 
	['as' => 'inventory_monitoring.updateInventoryRemarks', 
	'uses' => 'InventoryMonitoringController@updateInventoryMonitoringRemarks']);

Route::post('update_monitoring_status/{upkeep_monitoring_id}', 
	['as' => 'upkeep_monitoring.updateUpkeepStatus', 
	'uses' => 'UpkeepMonitoringController@updateUpkeepMonitoringStatus']);

Route::post('update_monitoring_remarks/{upkeep_monitoring_id}', 
	['as' => 'upkeep_monitoring.updateUpkeepRemarks', 
	'uses' => 'UpkeepMonitoringController@updateUpkeepMonitoringRemarks']);

Route::get('upkeep_monitoring/facility_edit/{monitoring_id}/{facility_id}', 
	['as' => 'upkeep_monitoring.facility_edit', 
	 'uses' => 'UpkeepMonitoringController@editFacilityItems']);


Route::get('upkeep_monitoring/events', 
	['as' => 'upkeep_monitoring.events', 
	 'uses' => 'UpkeepMonitoringController@getEventsJson']);

Route::get('room/{id}/delete', ['as' => 'rooms.delete', 'uses' => 'RoomController@destroy']);

Route::resource('upkeep_monitoring', 'UpkeepMonitoringController');
Route::get('upkeep_monitoring/{id}/delete', ['as' => 'upkeep_monitoring.delete', 'uses' => 'UpkeepMonitoringController@destroy']);


});