<?php

Route::group(['namespace' => 'Status'], function() {
  Route::resource('status_inventory', 'InventoryStatusController');

  Route::resource('status_upkeep', 'UpkeepStatusController');
  // Route::get('facility/{id}/delete', ['as' => 'facility.delete', 'uses' => 'FacilityController@destroy']);
  // Route::get('facility/{id}/undelete', ['as' => 'facility.undelete', 'uses' => 'FacilityController@undelete']);
});



