<?php

Route::group(['namespace' => 'User'], function() {
    Route::resource('users', 'UserController');

    Route::get('users/{id}/delete', ['as' => 'users.delete', 'uses' => 'UserController@destroy']);
});
