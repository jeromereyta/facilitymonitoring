<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DB;

class RoomFacilities extends Model
{
    //
    protected $table = 'room_facilities';
    protected $primaryKey = 'room_id';

/*    public function rooms() 
    {
        return $this->hasOne('Rooms','room_id');
    }*/

    public static function sqlStatement($sql_stmt) {
        DB::statement($sql_stmt);
    }
/*    public function facilities() 
    {
        return $this->belongsTo('Facilities');
    }*/
}
