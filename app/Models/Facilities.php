<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class facilities extends Model
{
    use SoftDeletes;

    public $table = 'facilities';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
/*
    public function inventories(){
        
        return $this->belongsToMany(InventoryItems::class, 'facility_inventory', 'inventory_id', 'facility_id');
        
    }*/
}
