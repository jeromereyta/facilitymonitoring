<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class User extends Model
{

/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'created_at',
        'updated_at',
    ];    //

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required'       
    ];


}
