<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class InventoryStatus extends Model
{
     public $table = 'inventory_status';

/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
              
    ];    //

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',       
    ];


}
