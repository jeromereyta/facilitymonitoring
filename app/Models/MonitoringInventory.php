<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MonitoringInventory extends Model
{
    public $table = 'monitoring_inventory';
/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'monitoring_id',
        'facility_id',
        'inventory_id',
        'remarks',
        'created_at', 
        'updated_at', 
        'deleted_at',       
    ];    //

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
            
    ];

}
