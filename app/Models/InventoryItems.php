<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class InventoryItems extends Model
{

/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'serial_no',
        'inventory_type',
        'description',
        'category',       
    ];    //

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
		'serial_no'=> 'required:unique:inventory_items,serial_no'        
    ];


}
