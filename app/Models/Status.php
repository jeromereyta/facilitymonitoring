<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
 
 	protected $table = 'inventory_status';
	protected $fillable = ['name','description','type','created_at'];
   
}


