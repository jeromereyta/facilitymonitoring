<div class="box">
<div class="box-header with-border">
<h5 class="box-title"><i class="fa fa-pencil"></i>Inventory Status Details</h5>
</div>

<div class="panel-body">
<div class="admin-form theme-primary">
<div class="container">

<form class="form-vertical" role="form">

<div class="col-md-4">
<div class="form-group">
<label class="field prepend-icon mt5" for="name">Name: </label>
<input type="text" class="form-control" name="name" placeholder="Enter Name">
</div>
</div>
    
<div class="form-group col-sm-12">

    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('status_inventory.index') !!}" class="btn btn-default">Cancel</a>
</div>
</form>
</div>
</div>  