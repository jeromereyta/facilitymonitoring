
{!! Form::open(['route' => 'status_upkeep.store']) !!}


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    
      <div class="modal-header">

      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Let's Add Status Name</h4>
      </div>
      <div class="modal-body">

        <form class="form-vertical" role="form">

      
        <div class="col-md-4">
        <div class="form-group">
        <label class="field prepend-icon mt5" for="name" >Name: </label>
        <input type="text" style="width: 200%" class="form-control" name="name" placeholder="Enter Upkeep Status Name">
        </div>
        </div>

        <div class="form-group col-sm-12">

        {!! Form::submit('Save', ['class' => 'btn btn-flat btn-primary']) !!}
        <a href="" data-dismiss="modal" class="btn btn-default">Cancel</a>
        </div>
        </form>

      </div>
      <div class="modal-footer">
      </div>
    
    </div>

  </div>
</div>
{!! Form::close() !!}

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-flat btn-primary pull-right" data-toggle="modal" data-target="#myModal">Add New Status</button>
