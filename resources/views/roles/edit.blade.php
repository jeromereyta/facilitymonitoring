@extends('partials.master')

@section('title')
        <a href = "#">Edit Role Details</a>
@endsection

@section('content')
@include('flash::message')

{!! Form::open(['method' => 'PUT', 'route' => ['roles.update', $role->id],
 'class' => 'form-horizontal', 'files' => 'true']) !!}

@include('roles.edit_fields',['disable' => false])

{!! Form::close() !!}        

 
@endsection
