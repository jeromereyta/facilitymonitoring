@extends('partials.master')

@section('title')
        <a href = "#">View Facility</a>
@endsection

@section('content')
    @include('roles.edit_fields',['disable' => true])


    <div class="box-footer">
        <div class="form-group">
            <div class="col-sm-10">
                <a href="{!! route('roles.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
            </div>
        </div>
    </div><!-- box-footer -->   
  
@endsection
