@extends('partials.master')

@section('title')
        <a href = "#">Create New Role</a>
@endsection

@section('content')

 
    <div class="row">
{!! Form::open(['route' => 'roles.store']) !!}
@include('roles.edit_fields')
{!! Form::close() !!}

    </div>
@endsection