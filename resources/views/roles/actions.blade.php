<div class="btn-group">
            <a class="btn btn-md btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions
                <span class="caret ml5"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('roles.show', $role->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View Role </a>
                </li>

                <li>
                    <a href="{{ route('roles.edit', $role->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit Role</a>
                </li>

                <li>
                    <a href="{{ route('roles.destroy', $role->id) }}" class="alert-link" onclick='return confirm("Are you sure you want to delete ?")'><span class="label label-warning"><i class="glyphicon glyphicon-trash"></i></span> Delete</a>
                </li>

            </ul>
</div>