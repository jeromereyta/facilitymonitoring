<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h5 class="box-title"><i class="fa fa-pencil"></i>Category Details</h5>
            </div>

                    <div class="panel-body">
                            <div class="admin-form theme-primary">
                                <div class="container">
@include('flash::message')
                                @if($role)

                                        <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="field prepend-icon mt5" for="name">Name: </label>
                                        <input  value ="{{$role->name}}" type="text" class="form-control" name="name" placeholder="Enter Name"
                                        @if($disable)
                                       disabled
                                        @endif>
                                        </div>
                                        </div>
                                 @else
                                        <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="field prepend-icon mt5" for="name">Name: </label>
                                        <input  type="text" class="form-control" name="name" placeholder="Enter Name">
                                        </div>
                                        </div>
                                 @endif
                                        </div>
                                        </div>        
                                        </div>
                                        </div>
                                @include('roles.permission_table')



            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-10">
                    @if(!$disable)
                        <button type="submit" class="btn btn-default btn-sm ph15">Submit</button>
                     @endif   
                        <a href="{!! route('roles.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                    </div>
                </div>
            </div><!-- box-footer -->   
             
        </div>

</div>