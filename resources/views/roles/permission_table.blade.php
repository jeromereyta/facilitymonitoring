<div class="box">
<div class="box-header with-border">
<h5 class="box-title"><i class="fa fa-pencil"></i>Permission Details</h5>
</div>
    <div class="well">
<!--     <p class="text-muted">  Admin Access</p>
    <p> {{  Form::checkbox('admin-read',1,false)  }}Read</p>
    <p> {{  Form::checkbox('admin-write',1,false)  }}Write</p>
    <p> {{  Form::checkbox('admin-update',1,false)  }}Update</p>
    <p> {{  Form::checkbox('admin-delete',1,false)  }}Delete</p>
     </div> -->

    <div class="well">
    <p class="text-muted">  Monitoring Inventory Access</p>
    <p> {{  Form::checkbox('permissions[monitoring_inventory-read]',1,in_array('monitoring_inventory-read', $permissions))  }}Read</p>
    <p> {{  Form::checkbox('permissions[monitoring_inventory-create]',1,in_array('monitoring_inventory-create', $permissions))  }}Write</p>
    <p> {{  Form::checkbox('permissions[monitoring_inventory-update]',1,in_array('monitoring_inventory-update', $permissions))  }}Update</p>
    <p> {{  Form::checkbox('permissions[monitoring_inventory-delete]',1,in_array('monitoring_inventory-delete', $permissions))  }}Delete</p>
     </div>
    
    <div class="well">
    <p class="text-muted">  Monitoring Upkeep Access</p>
    <p> {{  Form::checkbox('permissions[monitoring_upkeep-read]',1,in_array('monitoring_upkeep-read', $permissions))  }}Read</p>
    <p> {{  Form::checkbox('permissions[monitoring_upkeep-create]',1,in_array('monitoring_upkeep-create', $permissions))  }}Write</p>
    <p> {{  Form::checkbox('permissions[monitoring_upkeep-update]',1,in_array('monitoring_upkeep-update', $permissions))  }}Update</p>
    <p> {{  Form::checkbox('permissions[monitoring_upkeep-delete]',1,in_array('monitoring_upkeep-delete', $permissions))  }}Delete</p>
     </div>


    <div class="well">
    <p class="text-muted">  Facility Access</p>
    <p> {{  Form::checkbox('permissions[facility-read]',1,in_array('facility-read', $permissions))  }}Read</p>
    <p> {{  Form::checkbox('permissions[facility-create]',1,in_array('facility-create', $permissions))  }}Write</p>
    <p> {{  Form::checkbox('permissions[facility-update]',1,in_array('facility-update', $permissions))  }}Update</p>
    <p> {{  Form::checkbox('permissions[facility-delete]',1,in_array('facility-delete', $permissions))  }}Delete</p>
     </div>

    <div class="well">
    <p class="text-muted">  Item Access</p>
    <p> {{  Form::checkbox('permissions[item-read]',1,in_array('item-read', $permissions))  }}Read</p>
    <p> {{  Form::checkbox('permissions[item-create]',1,in_array('item-create', $permissions))  }}Write</p>
    <p> {{  Form::checkbox('permissions[item-update]',1,in_array('item-update', $permissions))  }}Update</p>
    <p> {{  Form::checkbox('permissions[item-delete]',1,in_array('item-delete', $permissions))  }}Delete</p>
     </div>

    <div class="well">
    <p class="text-muted">  Status Access</p>
    <p> {{  Form::checkbox('permissions[status-read]',1,in_array('status-read', $permissions))  }}Read</p>
    <p> {{  Form::checkbox('permissions[status-create]',1,in_array('status-create', $permissions))  }}Write</p>
    <p> {{  Form::checkbox('permissions[status-update]',1,in_array('status-update', $permissions))  }}Update</p>
    <p> {{  Form::checkbox('permissions[status-delete]',1,in_array('status-delete', $permissions))  }}Delete</p>
     </div>

    <div class="well">
    <p class="text-muted">  Role Access</p>
    <p> {{  Form::checkbox('permissions[role-read]',1,in_array('role-read', $permissions))  }}Read</p>
    <p> {{  Form::checkbox('permissions[role-create]',1,in_array('role-create', $permissions))  }}Write</p>
    <p> {{  Form::checkbox('permissions[role-update]',1,in_array('role-update', $permissions))  }}Update</p>
    <p> {{  Form::checkbox('permissions[role-delete]',1,in_array('role-delete', $permissions))  }}Delete</p>
    </div>

    <div class="well">
    <p class="text-muted">  User Access</p>
    <p> {{  Form::checkbox('permissions[user-read]',1,in_array('user-read', $permissions))  }}Read</p>
    <p> {{  Form::checkbox('permissions[user-create]',1,in_array('user-create', $permissions))  }}Write</p>
    <p> {{  Form::checkbox('permissions[user-update]',1,in_array('user-update', $permissions))  }}Update</p>
    <p> {{  Form::checkbox('permissions[user-delete]',1,in_array('user-delete', $permissions))  }}Delete</p>
     </div>
</div>        
</div>
</div>
