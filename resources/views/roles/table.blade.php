   <div class="box">
            <div class="box-header">
              <h6 class="box-title">Roles</h6>
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table class="table table-responsive">
                <thead>
                      <tr style="font-size: 15px;  font-weight: bold;">
                      <td nowrap="nowrap" >Role Name</td>
                      <td nowrap="nowrap">Actions</td>
              </tr>
              </thead>
              <tbody>           
                    @foreach($roles as $key => $role)

              <tr>
                    <tr style="font-size: 15px;">

                        <td>{{$role->name}}</td>           
                        <td>@include('roles.actions')</td>           
                    </tr> 
              </tr>
              </tbody>
              @endforeach

            </table>
        </div>

    </div>
  </div>