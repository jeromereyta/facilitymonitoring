@extends('layouts.search.master',['searchDomain' => 'upkeep_list'])
@section('search-table')
    <div id="grid">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr style="font-size: 14px;">
                    <th>Upkeep Item</th>
                    <th>Status</th>             
                </tr>
                <tbody>
                @forelse($list as $l)
                    <tr style="font-size: 14px;">
                        
                        <td>{{ $l->name }}</td>
                        <td>{{ $l->status }}</td>
                       
                         @if($params['page_type']=='index')
                        <td>
                        
                            @if(is_null($l->deleted_at))
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Deactivated</span>
                            @endif
                        </td>
                        @endif    
                       
                     
                        @if($params['page_type']=='template_list')
                            <td>
                                <input type="hidden" value="{{$l->id}}" class="addUpkeep">
                                <a href="#!" onclick="addUpkeepToFacility('{{$l->id}}');return false;">Select</a> 
                            </td>
                     @endif
                    </tr>
                @empty
                    <tr style="font-size: 14px;">
                        <td colspan="6" class="text-center">No record found.</td>
                    </tr>
                @endforelse

                </tbody>
            </table>
                    {!! $list->render() !!}
        </div>
            </div>
        </div>
    </div>
@stop