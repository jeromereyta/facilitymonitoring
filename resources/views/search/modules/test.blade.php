@extends('layouts.search.master',['searchDomain' => 'provinces'])
@section('search-table')
    <div id="grid">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>Region</th>
                    <th>Province Name</th>
                </tr>

                <tbody>
                @forelse($list as $l)
                    <tr data-dismiss="modal" onclick="kr.UI.Variables.Modals['{{$params['ui_id']}}'].addOnCloseCallbackParams('sel_row',{{json_encode($l->toArray())}})";>
                        <td>{{$l->region->name}}</td>
                        <td>{{ $l->name }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">No record found.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $list->render() !!}
        </div>
            </div>
        </div>
    </div>
@stop