@extends('layouts.search.master',['searchDomain' => 'inventory_facilities'])
@section('search-table')
    <div id="grid">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>Facility Name</th>
                    <th>Status</th> 
                    <th>Remarks</th>
                </tr>
                <tbody>
                @forelse($list_facility as $l)                  

                   @if($params['page_type']=='inventory_index')
                    <tr>                          
                            @foreach($list_inventory_items as $l_items)
                              @foreach($l_items['facilities'] as $facilities => $facility_value)
                                @foreach($facility_value['inventories'] as $value)
                              
                                  @if($facility_value['id'] == $l->id)
                                    <td>{{$value['inventory_name']}}</td>
                                
                            @inject('inventoryStatus', 'App\Models\InventoryStatus')
                           
                            <td>
                                <div class="form-group">
                                    <?php $monitoringInventory = App\Models\MonitoringInventory::find($params['monitoring_id']); ?>
                                    <?php $inventory_monitoring_id = $l_items['id'];
                                          $facility_id = $facility_value['id']; 
                                          $inventory_id = $value['id']; 

                                          $test =$inventoryStatus::all()->pluck('id');?>
                                          
                                    {!! Form::select('status',['' => ''] +  $inventoryStatus::all()->pluck('name','name')->toArray(),
                                    $value['status'],
                                    ['id'=>''.$inventory_id.'','class' => 'form-control gui-input bg-light arrow-key',
                                    'onchange'=>"updateMonitoringStatus('$inventory_monitoring_id',
                                        '$facility_id','$inventory_id',$(this).val());"]) !!}
                                   <!--  inventory_id, facility_id, inventory_id, new_status -->
                                </div>
                            </td>  
                            <td>
                              <div class="form-group">
                                {!! Form::text('remarks', $value['remarks'] ,['class' => 'form-control gui-input bg-light arrow-key',
                                'onchange'=>"updateMonitoringRemarks('$inventory_monitoring_id',
                                        '$facility_id','$inventory_id',$(this).val());"]) !!}
                              </div>
                            </td>

                        </tr>  
                                  @endif
                                @endforeach
                              @endforeach
                            @endforeach

                            
                    @endif 

                    @if($params['page_type']=='upkeep_index')
                    <tr>               
                            @foreach($list_upkeep_items as $l_items)
                              @foreach($l_items['facilities'] as $facilities => $facility_value)
                                @foreach($facility_value['inventories'] as $value)
                              
                                  @if($facility_value['id'] == $l->id)
                                    <td>{{$value['inventory_name']}}</td>
                          
                            @inject('upkeepStatus', 'App\Models\UpkeepStatus')
                           
                            <td>
                                <div class="form-group">
                                    <?php $inventory_monitoring_id = $l_items['id'];
                                          $facility_id = $facility_value['id']; 
                                          $inventory_id = $value['id']; ?>
                                          
                                    {!! Form::select('status',['' => ''] +  $upkeepStatus::all()->pluck('name','name')->toArray(),
                                    $value['status'],
                                    ['id'=>''.$inventory_id.'','class' => 'form-control gui-input bg-light arrow-key',
                                    'onchange'=>"updateUpkeepMonitoringStatus('$inventory_monitoring_id',
                                        '$facility_id','$inventory_id',$(this).val());"]) !!}
                                   <!--  inventory_id, facility_id, inventory_id, new_status -->
                                </div>
                            </td>  
                            <td>
                              <div class="form-group">
                                {!! Form::text('remarks', $value['remarks'] ,['class' => 'form-control gui-input bg-light arrow-key',
                                'onchange'=>"updateUpkeepMonitoringRemarks('$inventory_monitoring_id',
                                        '$facility_id','$inventory_id',$(this).val());"]) !!}
                              </div>
                            </td>

                        </tr>  
                                  @endif
                                @endforeach
                              @endforeach
                            @endforeach

                            
                    @endif 

                    @if($params['page_type']=='show')
                    <tr>                          
                            @foreach($list_items as $l_items)
                              @foreach($l_items['facilities'] as $facilities => $facility_value)
                                @foreach($facility_value['inventories'] as $value)
                              
                                  @if($facility_value['id'] == $l->id)
                                    <td>{{$value['inventory_name']}}</td>
                                
                            @inject('inventoryStatus', 'App\Models\InventoryStatus')
                           
                            <td>
                                <div class="form-group">
                                    <?php $monitoringInventory = App\Models\MonitoringInventory::find($params['monitoring_id']); ?>
                                    <?php $inventory_monitoring_id = $l_items['id'];
                                          $facility_id = $facility_value['id']; 
                                          $inventory_id = $value['id']; 

                                          $test =$inventoryStatus::all()->pluck('id');?>
                                          
                                    {!! Form::select('status',['' => ''] +  $inventoryStatus::all()->pluck('name','name')->toArray(),
                                    $value['status'],
                                    ['id'=>''.$inventory_id.'','class' => 'form-control gui-input bg-light arrow-key',
                                    'onchange'=>"updateMonitoringStatus('$inventory_monitoring_id',
                                        '$facility_id','$inventory_id',$(this).val());",'disabled'=>'disabled']) !!}
                                   <!--  inventory_id, facility_id, inventory_id, new_status -->
                                </div>
                            </td> 
                            <td>
                              <div class="form-group">
                                {!! Form::text('remarks', $value['remarks'] ,['class' => 'form-control gui-input bg-light arrow-key',
                                'onchange'=>"updateMonitoringRemarks('$inventory_monitoring_id',
                                        '$facility_id','$inventory_id',$(this).val());", 'disabled'=>'disabled']) !!}
                              </div>
                            </td> 
                        </tr>  
                                  @endif
                                @endforeach
                              @endforeach
                            @endforeach

                            
                    @endif 

                        
                    
                    @empty
                    <tr>
                        <td colspan="6" class="text-center">No record found.</td>
                    </tr>

                @endforelse
                                

                </tbody>
            </table>
                    {!! $list_facility->render() !!}
        </div>
      </div>
  
@stop