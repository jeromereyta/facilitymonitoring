@extends('layouts.search.master',['searchDomain' => 'upkeep_facilities'])
@section('search-table')
    <div id="grid">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>Facility Name</th>
                    
                </tr>
                <tbody>                  

                @forelse($list_facility as $l)
                <tr>
                  <td>{{$l->name}}</td>
                  <td> <a href="{{route('upkeep_monitoring.facility_edit', ['monitoring_id' => $params['monitoring_id'], 'facility_id' => $l->id]) }}"><i class="fa fa-pencil"></i> Modify</a></td>

                </tr>
                    @empty
                    <tr>
                        <td colspan="6" class="text-center">No record found.</td>
                    </tr>

                @endforelse
                                

                </tbody>
            </table>
                    {!! $list_facility->render() !!}
        </div>
      </div>
  
@stop