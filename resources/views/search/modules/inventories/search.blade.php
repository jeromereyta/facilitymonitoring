@extends('layouts.search.master',['searchDomain' => 'inventories'])
@section('search-table')
    <div id="grid">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr style="font-size: 14px;">
                    <th>Inventory Item</th>
                    <th>Status</th>             
                </tr>
                <tbody>
                @forelse($list as $l)
                    <tr style="font-size: 14px;">
                       
                  
                        <td>{{ $l->name }}</td>
                        <td>{{ $l->status }}</td>
                       
                         @if($params['page_type']=='index')
                       <tr>
                        <td><!--<img class="thumb" src="{{$l->thumbnail}}" style="width:29px;margin:0px 11px 0px 0px"/>-->{{ $l->name }}</td>
                        <td>{{$l->subcommodity!=null?$l->subcommodity->commodity!=null?$l->subcommodity->commodity->name:'-':'-' }}</td>
                        <td>{{$l->subcommodity!=null?$l->subcommodity->name:'-' }}</td>
                        
                    </tr>
                        @endif    
                       
                     
                        @if($params['page_type']=='template_list')
                            <td>
                                <input type="hidden" value="{{$l->id}}" class="addInventory">
                                <a href="#!" onclick="addInventoryToFacility('{{$l->id}}');return false;">Select</a> 
                            </td>
                        @elseif($params['page_type']=='template_selected')
                            <td>
                                <input type="hidden" value="{{$l->id}}" class="deleteInventory">
                                <a href="#! " onclick="deleteInventoryFromFacility('{{$l->id}}');return false;">Deselect</a> 
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr style="font-size: 14px;">
                        <td colspan="6" class="text-center">No record found.</td>
                    </tr>
                @endforelse

                </tbody>
            </table>
                    {!! $list->render() !!}
        </div>
            </div>
        </div>
    </div>
@stop