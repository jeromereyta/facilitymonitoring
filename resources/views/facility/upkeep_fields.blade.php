 
            
            @foreach($inventories as $local => $inventory)
            @endforeach
      
            @foreach($facility as $local => $facilities)
            @endforeach

  <div class="row">
      <div class="col-md-6">
          <div class="panel">
              <div class="panel-heading">
          <span class="panel-title">
            <span class="fa fa-list"></span>List of Upkeep Items</span>
              </div>
              <div class="panel-body pn">
                  <div id="upkeep_list_search">
                  </div>
              </div>
          </div>
      </div>
            <div class="col-md-6">
          <div class="panel">
              <div class="panel-heading">
          <span class="panel-title">
            <span class="fa fa-list"></span>List of Selected Upkeep Items</span>
              </div>
              <div class="panel-body pn">
                  <div id="upkeep_inventory_search">
                  </div>
              </div>
          </div>
      </div>
  </div>

@section('footer_plugins')
 
    <script>
        function addUpkeepToFacility(){
            $(".addUpkeep").each(function() {
                addUpkeepToFacility($(this).val());
            });
        }

        function deleteUpkeepFromFacility(){
            $(".deleteUpkeep").each(function() {
                deleteUpkeepFromFacility($(this).val());
            });
        }

        function addUpkeepToFacilityCallback(params){
          console.log(params);
            kr.UI.Variables.Search['Upkeep_list_search'].searchMe();
            kr.UI.Variables.Search['selected_Upkeep_search'].searchMe();
        }
        function deleteUpkeepFromFacilityCallback(params){
          console.log(params);
            kr.UI.Variables.Search['Upkeep_list_search'].searchMe();
            kr.UI.Variables.Search['selected_Upkeep_search'].searchMe();
            //initializeUsersSearch();
            //initializeSelectedUsersSearch();
        }
        function addUpkeepToFacility(upkeep_id){
        
            var facility_id = '{{$facilities->id}}';
            kr.Dependencies.Ajax.run({url:"{{URL::route('facility.addUpkeep')}}",callback:addUpkeepToFacilityCallback,params:{'facility_id':facility_id,'upkeep_id':upkeep_id}, dataType:'json'});
        }
        function deleteUpkeepFromFacility(upkeep_id){
           
            var facility_id = '{{$facilities->id}}';
            kr.Dependencies.Ajax.run({url:"{{URL::route('facility.deleteUpkeep')}}",callback:deleteUpkeepFromFacilityCallback,params:{'facility_id':facility_id,'upkeep_id':upkeep_id}, dataType:'json'});
        }
 
        function initializeUpkeepSearch(){
            new kr.UI.Modal({id:"Upkeep_list_search",isModal:false,url:"{{URL::route('search.search',array('searchDomain'=>'upkeep'))}}" ,params:<?php echo json_encode(array('page_type'=>'upkeep_list','facility_id'=>$item->id)) ?>,title:"Search Upkeep Items"});

        }
        function initializeSelectedUpkeepSearch(){
            new kr.UI.Modal({id:"selected_Upkeep_search",isModal:false,url:"{{URL::route('search.search',array('searchDomain'=>'upkeep'))}}",params:<?php echo json_encode(array('page_type'=>'upkeep_selected','facility_id'=>$item->id)) ?>,title:"Search Listed Items"});
        
        }

        kr.Dependencies.addLoadEvent(function(){
            initializeUpkeepSearch();
            initializeSelectedUpkeepSearch();
        });
    </script>
@stop
