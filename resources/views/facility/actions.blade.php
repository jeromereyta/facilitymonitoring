<div class="btn-group">
            <a class="btn btn-flat btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions
                <span class="caret ml5"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                @can('facility-read')
                <li>
                    <a href="{{ route('facility.show', $facility->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View Facility</a>
                </li>
                @endcan
                @if($facility->availability=='Deleted')
                <li>
                    <a href="{{ route('facility.undelete', $facility->id) }}"><i class="glyphicon glyphicon-edit"></i> Undelete Sheet</a>
                </li>

                @else
                @can('facility-update')
                <li>
                    <a href="{{ route('facility.edit', $facility->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit Facility</a>
                </li>
                @endcan
                @can('facility-delete')
                <li>
                    <a href="{{ route('facility.delete', $facility->id) }}" class="alert-link" onclick='return confirm("Are you sure you want to delete ?")'><span class="label label-warning"><i class="glyphicon glyphicon-trash"></i></span> Delete</a>
                </li>
                @endcan
                @endif



            </ul>
</div>