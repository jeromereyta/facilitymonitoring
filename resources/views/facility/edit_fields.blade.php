<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
                <div class="box-header with-border">
                  <h5 class="box-title"><i class="fa fa-pencil"></i>Facility Details</h5>
                </div>

                    <div class="panel-body"> 
        

  <div class="container col-md-12">
 @include('flash::message')   
            @foreach($facility as $item)
            {!! Form::open(['method' => 'PUT', 'route' => ['facility.update', $item->id], 'class' => 'form-vertical']) !!}
                <div class="col-md-4">

                    <div class="form-group">
                        <label class="field prepend-icon mt5" for="name">Name: </label>
                        <input type="text" value = "{{$item->name}}"  class="form-control" name="name" placeholder="Enter Name">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="pwd">Facility Type:</label>
                        <select class="form-control select2" name ="category" >
                        @foreach($categories as $key => $category)
                        <option value ="{{$category->id}}">{{$category->name}}</option>
                        @if($category->name==$item->room_type)
                        <option selected="selected" hidden value="{{$category->id}}" >{{$category->name}}</option>
                        @endif
                        @endforeach
                    </select>
                 </div>
                </div>

            
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="description">Description:</label>
                        <textarea name ="description" class="form-control"  rows="3"  >{{$item->description}}</textarea>
                    </div>
                </div>            
                      </div>
                </div>
            </div>     

            @endforeach

                @if($inventories)
                    @include('facility.inventory_fields')            
                @endif 

                     <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-default btn-sm ph15">Submit</button>
                                    <a href="{!! route('facility.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                                </div>
                            </div>
                        </div><!-- box-footer -->    
                        {!! Form::close() !!}       
                            <!-- Vendor JS -->


        </div>
</div>