 
            
            @foreach($inventories as $local => $inventory)
            @endforeach
      
            @foreach($facility as $local => $facilities)
            @endforeach


<!-- Modal -->
<div class="modal fade" id="inventoryListModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
         
          <div class="box">
            <div class="box-header">
          <span class="box-title">
            <span class="fa fa-list"></span>List of Inventory Items</span>
              </div>
              <div class="panel-body pn">
                  <div id="inventory_list_search">
                  </div>
              </div>
          </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="upkeepListModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
          <div class="box">
            <div class="box-header">
          <span class="box-title">
            <span class="fa fa-list"></span>List of Upkeep Items</span>
              </div>
              <div class="panel-body pn">
                  <div id="upkeep_list_search">
                  </div>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


  <div class="row">
      
      <div class="col-md-6">
          <div class="box">
            <div class="box-header">
          <span class="box-title">
            <span class="fa fa-list"></span>List of Selected Inventory Items</span>
              </div>  
              <div class="panel-body pn">

                  <!-- Button trigger modal -->
                 
                  <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#inventoryListModal">
                    <span class = "fa fa-plus-square"> </span>
                  </button>

                  <div id="selected_inventory_search">
                  </div>
              </div>
          </div>
      </div>

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
          <span class="box-title">
            <span class="fa fa-list"></span>List of Selected Upkeep Items</span>
              </div>
              <div class="panel-body pn">
              
                 <!-- Button trigger modal -->
              <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#upkeepListModal">
                <span class = "fa fa-plus-square"> </span>
              </button>

                  <div id="selected_upkeep_search">
                  </div>
              </div>
          </div>
      </div>

  </div>


@section('footer_plugins')
 
  <!-- REQUIRED JS SCRIPTS -->
  <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
  

    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
    
     <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/kr.js') }}"></script>

    
    <script>

      function addUpkeepCallback(params){
        $('#'+'municipality_id').val(params['sel_row']['id']);
        $('#'+'municipality_name').val(params['sel_row']['name']);
        console.log(params);
      }
      function initializeUpkeepModal(id){
          
          kr.UI.Modal({id:id,url:"{{URL::route('search.search', array('searchDomain'=>'upkeep'))}}",title:"Search Municipalities",onCloseCallback:addUpkeepCallback});
      }



        function addInventoryToFacility(){
            $(".addInventory").each(function() {
                addInventoryToFacility($(this).val());
            });
        }

        function deleteInventoryFromFacility(){
            $(".deleteInventory").each(function() {
                deleteInventoryFromFacility($(this).val());
            });
        }
        function addUpkeepToFacility(){
            $(".addUpkeep").each(function() {
                addUpkeepToFacility($(this).val());
            });
        }

        function deleteUpkeepFromFacility(){
            $(".deleteUpkeep").each(function() {
                deleteUpkeepFromFacility($(this).val());
            });
        }

        function addInventoryToFacilityCallback(params){
          console.log(params);
            kr.UI.Variables.Search['inventory_list_search'].searchMe();
            kr.UI.Variables.Search['selected_inventory_search'].searchMe();
        }
        function deleteInventoryFromFacilityCallback(params){
          console.log(params);
            kr.UI.Variables.Search['inventory_list_search'].searchMe();
            kr.UI.Variables.Search['selected_inventory_search'].searchMe();
            //initializeUsersSearch();
            //initializeSelectedUsersSearch();
        }
        function addUpkeepToFacilityCallback(params){
          console.log(params);
            kr.UI.Variables.Search['upkeep_list_search'].searchMe();
            kr.UI.Variables.Search['selected_upkeep_search'].searchMe();
        }
        function deleteUpkeepFromFacilityCallback(params){
          console.log(params);
            kr.UI.Variables.Search['upkeep_list_search'].searchMe();
            kr.UI.Variables.Search['selected_upkeep_search'].searchMe();
            //initializeUsersSearch();
            //initializeSelectedUsersSearch();
        }

        function addInventoryToFacility(inv_id){
          console.log(inv_id);
            var facility_id = '{{$facilities->id}}';
            kr.Dependencies.Ajax.run({url:"{{URL::route('facility.addInventory')}}",callback:addInventoryToFacilityCallback,params:{'facility_id':facility_id,'inv_id':inv_id}, dataType:'json'});
        }
        function deleteInventoryFromFacility(inv_id){
           console.log(inv_id);
            var facility_id = '{{$facilities->id}}';
            kr.Dependencies.Ajax.run({url:"{{URL::route('facility.deleteInventory')}}",callback:deleteInventoryFromFacilityCallback,params:{'facility_id':facility_id,'inv_id':inv_id}, dataType:'json'});
        }
        function addUpkeepToFacility(upkeep_id){
        
            var facility_id = '{{$facilities->id}}';
            kr.Dependencies.Ajax.run({url:"{{URL::route('facility.addUpkeep')}}",callback:addUpkeepToFacilityCallback,params:{'facility_id':facility_id,'upkeep_id':upkeep_id}, dataType:'json'});
        }
        function deleteUpkeepFromFacility(upkeep_id){
           
            var facility_id = '{{$facilities->id}}';
            kr.Dependencies.Ajax.run({url:"{{URL::route('facility.deleteUpkeep')}}",callback:deleteUpkeepFromFacilityCallback,params:{'facility_id':facility_id,'upkeep_id':upkeep_id}, dataType:'json'});
        }
 
        function initializeInventorySearch(){
            new kr.UI.Modal({id:"inventory_list_search",isModal:false,url:"{{URL::route('search.search',array('searchDomain'=>'inventories'))}}" ,params:<?php echo json_encode(array('page_type'=>'template_list','facility_id'=>$item->id)) ?>,title:"Search Inventory Items"});

        }
        function initializeSelectedInventorySearch(){
            new kr.UI.Modal({id:"selected_inventory_search",isModal:false,url:"{{URL::route('search.search',array('searchDomain'=>'inventories'))}}",params:<?php echo json_encode(array('page_type'=>'template_selected','facility_id'=>$item->id)) ?>,title:"Search Listed Inventory"});
        
        }
         function initializeUpkeepSearch(){
            new kr.UI.Modal({id:"upkeep_list_search",isModal:false,url:"{{URL::route('search.search',array('searchDomain'=>'upkeep'))}}" ,params:<?php echo json_encode(array('page_type'=>'template_list','facility_id'=>$item->id)) ?>,title:"Search Upkeep Items"});

        }
        function initializeSelectedUpkeepSearch(){
            new kr.UI.Modal({id:"selected_upkeep_search",isModal:false,url:"{{URL::route('search.search',array('searchDomain'=>'upkeep'))}}",params:<?php echo json_encode(array('page_type'=>'template_selected','facility_id'=>$item->id)) ?>,title:"Search Listed Items"});
        
        }

        kr.Dependencies.addLoadEvent(function(){
            initializeInventorySearch();
            initializeSelectedInventorySearch();
            initializeUpkeepSearch();
            initializeSelectedUpkeepSearch();
        });

    
    </script>
@stop
