@extends('partials.master')

@section('title')
        <a href = "#">Create New Facility</a>
@endsection

@section('content')

 
    <div class="row">
@include('flash::message')
                  <div id="report_form_append">
                    @if($errors->all())
                      <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                          @if($error != '')
                          <li>{{$error}}</li>
                          @endif
                        @endforeach
                      </ul>
                    @endif
                  </div>


        {!! Form::open(['route' => 'facility.store','class' => 'form-vertical']) !!}

            @include('facility.fields')
        {!! Form::close() !!}
    </div>
@endsection

@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection