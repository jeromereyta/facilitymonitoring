    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Inventory Items</h3>

            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table id="example2" class="table table-responsive">
                <thead>
                <tr style="font-size: 15px;  font-weight: bold;">
                  <th>Inventory Name</th>
                  <th>Serial Number</th>
                  <th>Category</th>
                  <th>Status</th>
                  <th>Date Created</th>
                </tr>
                </thead>
                <tbody>
                @if($inventories)
                @foreach($inventories as $key => $inventory)
                <tr style="font-size: 15px;">
                <td>{{$inventory->name}}</td>
                <td>{{$inventory->serial_no}}</td>
                <td>{{$inventory->category}}</td>
                <td>{{$inventory->status}}</td>
                <td>{{$inventory->created_at}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            @endif
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
