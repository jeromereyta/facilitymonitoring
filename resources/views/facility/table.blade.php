    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Facilities</h3>
              @can('facility-create')
              <a class="btn btn-flat btn-primary pull-right" href="{!! route('facility.create') !!}">Add New</a>
              @endcan
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table id="example2" class="table table-responsive">
                <thead>
                  <tr style="font-size: 15px;  font-weight: bold;">
                  <th>Facility  Name</th>
                  <th>Facility Type</th>
                  <th>Description</th>
                  <th>Status</th>
            <!--       <th>Items Tagged</th> -->
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($facilities as $key => $facility)
                  
                    <tr style="font-size: 15px; ">
                    <td>{{$facility->name}}</td>
                    <td>{{$facility->room_type}}</td>
                    <td>{{$facility->description}}</td>
                    @if($facility->availability == 'Active')
                        <td><span class="label label-success">{{$facility->availability}}</span></td>
                      @else
                        <td><span class="label label-danger">{{$facility->availability}}</span></td>
                    @endif
          
                    <td>
                        @include('facility.actions')
                    </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            {{ $facilities->links() }}
            <!-- /.box-body -->
          </div>
</div>