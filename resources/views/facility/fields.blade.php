<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
                <div class="box-header with-border">
                  <h5 class="box-title"><i class="fa fa-pencil"></i>Facility Details</h5>
                </div>

                    <div class="panel-body"> 

                    <h2>Facility Details</h2>
              
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label" for="name">Name:</label>
                              <input type="text" class="form-control" name="name" placeholder="Enter Name">
                          </div>    
                      </div>   
                
                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label" for="pwd">Room Type:</label>
                              @if($categories)
                              <select class="form-control select2" name ="category" >
                              @foreach($categories as $key =>$category)
                              <option value="{{$category->id}}">{{$category->name}}</option>
                              @endforeach
                              </select>
                              @endif
                          </div>    
                      </div>  

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label" for="description">Description:</label>
                            <textarea  name ="description" class="form-control" rows="3" placeholder="Enter description. . ."></textarea>
                          </div>    
                      </div>  

                  </div>
              </div>     

              <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-default btn-sm ph15">Submit</button>
                            <a href="{!! route('facility.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                        </div>
                    </div>
                </div><!-- box-footer -->    
      </div>
</div>

