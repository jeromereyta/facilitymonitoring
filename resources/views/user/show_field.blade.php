<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h5 class="box-title"><i class="fa fa-pencil"></i>User Details</h5>
            </div>

                    <div class="panel-body">
                            <div class="admin-form theme-primary">
                                <div class="container">
                                                                
                       <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" disabled value="{{$users->name }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" disabled name="email" value="{{$users->email}}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                                @if($roles)


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Role</label>
                   <div class="col-md-6">
                      
                    <select class="form-control select2" name ="role_id" disabled >
                    @foreach($roles as $key => $category)
                    <option value ="{{$category->id}}">{{$category->name}}</option>
                    @if($category->name==$role[0]->name)
                    <option selected="selected" hidden value="{{$category->id}}" >{{$category->name}}</option>
                    @endif
                    @endforeach
                </select>
                        </div>
                        </div>
                
             


                            @endif
              </form>
                                </div>



                        </div>        
                    </div>
            </div>

            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-10">
                        <a href="{!! route('users.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                    </div>
                </div>
            </div><!-- box-footer -->   
             
        </div>
</div>