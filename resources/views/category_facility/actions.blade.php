<div class="btn-group">
            <a class="btn btn-flat btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions
                <span class="caret ml5"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('category_facility.show', $category->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View Monitoring</a>
                </li>
                @can('facility-update')
                <li>
                    <a href="{{ route('category_facility.edit', $category->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit Sheet</a>
                </li>
                @endcan
                @can('facility-delete')
                <li>
                    <a href="{{ route('category_facility.delete', $category->id) }}" class="alert-link" onclick='return confirm("Are you sure you want to delete ?")'><span class="label label-warning"><i class="glyphicon glyphicon-trash"></i></span> Delete</a>
                </li>
                @endcan

            </ul>
</div>