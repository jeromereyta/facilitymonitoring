    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Facility Category</h3>
              @can('facility-create')
              <a class="btn btn-flat btn-primary pull-right" href="{!! route('category_facility.create') !!}">Add New</a>
              @endcan
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table id="example2" class="table table-responsive">
                <thead>
                <tr style="font-size: 15px;  font-weight: bold;">
                  <th>Category Name</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($categories)
                @foreach($categories as $key => $category)
                <tr style="font-size: 15px;">
                <td>{{$category->name}}</td>
                <td>{{$category->description}}</td>
                <td>
                    @include('category_facility.actions')
                </td>
                </tr>
                @endforeach
                @endif
                </tbody>
              </table>
            </div>
            {{ $categories->links() }}
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
