<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h5 class="box-title"><i class="fa fa-pencil"></i>Category Details</h5>
            </div>

                    <div class="panel-body">
                            <div class="admin-form theme-primary">
                                <div class="container">

                                @if($category)
                            
                                     <div class="col-md-4">
                                            <div class="form-group">
                                            <label class="field prepend-icon mt5" for="name">Name: </label>
                                            <input  value ="{{$category->name}}" type="text" class="form-control" name="name" placeholder="Enter Name">
                                            </div>
                                        </div>
                                    
                                                                      

                                        <div class="col-md-4">
                                            <div class="form-group">
                                            <label class="field prepend-icon mt5" for="description">Description:</label>
                                            <textarea  name ="description" class="form-control" rows="3" placeholder="Enter description. . .">{{$category->description}}</textarea>
                                            </div>    
                                        </div>                                    
                                 @else
                                          <div class="col-md-4">
                                            <div class="form-group">
                                            <label class="field prepend-icon mt5" for="name">Name: </label>
                                            <input  type="text" class="form-control" name="name" placeholder="Enter Name">
                                            </div>
                                        </div>
                                    
                                        <div class="col-md-4">
                                            <div class="form-group">
                                            <label class="field prepend-icon mt5" for="description">Description:</label>
                                            <textarea  name ="description" class="form-control" rows="3" placeholder="Enter description. . ."></textarea>
                                            </div>    
                                        </div>

                                 @endif

                                </div>




                        </div>        
                    </div>
            </div>

            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-default btn-flat btn-sm ph15">Submit</button>
                        <a href="{!! route('category_facility.index') !!}" class="btn btn-default btn-flat btn-sm ph15">Back</a>
                    </div>
                </div>
            </div><!-- box-footer -->   
             
        </div>

</div>