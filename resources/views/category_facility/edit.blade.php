@extends('partials.master')

@section('title')
        <a href = "#">Edit Facility Category</a>
@endsection

@section('content')

@include('flash::message')
                                                                      {!! Form::open(['method' => 'PUT', 'route' => ['category_facility.update', $category->id], 'class' => 'form-horizontal', 'files' => 'true']) !!}
@include('category_facility.fields')
                              {!! Form::close() !!}        


 
@endsection


@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection