<script>
    $('.del-item').click(function(e) {
        e.preventDefault();
        var item_name ='';
        if(e.target.value==''){
            item_name = "this item?";
        }else{
            item_name = e.target.value+"?";
        }
        kr.UI.Modal({id:"delete_modal",confirm:true,title:"Please Confirm",content:"Are you sure you want to delete "+ item_name,onCloseCallback:cb_del_confirm,onCloseCallbackParams:{'btn':e}});
        kr.UI.Variables.Modals['delete_modal'].show();
    });
    function cb_del_confirm(params){
        var confirm = kr.UI.Variables.Modals['delete_modal'].onCloseCallbackParams.confirm||false;
        var btn = kr.UI.Variables.Modals['delete_modal'].onCloseCallbackParams.btn.target;
        if(confirm){
            console.log($(btn).parents('form'));
            $(btn).parents('form')[0].submit();
        }else{
            //if false do nothing ignore the shit out of everything.kr rules.
        }
    }
    //for product lists in search
    function addSubcommodityCallback(params){
        $('#'+'subcommodity_id').val(params['sel_row']['id']);
        $('#'+'subcommodity_name').val(params['sel_row']['name']);
        kr.UI.Variables.Search['{{$params['ui_id']}}'].searchMe();
        console.log(params);
    }
    function initializeSubcommodityModal(id){
        commodity_id = $('#commodity_id').val();
        if(!commodity_id){
            return;
        }
        kr.UI.Modal({id:id,url:"{{URL::route('search.search', array('searchDomain'=>'subcommodities'))}}",title:"Search Commodity Type",onCloseCallback:addSubcommodityCallback});
    }

    function addWeeklyPrevailingSubcommodityCallback(params){
        $('#'+'subcommodity_id').val(params['sel_row']['id']);
        $('#'+'subcommodity_name').val(params['sel_row']['name']);
        kr.UI.Variables.Search['{{$params['ui_id']}}'].searchMe();
        console.log(params);
    }
    function initializeWeeklyPrevailingSubcommodityModal(id){
        // commodity_id = $('#commodity_id').val();
        // if(!commodity_id){
        //     return;
        // }
        kr.UI.Modal({id:id,url:"{{URL::route('search.search', array('searchDomain'=>'subcommodities'))}}",title:"Search Commodity Type",onCloseCallback:addWeeklyPrevailingSubcommodityCallback});
    }
    //for product lists in search
    function addCommodityCallback(params){
        $('#'+'commodity_id').val(params['sel_row']['id']);
        $('#'+'commodity_name').val(params['sel_row']['name']);
        kr.UI.Variables.Search['{{$params['ui_id']}}'].searchMe();

    }
    function initializeCommodityModal(id){
        $('#'+'subcommodity_id').val('');
        $('#'+'subcommodity_name').val('');
        $('#'+'commodity_id').val('');
        $('#'+'commodity_name').val('');
        kr.UI.Modal({id:id,url:"{{URL::route('search.search', array('searchDomain'=>'commodities'))}}",title:"Search Commodity",onCloseCallback:addCommodityCallback});
    }

    new kr.Utilities.Search("{{URL::route('search.search',array('searchDomain'=>$searchDomain))}}",'{{$params['ui_id']}}');
    $('#{{$params['ui_id']}}').find("#searchForm").on('submit', function(e){
        console.log(e);
        e.preventDefault();
    });
    function initializeInformationModal(id,title,content){
        return new kr.UI.Modal({id:id,title:title,content:content});
    }
</script>