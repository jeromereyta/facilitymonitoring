<div name="inlineSearchDiv"id="inlineSearchDiv">
    {!! Form::open(array('id'=>'searchForm','url' => 'search.search')) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                        <div class="box-tools">
                            <div class="input-group input-group-sm pull-right" style="width:300px">
                                @foreach($params as $key=>$value)
                                    @if($key!="searchQuery"&&$key!="_token"&&$key!="sort"&&$key!="order"&&$key!="view_thrashed_rows")
                                        @if($searchDomain=='products')
                                            @if($key!="commodity_id"&&$key!="commodity_name"&&$key!="subcommodity_id"&&$key!="subcommodity_name")
                                                <input id="{{$key}}"name="{{$key}}"type="hidden" class="form-control" value="{{$value}}"/>
                                            @endif
                                        @else
                                        <input id="{{$key}}"name="{{$key}}"type="hidden" class="form-control" value="{{$value}}"/>
                                        @endif
                                    @endif
                                @endforeach
                                <input id="searchQuery"name="searchQuery"type="text" class="form-control pull-right" value="{{$params['searchQuery'] or null}}"onchange="kr.UI.Variables.Search['{{$params['ui_id']}}'].searchMe();"/>
                                <div class="input-group-btn">
                                    <a href="#" class="btn btn-default" onclick="kr.UI.Variables.Search['{{$params['ui_id']}}}'].searchMe();"><i class="fa fa-search"></i></a>
                                </div>
                           
                            </div>
                           
                        </div>
                    {!! Form::close() !!}
            </div>
            </div>
                </div>
    <div class="row">
        <div class="col-xs-12">
   