<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
      <!--   <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p> -->
                <!-- Status -->
       <!--          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->

        <!-- search form (Optional) -->
       <!--  <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
        
            <span class="input-group-btn">
              <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
            
            </div>
        </form> -->
        <!-- /.search form -->

       <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
           
            <li class="treeview">
                <a href="#"><i class="fa fa-angle-left pull-right"></i><span>Monitoring</span></a>
                <ul class="treeview-menu">
                    @can('monitoring_inventory-read')
                    <li><a class="ajax-link" href="{{ url('inventory_monitoring') }}"><i class="glyphicon glyphicon-home"></i><span> Inventory </span></a></li>
                    @endcan
                    @can('monitoring_upkeep-read')
                    <li><a class="ajax-link" href="{{ url('upkeep_monitoring') }}"><i class="glyphicon glyphicon-home"></i><span> Upkeep </span></a></li>
                    @endcan
                </ul>
        </li>
     

        <li class="treeview">
                <a href="#"><i class="fa fa-angle-left pull-right"></i><span>Admin</span></a>
                <ul class="treeview-menu">
                        <li class="treeview">
                        @can('facility-read')
                         <a href="#"><i class="fa fa-angle-left pull-right"></i><span> Facility Maintenance</span></a>
                            <ul class="treeview-menu">
                                <li><a class="ajax-link" href="{{ url('facility') }}"><i class="glyphicon glyphicon-th-large"></i><span> Facilities </span></a></li>
                                <li><a class="ajax-link" href="{{ url('/category_facility') }}"><i class="glyphicon glyphicon-th-large"></i><span> Facility Category </span></a></li>
                            </ul> 
                        </li>
                        @endcan
                        @can('item-read')    
                        <li class="treeview">
                         <a href="#"><i class="fa fa-angle-left pull-right"></i><span> Items Maintenance</span></a>
                            <ul class="treeview-menu">
                                <li><a class="ajax-link" href="{{ url('/inventory') }}"><i class="glyphicon glyphicon-th-large"></i><span> Inventory Items </span></a></li>
                                <li><a class="ajax-link" href="{{ url('upkeep_item') }}"><i class="glyphicon glyphicon-th-large"></i><span> Upkeep Items </span></a></li>
                            </ul> 
                        </li>
                        @endcan
                        @can('status-read')
                        <li class="treeview">
                         <a href="#"><i class="fa fa-angle-left pull-right"></i><span> Status Maintenance</span></a>
                            <ul class="treeview-menu">
                                <li><a class="ajax-link" href="{{ url('/status_inventory') }}"><i class="glyphicon glyphicon-th-large"></i><span> Inventory Status</span></a></li>
                                <li><a class="ajax-link" href="{{ url('/status_upkeep') }}"><i class="glyphicon glyphicon-th-large"></i><span> Upkeep Status</span></a></li>
                            </ul> 
                        </li>
                        @endcan
                        @can('user-read')
                        <li class="treeview">
                         <a href="#"><i class="fa fa-angle-left pull-right"></i><span> User Maintenance</span></a>
                            <ul class="treeview-menu">
                                <li><a class="ajax-link" href="{{ url('users') }}"><i class="glyphicon glyphicon-th-large"></i><span> Users </span></a></li>
                                <li><a class="ajax-link" href="{{ url('/roles') }}"><i class="glyphicon glyphicon-th-large"></i><span> Roles</span></a></li>
                                <!-- <li><a class="ajax-link" href="{{ url('/rooms') }}"><i class="glyphicon glyphicon-th-large"></i><span> User Groups </span></a></li> -->
                            </ul> 
                        </li>
                        @endcan



                        </li>
                
                </ul>
        </li>
        <li><a class="ajax-link" href="{{ url('/reports') }}"><i class="glyphicon glyphicon-book"></i><span>Reports</span></a></li>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>