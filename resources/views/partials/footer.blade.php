<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Facility Monitoring System
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2016 <a href="http://itdc.up.edu.ph">UPITDC</a>.</strong> All rights reserved.

</footer>