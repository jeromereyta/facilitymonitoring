<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo"><b>Facility Monitoring</b></a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="{{Route::current()->getName()}}" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
        

               
                <!-- Tasks Menu -->
             
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                       <!--  <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="user-image" alt="User Image"/>
                         --><!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-m">Hi, {{Auth::user()->full_name!=null?Auth::user()->full_name:Auth::user()->email}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <!-- <li class="user-header">
                            <img src="" class="img-circle" alt="User Image" />
                            <p>
                                Alexander Pierce - Web Developer
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li> -->
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                               
                            </div>
                            <div class="col-xs-4 text-center">
                                 <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                            
                        </li>
                        <!-- Menu Footer-->
                       <!--  <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="">
                                
                            </div>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>