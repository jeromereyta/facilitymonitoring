<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
                <div class="box-header with-border">
                  <h5 class="box-title"><i class="fa fa-pencil"></i>Upkeep Item Details</h5>
                </div>

    @include('flash::message')
                    <div class="panel-body">
                            <div class="admin-form theme-primary">

                                <div class="container">
                                  <h2>Item Details</h2>
                                   
                                            @foreach($inventory as $item)
                                            {!! Form::open(['method' => 'PUT', 'route' => ['upkeep_item.update', $item->id], 'class' => 'form-vertical','role'=>'form', 'files' => 'true']) !!}

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="field prepend-icon mt5" for="name">Name: </label>
                                                        <input type="text" value = "{{$item->name}}"  class="form-control" name="name" placeholder="Enter Name">
                                                    </div>
                                                </div>
                                          

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="field prepend-icon mt5" for="description">Description:</label>
                                                        <textarea  name ="description" class="form-control" rows="3" placeholder="{{$item->description}}"></textarea>
                                                    </div>    
                                                </div>  
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                <label class="field prepend-icon mt5" for="name">Remarks Required?: </label></br>
                                                <input type="checkbox" name="remarks_required" id="remarks_required" value="1" {{$item->is_remarks_required == 1 ? 'checked' : '' }}>
                                            </div>    
                                        </div>   

                                        @endforeach

                            </div>
                       </div>
                </div>
            </div>     

                     <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-default btn-sm ph15">Submit</button>
                                    <a href="{!! route('upkeep_item.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                                </div>
                            </div>
                        </div><!-- box-footer -->    
                        {!! Form::close() !!}        
        </div>
</div>