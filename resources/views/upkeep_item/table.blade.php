    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Inventory Items</h3>
                <a class="btn btn-primary btn-flat pull-right" href="{!! route('upkeep_item.create') !!}">Add New</a>
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table id="example2" class="table table-responsive">
                <thead>
                  <tr style="font-size: 15px;  font-weight: bold;">
                  <th>Item Name</th>
                  <th>Description
                  <th>Date Created</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($inventories as $key => $inventory)
                
                  <tr style="font-size: 15px; ">
                <td>{{$inventory->name}}</td>
                <td>{{$inventory->description}}</td>
                <td>{{$inventory->created_at}}</td>
                <td>
                    @include('upkeep_item.actions')
                </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
             {{ $inventories->links() }}
            <!-- /.box-body -->
          </div>
</div>