    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Inventory Items</h3>
        @can('item-update')
        <a class="btn btn-flat btn-primary pull-right" href="{!! route('inventory.create') !!}">Add New</a>
        @endcan
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table id="example2" class="table table-responsive">
                <thead>
                <tr style="font-size: 15px;  font-weight: bold;">
                  <th>Inventory Name</th>
                  <th>Serial Number</th>
                  <th>Status</th>
                  <th>Date Created</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($inventories as $key => $inventory)
                <tr style="font-size: 15px;">
                <td>{{$inventory->name}}</td>
                <td>{{$inventory->serial_no}}</td>
                <td>{{$inventory->status}}</td>
                <td>{{$inventory->created_at}}</td>
                <td>
                    @include('inventory.actions')
                </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            {{ $inventories->links() }}
            <!-- /.box-body -->
          </div>
</div>
</div>
</div>
