<div class="btn-group">
            <a class="btn btn-flat btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions
                <span class="caret ml5"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('inventory.show', $inventory->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View Monitoring</a>
                </li>

                @if($inventory->status=='Deleted')
                <li>
                    <a href="{{ route('inventory.undelete', $inventory->id) }}"><i class="glyphicon glyphicon-edit"></i> Undelete Sheet</a>
                </li>

                @else
                @can('item-update')
                <li>
                    <a href="{{ route('inventory.edit', $inventory->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit Sheet</a>
                </li>
                @endcan
                @can('item-delete')
                <li>
                    <a href="{{ route('inventory.delete', $inventory->id) }}" class="alert-link" onclick='return confirm("Are you sure you want to delete ?")'><span class="label label-warning"><i class="glyphicon glyphicon-trash"></i></span> Delete</a>
                </li>
                @endcan
                @endif


            </ul>
</div>