@extends('partials.master')

@section('title')
        <a href = "#">Create New Inventory Monitoring</a>
@endsection

@section('content')

 
    <div class="row">
@include('flash::message')

        {!! Form::open(['route' => 'inventory_monitoring.store']) !!}
        @include('inventory_monitoring.fields')

        {!! Form::close() !!}
    
    </div>
@endsection