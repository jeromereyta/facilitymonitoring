
<div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-heading">
          <span class="panel-title">
            <span class="fa fa-list"></span>List of Facility</span>
        </div>
        <div class="panel-body pn">
          <div id="facility_search">
                      
          </div>
        </div>
      </div>
    </div>
  </div>

@section('footer_plugins')
 
  <script src="{{ URL::asset('js/all.js') }}"></script>

    <script>
    function initializeFacilitySearch(){
          modal = new kr.UI.Modal({id:"facility_search",isModal:false,url:"{{URL::route('search.search', array('searchDomain'=>'inventory_facilities'))}}",params:{'page_type':'inventory_index','monitoring_id':<?php echo $value['id']; ?>},title:"Search Facility"});
      }
    kr.Dependencies.addLoadEvent(function(){
        initializeFacilitySearch();
      });

    function updateMonitoringStatus(inventory_monitoring_id,facility_id,inventory_id,status){
           //if(!value||value==0)return;
           var inventory_monitoring_id = '<?php echo $value['id']; ?>';
          
           kr.Dependencies.Ajax.run({url:"{{URL::route('inventory_monitoring.updateInventoryStatus','inventory_monitoring_id')}}",params:{'inventory_monitoring_id':inventory_monitoring_id,'facility_id':facility_id,'inventory_id':inventory_id,'status':status}, dataType:'json'});

       }

    function updateMonitoringRemarks(inventory_monitoring_id,facility_id,inventory_id,remarks){
           //if(!value||value==0)return;
           var inventory_monitoring_id = '<?php echo $value['id']; ?>';
          
           kr.Dependencies.Ajax.run({url:"{{URL::route('inventory_monitoring.updateInventoryRemarks','inventory_monitoring_id')}}",params:{'inventory_monitoring_id':inventory_monitoring_id,'facility_id':facility_id,'inventory_id':inventory_id,'remarks':remarks}, dataType:'json'});

       }   
    </script>
@stop
