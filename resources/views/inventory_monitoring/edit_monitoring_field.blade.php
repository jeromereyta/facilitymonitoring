@extends('partials.master')
@include('reports.charts.facilityCountChart')

@section('title')
    <a href = "/reports">Reports</a>
@endsection


          @section('content')

<div class="clearfix"></div>

@include('flash::message')

<div class="clearfix"></div>


<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h5 class="box-title"><i class="fa fa-pencil"></i>Inventory Monitoring Details</h5>
        </div>

        {!! Form::open(['route' => 'inventory_monitoring.updateMonitoring']) !!}
        <div class="panel-body">
          <div class="admin-form theme-primary">
              <div class="container" style="overflow-x:auto;">

                  <div class="col-md-4">
                  <div class="form-group">
                  <label class="field prepend-icon mt5" for="name">Name: </label>
                  <input type="text" class="form-control" name="name" value="{{$monitorings->name}}" >
                  <input type="hidden" class="form-control" name="facility_id"  value="{{$facility_id}}">
                  </div>
                  </div>
                  <div class="col-md-4">
                  <div class="form-group">
                  <label class="field prepend-icon mt5" for="description">Description:</label>
                  <textarea ="" name ="description" class="form-control" rows="3" placeholder="Enter description. . .">{{$monitorings->description}}</textarea>
                  </div>    
                  </div>                                    
                  </div>

                  <div class="panel-body">
                  <div class="admin-form theme-primary">

                  <table class="table table-responsive">
                  <thead>
                  <tr style="font-size: 15px;  font-weight: bold;">
                  <td nowrap="nowrap" >Inventory Name</td>
                  <td nowrap="nowrap">Serial Number</td>
                  <td nowrap="nowrap">Category</td>
                  <td nowrap="nowrap">Status</td>
                  <td nowrap="nowrap">Remarks</td>
                  </tr>
                  </thead>
                  <tbody>           
                  @foreach($monitorings['inventories'] as $key => $inventory)       

                  <tr>
                  <input type="hidden" value="{{$inventory->id}}" name="inventory_id[{{$key}}]">

                  <tr style="font-size: 15px;">
                  <td>{{$inventory->name}}</td>           
                  <td>{{$inventory->serial_no}}</td>           
                  <td>{{$inventory->category}}</td>
                  <td>
                 @inject('inventoryStatus', 'App\Models\InventoryStatus')
                           
                            <td>
                                <div class="form-group">
                                    <?php $inventory_monitoring_id = $monitorings->id;
                                          
                                          $inventory_id = $inventory->id; 

                                          $test =$inventoryStatus::all()->pluck('id');?>
                                          
                                    {!! Form::select('status',['' => ''] +  $inventoryStatus::all()->pluck('name','name')->toArray(),
                                    $inventory->status,
                                    ['id'=>''.$inventory_id.'','class' => 'form-control gui-input bg-light arrow-key',
                                    'onchange'=>"updateMonitoringStatus('$inventory_monitoring_id',
                                        '$facility_id','$inventory_id',$(this).val());"]) !!}
                                </div>

                  </td>
                  <td><input type="text" class="form-control" name="remarks[{{$inventory->id}}]")}}" value = "{{$inventory->remarks}}"></td>

                  </tbody>
                  @endforeach

                  </table>
                  </div>
                   
                    <div class="box-footer">
                      <div class="form-group">
                        <div class="col-sm-10">
                          <button type="submit" class="btn btn-default btn-sm ph15">Submit</button>

                          <a href="{{ route('inventory_monitoring.showMonitoringList', $facility_id) }}" class="btn btn-default btn-sm ph15">Back</a>
                        </div>
                      </div>
                    </div><!-- box-footer -->   
                </div>  
              </div>
          </div>
        {!! Form::close() !!}

@endsection
@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection