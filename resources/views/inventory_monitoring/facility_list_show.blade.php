
<div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-heading">
          <span class="panel-title">
            <span class="fa fa-list"></span>List of Facility</span>
        </div>
        <div class="panel-body pn">
          <div id="facility_search">
                      
          </div>
        </div>
      </div>
    </div>
  </div>

@section('footer_plugins')
 
  <script src="{{ URL::asset('js/all.js') }}"></script>
    <script>
    function initializeFacilitySearch(){
          modal = new kr.UI.Modal({id:"facility_search",isModal:false,url:"{{URL::route('search.search', array('searchDomain'=>'facilities'))}}",params:{'page_type':'show','monitoring_id':<?php echo $value['id']; ?>},title:"Search Facility"});
      }
    kr.Dependencies.addLoadEvent(function(){
        initializeFacilitySearch();
      });
    </script>
@stop
