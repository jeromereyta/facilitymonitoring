@extends('partials.master')


@section('title')
    <a href = "/inventory_monitoring">Inventory Monitoring</a>
@endsection
@section('content')
        
@include('flash::message')
      <div class="row">
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
          <div class="inner">
          <h3>{{$facility_count}}</h3>
          <p>Facilities</p>
          </div>
          <div class="icon">
          <i class="ion ion-pie-graph"></i>
          </div>
          <a href="/facility" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
          <div class="inner">
          <h3>{{$inventory_count}}</h3>

          <p>Inventory Items</p>
          </div>
          <div class="icon">
          <i class="ion ion-ios-gear-outline"></i>
          </div>
          <a href="/inventory" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
          </div>
          <!-- ./col -->
          <!-- ./col -->
          <?php
          use App\Models\monitorings;
           $numberOfMonitorings = monitorings::count();
           ?>
          <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
          <div class="inner">
          <h3>{{$numberOfMonitorings}}<sup style="font-size: 20px"></sup></h3>
                
          <p>Number of Monitorings</p>
          </div>
          <div class="icon">
          <i class="ion ion-stats-bars"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
          </div>

      </div>

@include('inventory_monitoring.facility_list_table')


@endsection
@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection