@extends('partials.master')

@section('title')
        <a href = "#">Edit Facility Details</a>
@endsection

@section('content')
@include('flash::message')
    @include('inventory_monitoring.edit_field')

@endsection
