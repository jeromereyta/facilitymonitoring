@extends('partials.master')

@section('title')
        <a href = "#">View Monitoring Inventory</a>
@endsection

@section('content')

    <!-- @include('inventory_monitoring.show_field') -->

    <div class="box-footer">
        <div class="form-group">
            <div class="col-sm-10">
                <a href="{!! route('inventory_monitoring.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
            </div>
        </div>
    </div><!-- box-footer -->   
  
@endsection
