   <div class="box">
            <div class="box-header">
              <h6 class="box-title">List of Facilities</h6>
              <!-- <a class="btn btn-flat btn-primary pull-right" href="{!! route('inventory_monitoring.create') !!}">Add New</a> -->
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table class="table table-responsive">
                <thead>
                      <tr style="font-size: 15px;  font-weight: bold;">
                      <td nowrap="nowrap" >Facility Name</td>
                      <td nowrap="nowrap">Description</td>
                     <td nowrap="nowrap">Actions</td>
              </tr>
              </thead>
              <tbody>           
                    @foreach($facilities as $key => $facility)
              <tr>
                    <tr style="font-size: 15px;">

                        <td>{{$facility->name}}</td>           
                        <td>{{$facility->description}}</td>           
                        
                        <td>@include('inventory_monitoring.actions')</td>           
                    </tr> 
              </tr>
              </tbody>
                                  @endforeach

            </table>
        </div>
         {{ $facilities->links() }}
    </div>
  </div>