<div class="btn-group">
            <a class="btn btn-flat btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions
                <span class="caret ml5"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('inventory_monitoring.showMonitoringList', $facility->id) }}"><i class="glyphicon glyphicon-eye-open"></i>
                     View List of Monitoring Inventory</a>
                </li>
            </ul>
</div>