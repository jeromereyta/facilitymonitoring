@extends('partials.master')


@section('title')
    <a href = "/inventory_monitoring">List of Monitoring Sheets for : {{$facility_name}}</a>
@endsection
@section('content')

@include('flash::message')
   <div class="box">
            <div class="box-header">
              <a class="btn btn-flat btn-primary pull-right" href="{!! route('inventory_monitoring.createMonitoring',$facility_id) !!}">Add New</a>
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table class="table table-responsive">
                <thead>
                      <tr style="font-size: 15px;  font-weight: bold;">
                      <td nowrap="nowrap" >Monitoring Name</td>
                      <td nowrap="nowrap">Description</td>
                      <td nowrap="nowrap">Created By</td>
                      <td nowrap="nowrap">Actions</td>

              </tr>
              </thead>
              <tbody>     
                    @if(!$monitorings==[])

              <tr>
                                  @foreach($monitorings as $key => $monitoring)

                    <tr style="font-size: 15px;">
                        <td>{{$monitoring['name']}}</td>           
                        <td>{{$monitoring['description']}}</td>           
                        <td>{{$monitoring['created_by']}}</td>
                        <td>
                              <div class="btn-group">
                              <a class="btn btn-flat btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions
                              <span class="caret ml5"></span>
                              </a>
                              <ul class="dropdown-menu" role="menu">
                              <li>
                              <a href="{{ route('inventory_monitoring.editMonitoring', [$monitoring['id'],$facility_id]) }}"><i class="glyphicon glyphicon-edit"></i>
                              Edit  Monitoring Inventory</a>
                              </li>
                              <li>
                              <a href="{{ route('inventory_monitoring.viewMonitoring', [$monitoring['id'],$facility_id]) }}"><i class="glyphicon glyphicon-eye-open"></i>
                              View  Monitoring Inventory</a>
                              </li>
                              <li>
                              <a href="{{ route('inventory_monitoring.deleteMonitoring', [$monitoring['id'],$facility_id]) }}"><i class="glyphicon glyphicon-trash"></i>
                              Delete  Monitoring Inventory</a>
                              </li>

                              </ul>
                              </div>
                        </td>   
                                @endforeach
                      </tr>

                    @else
                    <tr style="font-size: 15px;"><td>No Monitorings Found in this facility</td></tr>
                    @endif
             

              </tr>
              </tbody>
                      
            </table>
        </div>
         {{ $monitorings->links() }}
    </div>
  </div>

@endsection
@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection