    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box-header">
              <h3 class="box-title">List of Inventory Status</h3>
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
      <ul class="list-group">
      @if($status)
      @foreach($status as $key => $inventory)
      <li class="list-group-item">{{$inventory->name}}</li>
      @endforeach
      @endif
      </ul>
</div>

