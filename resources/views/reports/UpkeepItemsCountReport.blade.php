@extends('partials.master')

@section('header_plugins')

@section('title')
    <a href = "/reports">Reports</a>
@endsection

@section('content')

 <!-- Datepicker -->
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/moment/min/moment.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}" type="text/css"></script>

<!-- body -->



   <div class="box">
            <div class="box-header">
              <h6 class="box-title">Upkeep Items Count Report</h6>
                 </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table class="table table-responsive">
                <thead>
                      <tr style="font-size: 15px;  font-weight: bold;">
                      <td nowrap="nowrap">Upkeep/Item Name</td>
                      <td nowrap="nowrap">Total Assigned</td>
              </tr>
              </thead>
              <tbody>           
                    @forelse($count_list as $key => $upkeep)
              <tr>
                    <tr style="font-size: 15px;">

                        <td>{{$upkeep->name}}</td>                  
                        <td>{{$upkeep->count}}</td>           
                    </tr> 
              </tr>
              </tbody>

              @empty

          
                        <tr><td>No data found for given filters.</td></tr>
                        
              @endforelse

            </table>
        </div>

    </div>
  </div>

<!-- <div class="box-body">
<div class="box box-info">
<canvas id="upkeep_count" height="280" width="500"></canvas>
</div>
</div> -->

<!-- end body  -->


@endsection

@section('footer_plugins')
   <!-- REQUIRED JS SCRIPTS -->
    
    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection

@stop