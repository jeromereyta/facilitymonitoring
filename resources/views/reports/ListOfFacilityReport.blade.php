@extends('partials.master')
@include('reports.charts.FacilitySummaryChart')

@section('header_plugins')

@section('title')
    <a href = "/reports">Reports</a>
@endsection

@section('content')

 <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><bold></bold></h3>
            </div>
           
          <!-- BAR CHART -->
          <div class="box box-success">
          <div class="box box-info">
                        <h3 class="box-title">Facility Summary Report</h3>
          <canvas id="clients" width="500" height="350"></canvas>
          </div>
          </div>
          <!-- /.box -->


            <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>Inventory Name</th>
                    <th>Serial Number</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Last Monitored Status</th>
                </tr>
                    <tbody>
                    @foreach($facilities as $index => $value)
                        <tr style="font-size: 14px;background-color: #67B3DD">
                          <td colspan="5"> {{$value['facility_name']}}</td>     
                        </tr>
                        @if($value['inventories'])
                        @foreach($value['inventories'] as $index => $value)
                        <tr>
                        <td >{{$value['name']}}</td>
                        <td >{{$value['serial_no']}}</td>
                        <td >{{$value['description']}}</td>
                        <td >{{$value['category']}}</td>
                        <td >{{$value['recent_status']}}</td>
                        </tr>                    
                        @endforeach
                        @else
                        <tr><td>No data found for given filters.</td></tr>
                        @endif
                    </tbody>
                    @endforeach
            </table>
            </div>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>



      

@endsection
@section('footer_plugins')

@stop