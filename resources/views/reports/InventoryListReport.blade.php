@extends('partials.master')

@section('header_plugins')

@section('title')
    <a href = "/reports">Reports</a>
@endsection

@section('content')
@include('reports.charts.InventoryStatusChart')

            {!! Form::open(['url' => '/inventoryListReport', 'class' => 'form-horizontal']) !!}

              <div class="container">
                  <div class='col-lg-3'>
                      <div class="form-group">
                          <div class='input-group date' id='datetimepicker6'>
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                              <input type='text' class="form-control" name="from_date" placeholder="from" value="{{$params['from'] or null}}"/>
                          </div>

                          <div class='input-group date' id='datetimepicker7'>
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                              <input type='text' class="form-control" name="to_date" placeholder="to" value="{{$params['to'] or null}}"/>
                          </div>
                      </div>
                  </div>
              </div>

               <button class="btn btn-primary btn-sm ph15" onclick="$('#report_form_append').html('')" type="submit"><i class="fa fa-save"></i> Generate</button>
                          
            {!! Form::close() !!}
 <!-- Datepicker -->
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/moment/min/moment.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}" type="text/css"></script>

<!-- body -->



   <div class="box">
            <div class="box-header">
              <h6 class="box-title">Inventory List Report</h6>
                 </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table class="table table-responsive">
                <thead>
                      <tr style="font-size: 15px;  font-weight: bold;">
                      <td nowrap="nowrap">Inventory/Item Name</td>
                      <td nowrap="nowrap">Serial Number</td>
                      <td nowrap="nowrap">Status</td>
                      <td nowrap="nowrap">Description</td>
                      <td nowrap="nowrap">Facility</td>
                      <td nowrap="nowrap">Date Monitored</td>
              </tr>
              </thead>
              <tbody>           
                    @if($inventory_list)
                    @foreach($inventory_list as $key => $inventory)
              <tr>
                    <tr style="font-size: 15px;">

                        <td>{{$inventory->name}}</td>           
                        <td>{{$inventory->serial_no}}</td>           
                        <td>{{$inventory->status}}</td>           
                        <td>{{$inventory->description}}</td>
                        <td>{{$inventory->facility_name}}</td>   
                        <td>{{$inventory->created_at}}</td>
                    </tr> 
              </tr>
              @endforeach
              @else
              <tr>
                        <td>No results found</td>
              </tr>
              @endif
              </tbody>

            </table>
        </div>

    </div>
  </div>

<div class="box-body">
<div class="box box-info">
<canvas id="inventory_count" height="280" width="500"></canvas>
</div>
</div>

<!-- end body  -->
<script>

    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: "YYYY-MM-DD"
        });
        $('#datetimepicker7').datetimepicker({
            format: "YYYY-MM-DD"
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>

@endsection

@section('footer_plugins')
<!-- REQUIRED JS SCRIPTS -->

    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection

@stop