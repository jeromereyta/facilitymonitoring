
<script src="{{ asset ("/bower_components/AdminLTE/plugins/chartjs/Chart.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/AdminLTE/plugins/chartjs/Chart.min.js") }}" type="text/javascript"></script>

<!-- jQuery 2.1.3 -->
<script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>

<script>
   $(function(){
  $.getJSON("/facility_chart_count", function (result) {
console.log(result);
    var labels = [],data=[];
    for (var i = 0; i < result.length; i++) {
        labels.push(result[i].month);
        data.push(result[i].facilityCount);
    }

    var buyerData = {
      labels : labels,
      datasets : [
        {
          fillColor : "rgba(240, 127, 110, 0.3)",
          strokeColor : "#f56954",
          pointColor : "#A62121",
          pointStrokeColor : "#741F1F",
          data : data
        }
      ]
    };
    var buyers = document.getElementById('facility_count_graph').getContext('2d');
    new Chart(buyers).Line(buyerData, {
      bezierCurve : true
    });

  });

});

      $(function(){
  $.getJSON("/facility_chart_status", function (result) {
console.log(result);
    var labels = [],data=[];
    for (var i = 0; i < result.length; i++) {
        labels.push(result[i].status);
        data.push(result[i].facilityCount);
    }

    var buyerData = {
      labels : labels,
      datasets : [
        {
          fillColor : "rgba(240, 127, 110, 0.3)",
          strokeColor : "#f56954",
          pointColor : "#A62121",
          pointStrokeColor : "#741F1F",
          data : data
        }
      ]
    };
    var buyers = document.getElementById('facility_status_chart').getContext('2d');
    new Chart(buyers).Bar(buyerData);

  });

});

  /* $(function(){
  $.getJSON("/facility_chart_count", function (result) {
console.log(result);
    var labels = [],data=[];
    for (var i = 0; i < result.length; i++) {
        labels.push(result[i].month);
        data.push(result[i].projects);
    }

    var buyerData = {
      labels : labels,
      datasets : [
        {
          data : data
        }
      ]
    };
    var buyers = document.getElementById('projects-graph').getContext('2d');
    new Chart(buyers).Pie(buyerData);

  });

});
*/

</script>

