
<script src="{{ asset ("/bower_components/AdminLTE/plugins/chartjs/Chart.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/AdminLTE/plugins/chartjs/Chart.min.js") }}" type="text/javascript"></script>

<!-- jQuery 2.1.3 -->
<script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>

<script>
   $(function(){

    var buyerData = {
      labels : <?php echo $status; ?>,
      datasets : [
        {
          fillColor : "rgba(240, 127, 110, 0.3)",
          strokeColor : "#f56954",
          pointColor : "#A62121",
          pointStrokeColor : "#741F1F",
          data : <?php echo $count; ?>
        }
      ]
    };

    var buyers = document.getElementById('inventory_count').getContext('2d');
    new Chart(buyers).Line(buyerData, {
      bezierCurve : true
    });







});

</script>

