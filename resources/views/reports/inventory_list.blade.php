
            <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>Inventory Name</th>
                    <th>Serial Number</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Date Created</th>
                </tr>
                    <tbody>
                    @foreach($value['inventories'] as $index => $value)
                        <tr style="font-size: 14px;background-color: #67B3DD">
                            <td >{{$value->name}}</td>
                            <td >{{$value->serial_no}}</td>
                            <td >{{$value->description}}</td>
                            <td >{{$value->category}}</td>
                            <td >{{$value->created_at}}</td>
                            </tr>                    
                    @endforeach
                    </tbody>
                    
            </table>
            </div>
