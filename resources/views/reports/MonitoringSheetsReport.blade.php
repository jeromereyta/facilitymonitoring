@extends('partials.master')

@section('header_plugins')

@section('title')
    <a href = "/reports">Reports</a>
@endsection

@section('content')

            {!! Form::open(['url' => '/monitoringsheets', 'class' => 'form-horizontal']) !!}

              <div class="container">
                  <div class='col-lg-3'>
                      <div class="form-group">
                          <div class='input-group date' id='datetimepicker6'>
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                              <input type='text' class="form-control" name="from_date" placeholder="from" value="{{$params['from'] or null}}"/>
                          </div>

                          <div class='input-group date' id='datetimepicker7'>
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                              <input type='text' class="form-control" name="to_date" placeholder="to" value="{{$params['to'] or null}}"/>
                          </div>
                      </div>
                  </div>
              </div>

               <button class="btn btn-primary btn-sm ph15" onclick="$('#report_form_append').html('')" type="submit"><i class="fa fa-save"></i> Generate</button>
                          
            {!! Form::close() !!}
            @if($monitoring_list)
            @foreach($monitoring_list as $monitoring_name => $value)
     <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{$monitoring_name}}</h3>
            </div>
               <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                <tr>
                    <th>Facility Name</th>
                    <th>Status</th>
                    <th>Remarks</th>
                </tr>
                    <tbody>
                        @foreach($value as $room_name =>$data)
                        <tr style="font-size: 14px;background-color: #67B3DD">
                            <td  colspan=20>{{$room_name}}</td>
                        </tr>
                          @foreach($data['facilities'] as $facility)
                            <tr>
                            <td >{{$facility['name']}}</td>
                            <td >{{$facility['status']}}</td>
                            <td >{{$facility['remarks']}}</td>
                            </tr>                    
                          @endforeach
                    </tbody>
                        @endforeach
                    
          </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endforeach
      @endif
 <!-- Datepicker -->
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/moment/min/moment.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}" type="text/css"></script>


<script>

    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: "YYYY-MM-DD"
        });
        $('#datetimepicker7').datetimepicker({
            format: "YYYY-MM-DD"
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>

@endsection

@section('footer_plugins')

@stop