@extends('partials.master')
@include('reports.charts.facilityCountChart')

@section('title')
		<a href = "/reports">Reports</a>
@endsection

@section('content')


<div class="clearfix"></div>

@include('flash::message')

<div class="clearfix"></div>

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">List of Reports</h3>
    <div class="box-tools pull-right">
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">
    <li><a href="/facilitylist">Facility List Report</a></li>
    <li><a href="/inventoryListReport">List Of Inventory Items (Monitoring Based)</a></li>
    <li><a href="/upkeep-list-report">List Of Upkeep Items (Monitoring Based)</a></li>
    <li><a href="/upkeep-count-report">Count Of Upkeep Items</a></li>

  </div><!-- /.box-body -->
</div><!-- /.box -->


<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"></h3>
    <div class="box-tools pull-right">
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">

    <div class="box box-info">
     <canvas id="facility_count_graph" height="280" width="500"></canvas>
    </div>
    
    <div class="box box-info">
     <canvas id="facility_status_chart" height="280" width="500"></canvas>
    </div>

    
  </div><!-- /.box-body -->
</div><!-- /.box -->        


@endsection

@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection