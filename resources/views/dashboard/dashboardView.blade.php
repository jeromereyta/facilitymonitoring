
@extends('partials.master')

@section('crumb_url')
{{url('#')}}
@endsection

@section('title')
{!!$title or null!!}
@endsection

@section('content')

  {!! $filter or null !!}
  {!! $grid or null !!}
  {!! $edit or null !!}
    {!! $form or null !!}

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.pjax/1.9.6/jquery.pjax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/riot/2.2.4/riot+compiler.min.js"></script>


      <div class="container">
        <div class="row">
            <div class="col-sm-12">
                
                {!! $embed1 or null !!}
            </div>

            <div class="col-sm-6">

                {!! $embed2 or null !!}
                
            </div>
        </div>
    </div>

{!! Rapyd::scripts() !!}
<script>riot.mount("*")</script>
 
@endsection

