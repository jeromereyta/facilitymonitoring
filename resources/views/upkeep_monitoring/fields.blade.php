<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h5 class="box-title"><i class="fa fa-pencil"></i>Inventory Monitoring Details</h5>
        </div>

        <div class="panel-body">
          <div class="admin-form theme-primary">
            <div class="container" style="overflow-x:auto;">

              <form class="form-vertical" role="form">

                <div class="col-md-4">
                  <div class="form-group">
                    <label class="field prepend-icon mt5" for="name">Name: </label>
                    <input type="text" class="form-control" name="name_date" placeholder="Monitoring for {{date( "Y-m-d H:i:s" )}}" disabled="disabled">
                    <input type="hidden" class="form-control" name="name" placeholder="Enter Name" value="Monitoring for {{date( "Y-m-d H:i:s" )}}">
                  </div>
                </div>



                <div class="col-md-4">
                  <div class="form-group">
                    <label class="field prepend-icon mt5" for="description">Description:</label>
                    <textarea  value="" ="" name ="description" class="form-control" rows="3" placeholder="Enter description. . ."></textarea>
                  </div>    
                </div>                                    



              </div>
            </div>        
          </div>
        </div>
        

              
                            @foreach($facilities as $facility_name => $value)<!-- 
                            <tr style="font-size: 14px;background-color: #67B3DD"> -->
                             @if($value['inventories'] != null)
                             
                            @foreach($value['inventories'] as $data)       

                         
                                  <input type="hidden" id="data[{{$facility_name}}][{{$data['inventory_name']}}][status]" name="data[{{$facility_name}}][{{$data['inventory_name']}}][status]" >
                                
                                  <input type="hidden" name="data[{{$facility_name}}][{{$data['inventory_name']}}][remarks]" id="data[{{$facility_name}}][{{$data['inventory_name']}}][remarks]"> 
                                
                            @endforeach
                                @endif
                            @endforeach


                    <div class="box-footer">
                      <div class="form-group">
                        <div class="col-sm-10">
                          <button type="submit" class="btn btn-default btn-sm ph15">Submit</button>

                          <a href="{!! route('upkeep_monitoring.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                        </div>
                      </div>
                    </div><!-- box-footer -->   
                  </form>

                </div>
              </div>  
            </div>
          </div>

          