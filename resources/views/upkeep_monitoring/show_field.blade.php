<div class="box">
<div class="box-header with-border">
<h5 class="box-title"><i class="fa fa-pencil"></i>Inventory Monitoring Details</h5>
</div>

<div class="panel-body">
<div class="admin-form theme-primary">


<form class="form-vertical" role="form">

@if($monitorings)

  <div class="col-md-4">
   <div class="form-group">
    <label class="field prepend-icon mt5" for="name">Name: </label>
    <input type="text" class="form-control" value="" name="name-field" placeholder="{{$monitorings->name}}" disabled="disabled">
    <input type="hidden" value="{{$monitorings->name}}" name="name" placeholder="Enter Name">
    </div>
    </div>



    <div class="col-md-4">
    <div class="form-group">
    <label class="field prepend-icon mt5" for="description">Description:</label>
    <textarea  value="" ="" name ="description" class="form-control" rows="3" placeholder="Enter description. . ." disabled="disabled">{{$monitorings->description}}</textarea>
    </div>    
    </div>                                    

    <div class="col-md-4">
    <div class="form-group">
    <label class="field prepend-icon mt5" for="description">Created By:</label>
    <textarea  value="" ="" name ="description" class="form-control" rows="3" placeholder="Enter description. . ." disabled>{{$monitorings->created_by}}</textarea>
    </div>    
    </div>                                    

    <div class="col-md-4">
    <div class="form-group">
    <label class="field prepend-icon mt5" for="description">Date Created:</label>
    <textarea  value="" ="" name ="description" disabled class="form-control" rows="3" placeholder="Enter description. . .">{{$monitorings->created_at}}</textarea>
    </div>    
    </div>   

    @include('upkeep_monitoring.facility_list')                                 

@else
    @if(($request_date == date( "Y-m-d" )))
        <a class="btn btn-flat btn-primary pull-right" href="{!! route('upkeep_monitoring.create') !!}">Add New</a>
    @endif
    <tr style="font-size: 14px;">
        <td colspan="6" class="text-center">No record found for given date.</td>
    </tr>
@endif
    </div>
  </div>
    
      <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-10">
                        <a href="{!! route('upkeep_monitoring.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                    </div>
                </div>
            </div><!-- box-footer -->   
      </form>

</div>
</div>  