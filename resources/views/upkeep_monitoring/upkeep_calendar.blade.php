   <!-- fullCalendar 2.2.5-->
  <link href="{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.css")}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.print.css")}}" rel="stylesheet" media="print" />


  <!-- jQuery 2.1.3 -->
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script> 
  


<script src="{{ asset ("/bower_components/AdminLTE/plugins/moment/min/moment.min.js") }}" type="text/javascript"></script>

  <script src="{{ asset ("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.js") }}" type="text/javascript"></script>

  <script src="{{ asset ("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.js") }}" type="text/javascript"></script>


<div class="box">
    <div class="box-header with-border">
    <h5 class="box-title"><i class="fa fa-pencil"></i>Upkeep Monitoring Details</h5>
    </div>

    <div class="panel-body">
		
		<div id="calendar"></div>

	</div>
</div>


<script>

	
  $(function () {

  
    /* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();


    $('#calendar').fullCalendar({
        
        aspectRatio: 2.5,

    	 dayClick: function(date, jsEvent, view) {

        $(this).css('background-color', '#ff3333');

        var events = date.format();

        window.location = '/upkeep_monitoring/show?monitoring_date='+events;

    },
    dayRender: function (date, cell) {
       if (moment().diff(date,'days') > 0){
        cell.css("background-color", "BAD5DE");
        }
    },
      header: {
        left: 'prev,next today',
        center: 'title',
        right: ''
      },
      buttonText: {
        today: 'today',
        month: 'month'
      },
    
        events:'upkeep_monitoring/events'
      ,
    
    });
});


</script>
