    <div class="box">
    <div class="box-header with-border">
    <h5 class="box-title"><i class="fa fa-pencil"></i>Upkeep Monitoring Details</h5>
    </div>

    <div class="panel-body">

    @foreach($monitorings as $key => $value)
                {!! Form::open(['method' => 'PUT', 'route' => ['upkeep_monitoring.update', $value['id']], 'class' => 'form-vertical']) !!}

    <div class="col-md-4">
    <div class="form-group">
    <label class="field prepend-icon mt5" for="name">Name: </label>
    <input type="text" class="form-control" value="{{$value['name']}}" name="name-field" placeholder="Enter Name" disabled="disabled">
    </div>
    </div>



    <div class="col-md-4">
    <div class="form-group">
    <label class="field prepend-icon mt5" for="description">Description:</label>
    <textarea  value="" ="" name ="description" class="form-control" rows="3" placeholder="Enter description. . ." >{{$value['description']}}</textarea>
    </div>    
    </div>                                    




    <div class="col-md-4">
    <div class="form-group">
    <label class="field prepend-icon mt5" for="description">Created By:</label>
    <textarea  value="" ="" name ="description" class="form-control" rows="3" placeholder="Enter description. . ." disabled>{{$value['created_by']}}</textarea>
    </div>    
    </div>                                    

    <div class="col-md-4">
    <div class="form-group">
    <label class="field prepend-icon mt5" for="description">Date Created:</label>
    <textarea  value="" ="" name ="description" disabled class="form-control" rows="3" placeholder="Enter description. . .">{{$value['created_at']}}</textarea>
    </div>    
    </div>                                    
       
    </div>

          <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-default btn-sm ph15">Submit</button>

                            <a href="{!! route('upkeep_monitoring.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
                        </div>
                    </div>
                </div><!-- box-footer -->  
    </div>
  
  @endforeach

@include('upkeep_monitoring.facility_list')
  

                                  {!! Form::close() !!}       
                                  