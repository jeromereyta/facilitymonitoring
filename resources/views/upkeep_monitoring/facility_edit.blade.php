@extends('partials.master')

@section('title')
        <a href = "#"></a>
@endsection

@section('content')

    <div class="box">
    <div class="box-header with-border">
    <h5 class="box-title"><i class="fa fa-pencil"></i>Upkeep Monitoring Details</h5>
    </div>
<div class="admin-form theme-primary">
    <div class="panel-body">


                
    <div id="grid">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">

            <thead>
            	<tr>
            		<th>Name</th>
            		<th>Status</th>
            		<th>Remarks</th>
            	</tr>

            </thead>
           
    @foreach($data as $key => $value)    
    <tbody>
    <tr> 
 
 	<td>
    <input type="text" class="form-control gui-input bg-light arrow-key col-xs-3" value="{{$value['inventory_name']}}" name="name-field" placeholder="Enter Name" disabled="disabled">
  	</td>


	@inject('upkeepStatus', 'App\Models\UpkeepStatus')

 	
	        <?php $inventory_monitoring_id = $params['monitoring_id'];
	              $facility_id = $params['facility_id']; 
	              $inventory_id = $value['id']; ?>
	 <td>            
	        {!! Form::select('status',['' => ''] +  $upkeepStatus::all()->pluck('name','name')->toArray(),
	        $value['status'],
	        ['id'=>''.$inventory_id.'','class' => 'form-control gui-input bg-light arrow-key',
	        'onchange'=>"updateUpkeepMonitoringStatus('$inventory_monitoring_id',
	            '$facility_id','$inventory_id',$(this).val());"]) !!}
	       <!--  inventory_id, facility_id, inventory_id, new_status -->
	
 	</td>                            
    <td>
	    {!! Form::text('remarks', $value['remarks'] ,['class' => 'form-control gui-input bg-light arrow-key',
	    'onkeypress'=>"updateUpkeepMonitoringRemarks('$inventory_monitoring_id',
	            '$facility_id','$inventory_id',$(this).val());"]) !!}
	</td>


				</tr>
    		</tbody>
    		 @endforeach
    	</table>
	</div>
	
	</div>

    </div>

    </div>
  
 

	<div class="box-footer">
        <div class="form-group">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-default btn-sm ph15" onclick="alert('Successfully Updated')">Submit</button>

                <a href="{!! route('upkeep_monitoring.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
            </div>
        </div>
    </div><!-- box-footer -->  

</div>
 
  

                                  {!! Form::close() !!}       

@endsection


@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->
	
	<!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>

    <script src="{{ URL::asset('js/kr.js') }}"></script>
    <script>

    function updateUpkeepMonitoringStatus(upkeep_monitoring_id,facility_id,inventory_id,status){
           //if(!value||value==0)return;
           var upkeep_monitoring_id = '<?php echo $params['monitoring_id']; ?>';
          
           kr.Dependencies.Ajax.run({url:"{{URL::route('upkeep_monitoring.updateUpkeepStatus','upkeep_monitoring_id')}}",params:{'upkeep_monitoring_id':upkeep_monitoring_id,'facility_id':facility_id,'inventory_id':inventory_id,'status':status}, dataType:'json'});

       }

    function updateUpkeepMonitoringRemarks(upkeep_monitoring_id,facility_id,inventory_id,remarks){
           //if(!value||value==0)return;
           var upkeep_monitoring_id = '<?php echo $params['monitoring_id']; ?>';
           var is_remarks_required = '<?php echo $value['is_remarks_required'];?>';

           if(remarks.length === 0 && is_remarks_required == 1){
            alert('Remarks is required');
           }else{
            kr.Dependencies.Ajax.run({url:"{{URL::route('upkeep_monitoring.updateUpkeepRemarks','upkeep_monitoring_id')}}",params:{'upkeep_monitoring_id':upkeep_monitoring_id,'facility_id':facility_id,'inventory_id':inventory_id,'remarks':remarks}, dataType:'json'});         
        }
    }   
    </script>
@endsection