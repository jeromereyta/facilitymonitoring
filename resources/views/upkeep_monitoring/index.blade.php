@extends('partials.master')

@section('title')
		<a href = "/upkeep_monitoring">Upkeep Monitoring</a>
@endsection
@section('content')
 
<div class="clearfix"></div>
        <div class="clearfix"></div>
@include('flash::message')
{{--@include('upkeep_monitoring.monitoring_list')--}}
@include('upkeep_monitoring.upkeep_calendar')
@endsection

@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->


    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection