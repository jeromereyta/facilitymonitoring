<div class="btn-group">
            <a class="btn btn-flat btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions
                <span class="caret ml5"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('upkeep_monitoring.show', $monitoring->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View Monitoring Inventory</a>
                </li>
                <li>
                    <a href="{{ route('upkeep_monitoring.edit', $monitoring->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit Monitoring Inventory</a>
                </li>
                <li>
                    <a href="{{ route('upkeep_monitoring.delete', $monitoring->id) }}" class="alert-link" onclick='return confirm("Are you sure you want to delete ?")'><span class="label label-warning"><i class="glyphicon glyphicon-trash"></i></span> Delete</a>
                </li>

            </ul>
</div>