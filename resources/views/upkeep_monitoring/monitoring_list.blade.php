   <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Upkeep Monitoring</h3>
              <a class="btn btn-primary pull-right btn-flat" href="{!! route('upkeep_monitoring.create') !!}">Add New</a>
            </div>
            <!-- /.box-header -->
           <div class="panel-body">
            <div class="admin-form theme-primary">

              <table id="example2" class="table table-responsive">
                <thead>
                     <tr style="font-size: 15px;  font-weight: bold;">
                      <td nowrap="nowrap" >Monitoring Name</td>
                      <td nowrap="nowrap">Description</td>
                      <td nowrap="nowrap">Created By</td>
                      <td nowrap="nowrap">Date Created</td>
                      <td nowrap="nowrap">Actions</td>
              </tr>
              </thead>
              <tbody>           
                    @foreach($monitorings as $key => $monitoring)

              <tr>
                    <tr style="font-size: 15px;">
                        <td>{{$monitoring->name}}</td>           
                        <td>{{$monitoring->description}}</td>           
                        <td>{{$monitoring->created_by}}</td>           
                        <td>{{$monitoring->created_at}}</td>           
                        <td>@include('upkeep_monitoring.actions')</td>           
                    </tr> 
              </tr>
              </tbody>
                                  @endforeach

            </table>
        </div>

    </div>
  </div>