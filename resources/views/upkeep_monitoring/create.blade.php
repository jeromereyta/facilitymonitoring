@extends('partials.master')

@section('title')
        <a href = "#">Create New Upkeep Monitoring</a>
@endsection

@section('content')

 
    <div class="row">
@include('flash::message')

        {!! Form::open(['route' => 'upkeep_monitoring.store']) !!}
		@include('upkeep_monitoring.fields')
        {!! Form::close() !!}
    
    </div>
@endsection

@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection