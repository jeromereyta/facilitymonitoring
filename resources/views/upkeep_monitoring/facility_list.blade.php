
<div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-heading">
          <span class="panel-title">
            <span class="fa fa-list"></span>List of Facility</span>
        </div>
        <div class="panel-body pn">
          <div id="upkeep_facility_search">
                      
          </div>
        </div>
      </div>
    </div>
  </div>

@section('footer_plugins')
  <!-- jQuery 2.1.3 -->
  <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
  <script src="{{ URL::asset('js/all.js') }}"></script>
    <script>
    function initializeFacilitySearch(){
          modal = new kr.UI.Modal({id:"upkeep_facility_search",isModal:false,url:"{{URL::route('search.search', array('searchDomain'=>'upkeep_facilities'))}}",params:{'page_type':'upkeep_index','monitoring_id':<?php echo $monitorings->id; ?>},title:"Search Facility"});
      }
    kr.Dependencies.addLoadEvent(function(){
        initializeFacilitySearch();
      });

    function updateUpkeepMonitoringStatus(upkeep_monitoring_id,facility_id,inventory_id,status){
           //if(!value||value==0)return;
           var upkeep_monitoring_id = '<?php echo $monitorings->id; ?>';
          
           kr.Dependencies.Ajax.run({url:"{{URL::route('upkeep_monitoring.updateUpkeepStatus','upkeep_monitoring_id')}}",params:{'upkeep_monitoring_id':upkeep_monitoring_id,'facility_id':facility_id,'inventory_id':inventory_id,'status':status}, dataType:'json'});

       }

    function updateUpkeepMonitoringRemarks(upkeep_monitoring_id,facility_id,inventory_id,remarks){
           //if(!value||value==0)return;
           var upkeep_monitoring_id = '<?php echo $monitorings->id; ?>';
          
           kr.Dependencies.Ajax.run({url:"{{URL::route('upkeep_monitoring.updateUpkeepRemarks','upkeep_monitoring_id')}}",params:{'upkeep_monitoring_id':upkeep_monitoring_id,'facility_id':facility_id,'inventory_id':inventory_id,'remarks':remarks}, dataType:'json'});

       }   
    </script>
@stop