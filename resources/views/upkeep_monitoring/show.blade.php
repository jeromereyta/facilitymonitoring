@extends('partials.master')

@section('title')
        <a href = "#">View Upkeep Monitoring Sheets</a>
@endsection

@section('content')

    @include('upkeep_monitoring.show_field')

    <div class="box-footer">
        <div class="form-group">
            <div class="col-sm-10">
                <a href="{!! route('upkeep_monitoring.index') !!}" class="btn btn-default btn-sm ph15">Back</a>
            </div>
        </div>
    </div><!-- box-footer -->   
  
@endsection
@section('footer_plugins')

    <!-- REQUIRED JS SCRIPTS -->
    <!-- jQuery 2.1.3 -->
     <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script> 
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
@endsection