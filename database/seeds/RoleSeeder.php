<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
          'id' => 100,
          'name'=>'Administrator',
          'email' => 'admin@yahoo.com',
          'password' => '$2y$10$ruAzr7e4CL2VwwtbrlG1BObJvLtq29PSVxIAMlX3FtMf57XnFznwK',
        ]);

        DB::table('users')->insert([
          'id' => 200,
          'name'=>'Administrator',
          'email' => 'sg@yahoo.com',
          'password' => '$2y$10$ruAzr7e4CL2VwwtbrlG1BObJvLtq29PSVxIAMlX3FtMf57XnFznwK',
        ]);


        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'System Admin',
            'label'=>'Admin',
        ]);
        
        DB::table('roles')->insert([
            'id' => 2,
            'name' =>'User',
            'label'=>'User',
        ]);

        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'Guest',
            'label'=>'Guest',
        ]);

        DB::table('role_user')->insert([
            'role_id' => 3,
            'user_id' => 200
        ]);

            

        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 100
        ]);

        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 100
        ]);
        
        

        DB::table('permissions')->insert([
            'id' => 1,
            'name' => 'admin-create',
            'label' => 'admin-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 2,
            'name' => 'admin-read',
            'label' => 'admin-read',
        ]);

        DB::table('permissions')->insert([
            'id' => 3,
            'name' => 'admin-update',
            'label' => 'admin-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 4,
            'name' => 'admin-delete',
            'label' => 'admin-delete',
        ]);


        DB::table('permissions')->insert([
            'id' => 5,
            'name' => 'monitoring_inventory-create',
            'label' => 'monitoring_inventory-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 6,
            'name' => 'monitoring_inventory-read',
            'label' => 'monitoring_inventory-read',
        ]);

        DB::table('permissions')->insert([
            'id' => 7,
            'name' => 'monitoring_inventory-update',
            'label' => 'monitoring_inventory-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 8,
            'name' => 'monitoring_inventory-delete',
            'label' => 'monitoring_inventory-delete',
        ]);



        DB::table('permissions')->insert([
            'id' => 9,
            'name' => 'monitoring_upkeep-create',
            'label' => 'monitoring_upkeep-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 10,
            'name' => 'monitoring_upkeep-read',
            'label' => 'monitoring_upkeep-read',
        ]);

        DB::table('permissions')->insert([
            'id' => 11,
            'name' => 'monitoring_upkeep-update',
            'label' => 'monitoring_upkeep-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 12,
            'name' => 'monitoring_upkeep-delete',
            'label' => 'monitoring_upkeep-delete',
        ]);


        DB::table('permissions')->insert([
            'id' => 13,
            'name' => 'facility-create',
            'label' => 'facility-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 14,
            'name' => 'facility-read',
            'label' => 'facility-read',
        ]);

        DB::table('permissions')->insert([
            'id' => 15,
            'name' => 'facility-update',
            'label' => 'facility-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 16,
            'name' => 'facility-delete',
            'label' => 'facility-delete',
        ]);


        DB::table('permissions')->insert([
            'id' => 17,
            'name' => 'status-create',
            'label' => 'status-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 18,
            'name' => 'status-read',
            'label' => 'status-read',
        ]);

        DB::table('permissions')->insert([
            'id' => 19,
            'name' => 'status-update',
            'label' => 'status-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 20,
            'name' => 'status-delete',
            'label' => 'status-delete',
        ]);




        DB::table('permissions')->insert([
            'id' => 25,
            'name' => 'role-create',
            'label' => 'role-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 26,
            'name' => 'role-read',
            'label' => 'role-read',
        ]);

        DB::table('permissions')->insert([
            'id' => 27,
            'name' => 'role-update',
            'label' => 'role-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 28,
            'name' => 'role-delete',
            'label' => 'role-delete',
        ]);


        DB::table('permissions')->insert([
            'id' => 29,
            'name' => 'user-create',
            'label' => 'user-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 30,
            'name' => 'user-read',
            'label' => 'user-read',
        ]);




        DB::table('permissions')->insert([
            'id' => 31,
            'name' => 'user-update',
            'label' => 'user-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 32,
            'name' => 'user-delete',
            'label' => 'user-delete',
        ]);







        DB::table('permission_role')->insert([
            'permission_id' => 29,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 30,
            'role_id' => 1,
        ]);
    
        DB::table('permission_role')->insert([
            'permission_id' => 31,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 32,
            'role_id' => 1,
        ]);







/////////////////////////////

        DB::table('permissions')->insert([
            'id' => 33,
            'name' => 'item-create',
            'label' => 'item-create',
        ]);
        
        DB::table('permissions')->insert([
            'id' => 34,
            'name' => 'item-read',
            'label' => 'item-read',
        ]);

        DB::table('permissions')->insert([
            'id' => 35,
            'name' => 'item-update',
            'label' => 'item-update',
        ]);

        DB::table('permissions')->insert([
            'id' => 36,
            'name' => 'item-delete',
            'label' => 'item-delete',
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 1,
        ]);
    
        DB::table('permission_role')->insert([
            'permission_id' => 3,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 4,
            'role_id' => 1,
        ]);


    }

}

