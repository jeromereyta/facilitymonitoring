<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMonitoringInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring_inventory', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('monitoring_id');
        $table->integer('facility_id');
        $table->integer('inventory_id');
        $table->integer('inventory_status_id');
        $table->string('remarks');
        $table->timestamps();
        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('monitoring_inventory');
    }
}
