<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('inventory_items', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('serial_no');
        $table->string('inventory_type');
        $table->string('description');
        $table->string('category');
        $table->integer('facility_id');
        $table->integer('is_remarks_required');
        $table->timestamps();
        $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventory_items');
    }
}
