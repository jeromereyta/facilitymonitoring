<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFacility extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {       
        Schema::create('facilities', function (Blueprint $table) {
        $table->increments('id');
        $table->string('category_id');
        $table->string('name');
        $table->string('description');
        $table->string('room_type');
        $table->timestamps();
        $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('facilities');
    }
}
